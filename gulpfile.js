var elixir = require('laravel-elixir');
//var gulp = require('gulp');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
 

	//compile less
	mix.less('app.less');
	//font files
	mix.copy('resources/assets/less/src/themes/default/assets/fonts', 'public/themes/default/assets/fonts');

	//mix.copy('resources/assets/less/src/semantic.js', 'public/js/');
	//
	

	//mix.scripts(['init.js', 'post_comment.js', 'post_watch.js', 'post_care_factor.js', 'delete_watch.js', 'semantic.js', 'validate.js'], 'public/js/main.js');
    
    mix.scriptsIn('resources/assets/js', 'public/js/main.js');


});
