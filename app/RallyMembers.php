<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RallyMembers extends Model {

	/**
	 * The database table used by the model.
	 *	
	 * @var string
	 */
	protected $table = 'rally_members';
	public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_name','confirmation'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['user_name'];

	/**
	 * Set an eloquent relationship between rantrally and members.
	 *
	 * @var array
	 */
	public function rantRally()
	{
	  return $this->belongsTo('App\RantRally', 'rant_rally_id', 'rally_id');
	}	
}
