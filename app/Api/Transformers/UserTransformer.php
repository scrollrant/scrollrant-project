<?php namespace App\Api\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public static function transform(User $user)
    {
        if($user->state && $user->country && $user->gender)
        {

            return 
            [
                'user_id'        => (int) $user->user_id,
                'first_name'     => $user->first_name,
                'surname'        => $user->surname,
                'alias_name'     => $user->alias_name,
                'country'        => $user->country->country_name,
                'state'          => $user->state->state_name,
                'gender'         => $user->gender->gender,
            ];
        }

        return 
            [
                'user_id'        => (int) $user->user_id,
                'first_name'     => $user->first_name,
                'surname'        => $user->surname,
                'alias_name'     => $user->alias_name,
            ];
    }

}