<?php namespace App\Api\Transformers;

use App\Hook;
use League\Fractal\TransformerAbstract;

class HooksTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public static function transform(Hook $hook)
    {


        return 
        [
        'hook_id'        => (int) $hook->hook_id, 
        'hook_name'           => $hook->hook_name,
        'user_id'             => $hook->user_id,
        'company_id'          => $hook->company_id,
        'country_id'          => $hook->country_id,
        'state_id'            => $hook->state_id
        ];

    }
    


}