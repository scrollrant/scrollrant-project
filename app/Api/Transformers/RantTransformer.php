<?php namespace App\Api\Transformers;

use App\Rant;
use League\Fractal\TransformerAbstract;

class RantTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public static function transform(Rant $rant)
    {
        // if($rant->hooks || $rant->photo || $rant->careFactor || $rant->comment || $rant->watchers || $rant->rant_face)
        // {

        return 
        [
        'rant_id'        => (int) $rant->rant_id,
        'rant'           => $rant->rant,
        'user'           => $rant->user,
        'status'         => $rant->status,
        'create_date'    => $rant->create_date,
        'hooks'          => $rant->hooks,
        'photo'          => $rant->photo,
        'care_factor'   => $rant->careFactor,
        'care_factor_count'   => $rant->caresCount,
        'comments'       => $rant->comment,
        'comments_count' => $rant->userCommentsCount,
        'watch'       => $rant->watchers,
        'watch_count'       => $rant->watchCount,
        'rant_face'      => $rant->rant_face,
                // 'user_status'    => $rant->user->userStatus,

        ];

    }
    


}