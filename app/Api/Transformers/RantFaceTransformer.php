<?php namespace App\Api\Transformers;

use App\RantFace;
use League\Fractal\TransformerAbstract;

class RantFaceTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public static function transform(RantFace $rant_face)
    {


        return 
        [
        'rant_face_id'        => (int) $rant_face->rant_face_id,
        'rant_face_name'      => $rant_face->rant_face_name,
        'rant_face_path'      => $rant_face->rant_face_path,   
        ];

    }
    


}