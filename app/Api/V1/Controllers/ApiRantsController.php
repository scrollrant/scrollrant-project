<?php
namespace App\Api\V1\Controllers;

use App\Rant;
use App\Hook;
use Validator;
use Authorizer;
use App\MakeHook;
use App\RantFace;
use App\RantPhoto;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\RantFormRequest;
use Illuminate\Support\Facades\Password;
use App\Api\Transformers\RantTransformer;
use App\Api\Transformers\HooksTransformer;
use App\Api\Transformers\RantFaceTransformer;
use Dingo\Api\Exception\ValidationHttpException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Api\V1\Controllers\ApiBaseController as Controller;

class ApiRantsController extends Controller
{
	
	public function __construct()
	{
	   $this->middleware('api.auth');
	}


	/**
     * To get common validation rules for rant
     *
     * @return array
     */
    public function getRantValidationRules()
    {
        return [
            'rant' => 'required|max:500',
            'file' => 'image|max:900',
            'hook_id' => 'numeric' 
            ];
    }

	/**
	 * Get the users rants via the authenticated user and pass it through trnasformer
	 * 
	 * 
	 * @return [json] [paginated results of all the user's rant but if auth fails return 401]
	 */
	public function getUserRants()
	{	

		$user_id = $this->auth->user()->user_id;
		$user_rants = Rant::with(['careFactor' => function ($q) {
						  $q->where('user_id', $this->auth->user()->user_id);
						}, 'watchers' => function ($q) {
						  $q->where('user_id', $this->auth->user()->user_id);
						}])->where('user_id','=', $user_id)->orderBy('create_date', 'desc')->paginate(3);
		
		if ($user_rants) 

		{
			return $this->response->paginator($user_rants, new RantTransformer)->setStatusCode(200);
		}

		return $this->response->errorUnauthorized('Login to access this area');
	}

	/**
	 * Get all the rants for the rants page!!
	 * @return [type] [description]
	 */
	public function getRants()
	{

		$user = $this->auth->user();
		
		if ($user) 
		{
			if ($user->state_id  && $user->country_id != Null )
	        {
	            $state_rant = Rant::GetStateRants($user->state_id);

	            $country_rant = Rant::GetCountryRants($user->country_id, $user->state_id );

	            $global_rant = Rant::GetGlobalRants($user->country_id);

	            $rant = $global_rant->merge($country_rant)->merge($state_rant);

	           return $this->response->collection($rant, new RantTransformer)->setStatusCode(200);
	        }
	        else
	        {
	            
	            $rant = Rant::with(['careFactor' => function ($q) {
						  $q->where('user_id', $this->auth->user()->user_id);
						}, 'watchers' => function ($q) {
						  $q->where('user_id', $this->auth->user()->user_id);
						}])->orderBy('create_date', 'desc')->paginate(3);

	            return $this->response->paginator($rant, new RantTransformer)->setStatusCode(200);

	        }

	        return $this->response->errorUnauthorized('Login to access this area');
		}		
	}

	/**
	 * Make a post request to create a rant
	 * @return [type] [description]
	 */
	public function createRant(Request $request)
	
	{
		

		$validationRules =  $this->getRantValidationRules();

		$rant_fields = $request->all();

		$this->validateOrFail($rant_fields, $validationRules);

		$rants = new Rant;
		$user = $this->auth->user();

        $data = array('user_id'=>$user->user_id,

        'rant'=> $request->rant,
        'country_id'=> $user->country_id,
        'state_id'=> $user->state_id,
        'created_at'=> Carbon::now(),
        ); 

        $id = $rants->insertGetId($data);

        //Event::fire(new RantEvent($id));
        
        if($request->file('file'))

        {

            $photo = $this->makePhoto($request->file('file'));

            Rant::findorFail($id)->addPhoto($photo);
        }

        if ($request->hook_id)
        {

            $this->makeHook($id, $request->hook_id);
        }


        return $this->response->created('success','The rant has been created');
	}


	/**
	 * Deletes the rant!
	 * @return [type] [description]
	 */
	public function deleteRant()
	{

	}

	/**
     * [makePhoto description]
     * @param  UploadedFile $file [description]
     * @return [type]             [description]
     */
    protected function makePhoto(UploadedFile $file)
    
    {
        return  RantPhoto::named($file->getClientOriginalName())

        ->move($file);
    }

    /**
     * [makeHook description]
     * @param  [type] $rant_id [description]
     * @param  [type] $hook_id [description]
     * @return [type]          [description]
     */
    protected function makeHook($rant_id, $hook_id)
    {
        $make_hook = new MakeHook;

        $make_hook->rant_id = $rant_id;

        $make_hook->hook_id = $hook_id;

        $make_hook->save();

        $update_rant_status = Rant::where('rant_id', '=', $rant_id)->update(['status' =>  'public']);
    }

    /**
     * allows the user to pick a rant face for their rant!
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateRantFace(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'rant_id' => 'required|numeric',
            'rant_face_id' => 'required|numeric',
            ]
            );

        if ($validator->fails()) {
            
            throw new ValidationHttpException($validator->errors()->all());
        }

        $update_rant_face = Rant::find($request->rant_id);

        if($update_rant_face->user_id ==  $this->auth->user()->user_id && $request->rant_face_id != Null)
        {
           $update_rant_face->rant_face_id = $request->rant_face_id; 

           $update_rant_face->save();

           //Event::fire(new RantFaceEvent($update_rant_face));
        }
        
        return $this->response->created('success','The rant face has been created');
    }


    /**
     * Get all the rant faces 
     * @return [type] [description]
     */
    public function getRantFaces()
    {

    	$rant_faces = RantFace::all();

    	return $this->response->collection($rant_faces, new RantFaceTransformer)->setStatusCode(200);
    }

    /**
     * Get all the hooks
     * @return [type] [description]
     */
    public function getHooks()
    {

    	$hooks = Hook::all();

    	return $this->response->collection($hooks, new HooksTransformer)->setStatusCode(200);
    }


}