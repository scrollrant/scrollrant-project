<?php
namespace App\Api\V1\Controllers;
use Authorizer;
use Validator;
use Config;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Http\RedirectResponse;
use App\Api\Transformers\UserTransformer;
use App\Api\V1\Controllers\ApiBaseController as Controller;
use Illuminate\Support\Facades\Password;
use Dingo\Api\Exception\ValidationHttpException;

class ApiUserController extends Controller
{
	/*** 
		Improve this class so that it only has one method instead of the duplicate methods.
		Maybe pass request to user details method and get email and alias name as includes
		in the transformer.
	***/

	public function __construct()
	{
	   $this->middleware('api.auth');
	}


	/**
	 * Get the users details via the authenticated user and pass it through trnasformer
	 * 
	 * 
	 * @return [type] [description]
	 */
	public function getUserDetails()
	{	

		$user = $this->auth->user();
		
		if ($user) 

		{
			return $this->response->item($user, new UserTransformer)->setStatusCode(200)->addMeta('Success', 'The user exists')->addMeta('access_token', response()->json(compact('accessToken')));
		}
		
		return $this->response->errorUnauthorized('Login to access this area');

	}

	/**
	 * [getUserEmail description]
	 * @return [type] [description]
	 */
	public function getUserEmail()
	{
		
		$user = $this->auth->user();

		if ($user) 

		{
			return $this->response->array(['user_email' => $user->email, 'Success' => 'User email recieved'])->setStatusCode(200);
		}

		return $this->response->errorUnauthorized('Login to access this area');

	}

	/**
	 * [getUserAliasName description]
	 * @return [type] [description]
	 */
	public function getUserAliasName()
	{
		$user = $this->auth->user();

		if ($user) 

		{		
			return $this->response->array(['alias_name' => $user->alias_name, 'Success' => 'User aliasname recieved'])->setStatusCode(200);	
		}

		return $this->response->errorUnauthorized('Login to access this area');

	}


}