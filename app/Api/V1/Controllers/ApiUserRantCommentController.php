<?php namespace App\Api\V1\Controllers;

use Auth;
use Event;
use App\Rant;
use Validator;
use Carbon\Carbon;
use App\RantCommentUser;
use App\Events\CommentEvent;
use Illuminate\Http\Request;
use Dingo\Api\Exception\ValidationHttpException;
use App\Api\V1\Controllers\ApiBaseController as Controller;

class ApiUserRantCommentController extends controller{

    public function __construct()
    {
        $this->middleware('api.auth');
    }


    /**
     * authorize the user and then allow them to insert a rant commment.
     *
     * fire a new event that accepts array of data
     *@param CommentFormRequest $request
     *
     * @return redirect
     */
    public function createRantCommentUser(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'rant_id' => 'required|numeric',
            'comment' => 'required'
            ]
            );

        if ($validator->fails()) {
            
            throw new ValidationHttpException($validator->errors()->all());
        }

        $this->makeComment($this->auth->user()->user_id, $request->rant_id, $request->comment);     
    }



    private function makeComment($user_id, $rant_id, $comment)
    {

        $rantcomment = new RantCommentUser;

        $rant_exists = Rant::where('rant_id','=', $rant_id)->firstOrFail();

        if ($rant_exists) 
        {
            $data = array(
            'user_id'=>$user_id, 
            'rant_id'=>$rant_id,
            'rant_comment'=>$comment,
            'created_at'=> Carbon::now(),
            'created_date'=> Carbon::now());          
            $id = $rantcomment->insertGetId($data);

            $userRantComment = RantCommentUser::GetUserRantComment($id);

            $rant =  ['firstname' => $userRantComment->user->first_name,
            'surname' => $userRantComment->user->surname,
            'comment' => $userRantComment['rant_comment'],
            'rantID' => $userRantComment['rant_id'],
            'create_date' => $userRantComment['created_date']
            ];

            //Event::fire(new CommentEvent($rant));

            return $this->response->created();
        }

        
    }

    /**
     * get all the comments based on rantid. Function is used by the jquery
     *
     *@param  $rantid
     *
     * @return json
     */
    public function getComments($rantid)
    {
            
        $comments = RantComment::GetJsonRantComment($rantid);

        return Response::json($comments);
    }

    //other controller methods...
}

?>