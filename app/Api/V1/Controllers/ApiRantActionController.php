<?php namespace App\Api\V1\Controllers;


use File;
use App\Rant;
use Validator;
use Authorizer;
use App\NotRant;
use App\MakeHook;
use App\RantPhoto;
use App\WatchRant;
use App\ReportRant;
use Illuminate\Http\Request;
use Dingo\Api\Exception\ValidationHttpException;
use App\Api\V1\Controllers\ApiBaseController as Controller;

class ApiRantActionController extends controller{

    public function __construct()
    {
        $this->middleware('api.auth');
    }

   /**
    * Create various actions on a rant based on action
    * @param  Request $request [description]
    * @return redirect()->back()
    */
    public function createAction(Request $request)    
    {

        $validator = Validator::make($request->all(), [
            'rant_id' => 'required|numeric',
            'action' => 'required|alpha',
            ]);

        if ($validator->fails()) {
            
            throw new ValidationHttpException($validator->errors()->all());
        }

        if ($request->action == 'Watch')

        {
            $watch = $this->createWatch($request->rant_id,$this->auth->user()->user_id);

        }

        if ($request->action == 'Delete')

        {
            $delete = $this->deleteRant($request->rant_id, $this->auth->user()->user_id);
        }

        if ($request->action == 'Report')

        {
            $report = $this->createReport($request->rant_id, $this->auth->user()->user_id);
        }

        if ($request->action == 'NotRant')

        {
            $report = $this->createNotRant($request->rant_id, $this->auth->user()->user_id);
        }

        //return $this->response->error('action must be: - Watch, Delete, Report or Not_Rant',400);

    }

    /**
     * Watch a rant to get notifications
     * Can only watch 5 max
     * @param  [int] $rant_id 
     * @param  [int] $user_id
     * 
     */
    protected function createWatch($rant_id, $user_id)
    {
        $watch = new WatchRant;

        $rant_exists = Rant::where('rant_id','=',$rant_id)->firstOrFail();
        
        $already_watching  = WatchRant::where('rant_id', '=', $rant_id)->where('user_id', '=', $user_id)->first();
        
        $watch_count = WatchRant::where('user_id','=', $this->auth->user()->user_id)->count();

        if (!$already_watching && $watch_count < 5 && $rant_exists)
        {
            $watch->rant_id = $rant_id;

            $watch->user_id = $user_id;

            $watch->save();

            $watch_rant =  ['watch_id' => $watch->watch_id,
            'rant_id' => $watch->rant_id,
            'user_id' => $watch->user_id,
            'rant' => str_limit($watch->rant->rant, 20) 
            ];

           // Event::fire(new WatchEvent($watch_rant));
            return $this->response->created();
        }
        else
        {
            return $this->response->error('Already watching this rant!',400);
        }
    }
    

    /**
     * delete a  watch and send a rant id to the delete event so watch can be delete from that rant
     * @param  watch_id
     * @param  Request $request
     * @return redirect back
     */
    public function deleteWatch($watch_id, Request $request)
    {
        
        WatchRant::where('watch_id', $watch_id)->delete();

        $rant_id = [
        'rant_id' => $request->rant_id
        ];

        //Event::fire(new DeleteWatchEvent($rant_id));
        //
         return $this->response->created();

        
    }
    /**
     * Report a rant
     * @param  [int] $rant_id [description]
     * @param  [int] $user_id [description]
     * 
     */
    protected function createReport($rant_id, $user_id)
    {
        $report = new ReportRant;

        $rant_exists = Rant::where('rant_id','=',$rant_id)->firstOrFail();
        
        $already_reported  = ReportRant::where('rant_id', '=', $rant_id)->where('user_id', '=', $user_id)->first();
        
        if (!$already_reported && $rant_exists)
        {
            $report->rant_id = $rant_id;

            $report->user_id = $user_id;

            $report->save();

            //Event::fire(new ReportEvent($report));
        
    
            return $this->response->created();
        }
        else
        {
           
            return $this->response->error('You have already reported this rant.',400);
        }
    }

    /**
     * declare that a rant isn't a rant
     * @param  [int] $rant_id [description]
     * @param  [int] $user_id [description]
     * 
     */
    protected function createNotRant($rant_id, $user_id)
    {
        $not_rant = new NotRant;

        $rant_exists = Rant::where('rant_id','=',$rant_id)->firstOrFail();
        
        $already_declared  = NotRant::where('rant_id', '=', $rant_id)->where('user_id', '=', $user_id)->first();
        
        if (!$already_declared && $rant_exists)
        {
            $not_rant->rant_id = $rant_id;

            $not_rant->user_id = $user_id;

            $not_rant->save();

            //Event::fire(new NotRantEvent($not_rant));
            
            return $this->response->created();
        }
        else
        {
            return $this->response->error('You have already declared this rant!',400);
        }
    }


    /**
     * authorize user and delete a rant based on (rantid) and its associated pictures in publie folder. 
     * Only the creator can delete their own rant!
     * @return redirect
     */
    protected function deleteRant($rantid, $user_id)
    {

        $rant_exists = Rant::where('rant_id','=',$rantid)->firstOrFail();

        $user_owns_rant = Rant::where('rant_id','=',$rantid)->where('user_id','=',$user_id)->first();
        
        $rant_photo = RantPhoto::where('rant_id', $rantid)->first();

        if ($user_owns_rant && $rant_exists)  

        {
            $user_owns_rant->delete(); 
            
            if ($rant_photo)
            {
                File::delete('images/'.$rant_photo->name);
            }

            return $this->response->created();
        }   

        return $this->response->error('You are not authorized to delete this rant!',401);  
    }

    //other controller methods...
}

?>