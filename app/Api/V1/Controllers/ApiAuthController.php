<?php
namespace App\Api\V1\Controllers;

use Auth;
use Config;
use App\User;
use Validator;
use Authorizer;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use App\Jobs\SendWelcomeEmail;
use App\Jobs\SendVerificationEmail;
use App\Jobs\SendCompanyWelcomeEmail;
use Illuminate\Support\Facades\Password;
use App\Jobs\SendCompanyVerificationEmail;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Dingo\Api\Exception\ValidationHttpException;
use App\Api\V1\Controllers\ApiBaseController as Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class ApiAuthController extends Controller
{
    
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    
    private $maxLoginAttempts = 5;


    public function test()
    {
        $users = User::findOrFail(4);;

        return $this->response->item($users, new UserTransformer)->setStatusCode(200)->addMeta('Success', 'User exists');
    }
    /**
     * Verify user credentials and generates authentication token
     *
     * @Get("/login")
     * @Versions({"v1"})
     *
     * @Request({"grant_type":"password", "client_id":"{{client_id}}", "client_secret":"{{client_secret}}", "username":"fake@fake.com", "password":"secret"})
     *
     * @Response(200, body={"access_token":"{{generated_token}}","token_type":"Bearer","expires_in":86400})
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        
        $validation = Validator::make($request->all(), [
            'username' => 'required|email', 
            'password' => 'required',
            'grant_type'    => 'required',
            'client_id'     => 'required',
            'client_secret' => 'required',
        ]);

        if ($validation->fails()) {
            
            throw new ValidationHttpException($validation->errors()->all());
        }


        if (Auth::attempt($this->getCredentials($request))) 
        {
            try 
            {
                if (! $accessToken = Authorizer::issueAccessToken()) 
                {
         
                    return $this->response->errorUnauthorized();
                }
            } 
            catch (\League\OAuth2\Server\Exception\OAuthException $e) 
            {
                throw $e;
                return $this->response->error('could_not_create_token', 500);
            }
            
            $user_id = Auth::user()->user_id;

            return response()->json(compact('accessToken', 'user_id'));
        }
        
        return $this->response->error('could not create token', 500);
    
    }

    /**
     * Get the user credentials
     *
     * @return Response
     */
    public function getCredentials(Request $request)
    {
       return [
        'email' => $request->input('username'),

        'password' => $request->input('password'),

        'verified' => true

       ];
    }
   

    public function signup(Request $request)
    {
        
        $hasToReleaseToken = Config::get('boilerplate.signup_token_release');
        
        $userData = $request->all();
        
        $validator = Validator::make($userData, Config::get('boilerplate.signup_fields_rules'));
        
        if($validator->fails()) {
           
            throw new ValidationHttpException($validator->errors()->all());
        }
        
        $user_id = User::create([
            'first_name' => $request['first_name'],
            'surname' => $request['surname'],
            'alias_name' => $request['alias_name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            ])->user_id;
       
        if(!$user_id) {

            return $this->response->error('could_not_create_user', 500);
        }

            $signed_up_user = User::findOrFail($user_id);

            $this->dispatch(new SendVerificationEmail($signed_up_user));

            $welcome_email = (new SendWelcomeEmail($signed_up_user))->delay(60 * 5);

            $this->dispatch($welcome_email);

        return $this->response->created('Success','The user has been created, confirm your email address to be verified.');
    }
    

    public function recovery(Request $request)
    {
        $validator = Validator::make($request->only('email'), [
            'email' => 'required'
        ]);
        if($validator->fails()) {
            throw new ValidationHttpException($validator->errors()->all());
        }
        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject(Config::get('boilerplate.recovery_email_subject'));
        });
        switch ($response) {
            case Password::RESET_LINK_SENT:
                return $this->response->noContent();
            case Password::INVALID_USER:
                return $this->response->errorNotFound();
        }
    }
    public function reset(Request $request)
    {
        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
        $validator = Validator::make($credentials, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);
        if($validator->fails()) {
            throw new ValidationHttpException($validator->errors()->all());
        }
        $response = Password::reset($credentials, function ($user, $password) {
            $user->password = $password;
            $user->save();
        });
        switch ($response) {
            case Password::PASSWORD_RESET:
                if(Config::get('boilerplate.reset_token_release')) {
                    return $this->login($request);
                }
                return $this->response->noContent();
            default:
                return $this->response->error('could_not_reset_password', 500);
        }
    }


    public function logout()
    {

    
        Authorizer::getChecker()->getAccessToken()->expire();

        return $this->response->created();

    }
}