<?php namespace App\Api\V1\Controllers;

use Auth;
//use Event;
use App\Rant;
use Validator;
use App\CareFactor;
//use App\Events\CareEvent;
use Illuminate\Http\Request;
use Dingo\Api\Exception\ValidationHttpException;
use App\Api\V1\Controllers\ApiBaseController as Controller;

class ApiCareFactorController extends controller{

    public function __construct()
    {
        $this->middleware('api.auth');
    }


	/**
     * authorize user and insert a rant reaction "care" or "dont care"!
     * can only choose one option at a time
     * @return redirect
     */    
    public function rantReaction(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'rant_id' => 'required|numeric',
            'carefactor' => 'required|numeric|max:1',
            ]
            );

        if ($validator->fails()) {
            
            throw new ValidationHttpException($validator->errors()->all());
        }

        $this->Care( $request->rant_id, $this->auth->user()->user_id, $request->carefactor);      
   }

   /**
    * [Care description]
    * @param [type] $rant_id     [description]
    * @param [type] $user_id     [description]
    * @param [type] $care_factor [description]
    */
   protected function Care($rant_id, $user_id, $care_factor)
   {
       $care = new CareFactor;

       $duplicate  = CareFactor::where('rant_id', '=', $rant_id)->where('user_id', '=', $user_id)->first();

       $rant_exists = Rant::where('rant_id', '=', $rant_id)->firstOrFail();

       if (!$duplicate && $rant_exists)
       {
            $care->rant_id = $rant_id;

            $care->user_id = $user_id;

            $care->care_factor = $care_factor;

            if ($care_factor == 1)
            {
                    
              $care->save();

               // Event::fire(new CareEvent($care));
               
               return $this->response->created();
            }
            else
            {
                        
                $care->save();

                return $this->response->created();
            }
        }
        else
        {
            $updateCareFactor = CareFactor::where('rant_id', '=', $rant_id)->where('user_id', '=', $user_id)
            ->update(['care_factor' =>  $care_factor]);
        }
    }
}