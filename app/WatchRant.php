<?php namespace App;


use Illuminate\Database\Eloquent\Model;

class WatchRant extends Model{


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'watch_rant';
	protected $primaryKey = 'watch_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id', 'rant_id'];


	/**
	 * get user watch.
	 *
	 * @return 
	 */
	public function scopeGetUserWatch($query, $user_id)
	{
		return $query->where('user_id', '=', $user_id)->get();
	}

	/**
     * a watch can have a rant
     */
    public function rant()
    {
        return $this->hasOne('App\Rant', 'rant_id', 'rant_id');
    }
}
