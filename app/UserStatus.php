<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_status';

	protected $primaryKey = 'user_status_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['status'];
	
	protected $hidden = [];
	
}
