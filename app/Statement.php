<?php namespace App;


use Illuminate\Database\Eloquent\Model;

class Statement extends Model {

	/**
	 * The database table used by the model.
	 *	
	 * @var string
	 */
	protected $table = 'statement';
	public $timestamps = false;
	protected $dates = ['created_date'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['manage_rally_id', 'rally_id', 'rally_statement', 'created_date'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	
	/**
	 * Set an eloquent relationship between statement and rally.
	 *
	 * @return relationship
	 */
	public function rantRally()
	{
	  return $this->belongsTo('App\RantRally', 'manage_rally_id', 'rally_id');
	}

	/**
	 * Set an eloquent relationship between rally comment and statement.
	 *
	 * @return relationship
	 */
	public function rallyComment()
	{
	  return $this->hasMany('App\rallyComment', 'statement_id', 'manage_rally_id');
	}
}
