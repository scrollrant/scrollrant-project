<?php

namespace App\Jobs;

use App\User;
use App\Rant;
use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRantBlockedEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $rant;
    //private $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Rant $rant)
    {
        //
        $this->rant = $rant;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $email = User::where('user_id', $this->rant->user_id)->select('email','first_name')->first();

       $data = [ 'email' => $email['email'], 'name' => $email['first_name'], 'rant' => $this->rant->rant ];
       $mailer->queue('emails.BlockedRant', $data, function($message) use ($data)
        {
            $message->from('no-reply@scrollrant.com', "Scroll Rant");
            $message->subject("Dayyum crap has gone down!");
            $message->to($data['email'], $data['name']);
        });
    }
}
