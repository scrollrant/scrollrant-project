<?php

namespace App\Jobs;

use Auth;
use App\Company;
use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendCompanyVerificationEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $company;
    //private $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Company $company)
    {
        //
        $this->company = $company;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
       $data = [ 'email' => $this->company->company_email, 'name' => $this->company->company_name, 'token' => $this->company->email_token,
        'verify_url' => 'verify_company_account'];
       $mailer->queue('emails.verify', $data, function($message) use ($data)
        {
            $message->from('no-reply@scrollrant.com', "Scroll Rant");
            $message->subject("ScrollRant-Verify Account");
            $message->to($data['email'], $data['name']);
        });
    }
}
