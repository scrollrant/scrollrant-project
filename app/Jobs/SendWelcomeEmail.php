<?php

namespace App\Jobs;

use Auth;
use App\User;
use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendWelcomeEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $user;
    //private $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        //
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
       $data = [ 'email' => $this->user->email, 'name' => $this->user->first_name];
       $mailer->queue('emails.Welcome', $data, function($message) use ($data)
        {
            $message->from('no-reply@scrollrant.com', "Scroll Rant");
            $message->subject("Welcome to Scroll Rant");
            $message->to($data['email'], $data['name']);
        });
    }
}
