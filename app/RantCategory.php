<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RantCategory extends Model {

	/**
	 * The database table used by the model.
	 *	
	 * @var string
	 */
	protected $table = 'rant_category';
	public $timestamps = false;
	protected $primaryKey = 'categoryID';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['categoryID', 'rantID', 'category' ];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
}
