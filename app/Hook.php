<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Hook extends Model{


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'hook';
	protected $primaryKey = 'hook_id';


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['hook_name', 'country_id', 'state_id', 'user_id', 'company_id'];


	/**
     * a hook can have a rant
     */
    public function make_hooks()
    {
        return $this->hasMany('App\MakeHook', 'hook_id', 'hook_id')->where('make_hook.created_at','>', Carbon::now()->subDay(1));
    }

	public function scopeStateTopHooks($query, $user_state)
	{
	  return $this->with('make_hooks')->where('state_id',$user_state)->get()->sortByDesc(function($hook)
		{
		    return $hook->make_hooks->count();
		})
		->take(5);
	}

	public function scopeCountryTopHooks($query, $user_country)
	{
	  return $this->with('make_hooks')->where('state_id',$user_country)->get()->sortByDesc(function($hook)
		{
		    return $hook->make_hooks->count();
		})
		->take(5);
	}
}
