<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Image;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class RantPhoto extends Model {

	/**
	 * The database table used by the model.
	 *	
	 * @var string
	 */
	protected $table = 'rant_photos';

	public $timestamps = false;

	protected $basePath = 'images/';


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['path', 'thumnail_path' ];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	

	public function rant()
	{
		return $this->belongsTo('App\Rant');
	}

	public static function named($name)
	{
		return (new static)->saveAs($name);
	}


	protected function saveAs($name)

	{
		$this->name = sprintf("%s-%s", time(), $name);

		$this->path  = sprintf("%s/%s", $this->basePath, $this->name);

		$this->thumnail_path = sprintf("%s/tn-%s", $this->basePath, $this->name);

		return $this;

	}

	public function move(UploadedFile $file)

	{
		$file->move($this->basePath, $this->name);

		//$this->makeThumnail();

		$this->makePhoto();

		return $this;

	}

	// protected function makeThumnail()

	// {
	// 	Image::make($this->path)

	// 	->fit(100)

	// 	->save($this->thumnail_path);

	// }

	protected function makePhoto()

	{
		Image::make($this->path)

		->fit(500)

		->save($this->path);

	}
}
