<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

	/**
	 * The database table used by the model.
	 *	
	 * @var string
	 */
	protected $table = 'notifications';
	public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['notification'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['user_name'];
	
	/**
	 * count the number of rants in global.
	 *
	 * @return Number
	 */
	public function scopeGetNotification($query, $today)
	{
		return $query ->where('created_date','=', $today)
        ->select('notification')
        ->get();
	}	
}
