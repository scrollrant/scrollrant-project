<?php namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class RantFace extends Model{


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'rant_face';
	protected $primaryKey = 'rant_face_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['rant_face_name', 'rant_face_path'];

}
