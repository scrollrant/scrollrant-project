<?php
namespace App\Models;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class PasswordVerifier
{
    /**
     * Request object to be injected
     *
     * @var Request
     */
    public $request;
    protected $userRepo;
    public function __construct(Request $request, User $userRepo)
    {
        $this->request = $request;
        $this->userRepo = $userRepo;
    }
    public function verify($username, $password)
    {
        $credentials = [
            'email'    => $username,
            'password' => $password,
        ];
        // Check for FB login
        if ($this->request->has('token_facebook')) {
            $user = $this->userRepo->createUser($this->request->all());
            return $user->user_id;
        }
        // For normal users
        if (Auth::once($credentials)) {
            // dd('hello its authenticated');
            return Auth::user()->user_id;
        }
        return false;
  }
}