<?php namespace App;


use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Rant extends Model {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'rant';
	protected $primaryKey = 'rant_id';
    public $timestamps = true;
    
    protected $dates = ['create_date'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['rant','topic_category_id', 'scope_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['rant_score'];

	/**
     * The hooks that belong to the rant.
     */
    public function hooks()
    {
        return $this->belongsToMany('App\Hook', 'make_hook', 'rant_id', 'hook_id');
    }

    /**
     * The user that belong to the comment.
     */
    public function comment()
    {
        return $this->belongsToMany('App\User', 'rant_comment_user', 'rant_id', 'user_id')->withPivot('rant_comment','created_date');
    }


    public function getCreatedDateAttribute()
    {
        return $this->pivot->created_date  = Carbon::createFromFormat('d/m/Y', $this)->toDateString();
    }

	/**
     * Get the comments for the blog post.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'user_id');
    }

    /**
     * a rant can have a photo
     */
    public function photo()
    {
        return $this->hasOne('App\RantPhoto');
    }

    /**
     * a rant can have one rant face
     */
    public function rant_face()
    {
        return $this->hasOne('App\RantFace', 'rant_face_id', 'rant_face_id');
    }

    /**
     * 
     */
    public function addPhoto(RantPhoto $photo)
    {
        return $this->photo()->save($photo);
    }

    /**
     * Get the comments for the rant post.
     */
    public function comments()
    {
        return $this->hasMany('App\RantCommentUser', 'rant_id', 'rant_id');
    }

    /**
     * count the number of user comments for each rant!
     * @return [type] [description]
     */
    public function userCommentsCount()
    {
      return $this->hasMany('App\RantCommentUser')
        ->selectRaw('count(*) as user_comments_count');

    }



    /**
     * Rant can have many carefactor.
     */
    public function careFactor()
    {
        return $this->hasMany('App\CareFactor', 'rant_id', 'rant_id');
    }

    /**
     * count the number of cares for each rant!
     * @return [type] [description]
     */
    public function caresCount()
    {
        return $this->hasMany('App\CareFactor')
        ->where('care_factor', 1)
        ->selectRaw('count(*) as cares_count');
    }

    /**
     * Rant can have many watchers.
     */
    public function watchers()
    {
        return $this->hasMany('App\WatchRant', 'rant_id', 'rant_id');
    }

     /**
     * count the number of watchwa for each rant!
     * @return [type] [description]
     */
    public function watchCount()
    {
      return $this->hasMany('App\WatchRant')
        ->selectRaw('count(*) as watch_count');

    }

	/**
	 * Get all of the users rants.
	 *
	 * @return Number
	 */
	public function scopeGetUserRants($query, $user_id)
	{
		return $query->where('user_id', '=', $user_id)->orderBy('rant_id', 'desc')->paginate(300);
	}

    /**
     * Get all of the state rants.
     *
     * @return Number
     */
    public function scopeGetStateRants($query, $user_state_id)
    {
        return $query->where('scope_id', '=', 3)
        ->where('state_id', $user_state_id)
        ->where('scope_id','!=', 4)
        ->where('scope_id','!=', 5)
        ->orderBy('create_date', 'desc')
        ->orderBy('rant_score', 'desc')
        ->paginate(300);
    }

    /**
     * Get all of the country rants.
     *
     * @return Number
     */
    public function scopeGetCountryRants($query, $user_country_id, $user_state_id)
    {
        return $query->where('scope_id', '=', 2)
        ->where('country_id', $user_country_id)
        ->where('state_id','!=', $user_state_id)
        ->where('scope_id','!=', 4)
        ->where('scope_id','!=', 5)
        ->orderBy('create_date', 'desc')
        ->orderBy('rant_score', 'desc')
        ->paginate(300);
    }

    /**
     * Get all of the global rants rants.
     * Newest first ordered by rant score
     * @return Number
     */
    public function scopeGetGlobalRants($query, $user_country_id)
    {
        return $query->where('scope_id', '=', 1)
        ->where('country_id','!=', $user_country_id)
        ->where('scope_id','!=', 4)
        ->where('scope_id','!=', 5)
        ->orderBy('create_date', 'desc')
        ->orderBy('rant_score', 'desc')
        ->paginate(300);
    }

	/**
	 * all the rants ever created.
	 *
	 * @return Number
	 */
	public function scopeGetAllRants($query)
	{
		return $query
        ->orderBy('create_date', 'desc')
        ->orderBy('rant_score', 'desc')
        ->paginate(3000); //needs to be changed when i push to web repo!
	}

	/**
	 * get 5 rants made by the user who's rant is being watched.
	 *
	 * @return Rant
	 */
	public function scopeGetOtherRants($query, $user_id,$status,$rant_id)
	{
		return $query->where('user_id','=',$user_id)->where('status','=', $status)
        ->where('rant_id', '!=', $rant_id)->take(5)->get();
	}

    /**
     * Increment the rant_score by one each time an event happens to the rant.
     *
     * @return nothing
     */
    public function incrementRantScore($increment_amount)
    {
        $this->rant_score += $increment_amount;

        $this->save();
    }

    /**
     * Increment the rant_score by one each time an event happens to the rant.
     *
     * @return nothing
     */
    public function decrementRantScore($decrement_amount)
    {
        $this->rant_score -= $decrement_amount;

        $this->save();
    }

    /**
     * all the top two rants for state.
     * * need to implement life time of rant which will be create_date diff current date
     *
     * @return Number
     */
    public function scopeGetTopStateRants($query, $user_state_id)
    {
        return $query->where('scope_id', '=', 3)
        ->where('create_date','>', Carbon::now()->subMinute(40) )
        ->where('state_id', '=', $user_state_id)
        ->orderBy('rant_score', 'desc')
        ->take(2)
        ->get();
    }

    /**
     * all the top two rants for country.
     *
     * @return Number
     */
    public function scopeGetTopCountryRants($query, $user_country_id)
    {
        return $query->where('scope_id', '=', 2)
        ->where('create_date','>', Carbon::now()->subDay(2))
        ->where('country_id', '=', $user_country_id)
        ->orderBy('rant_score', 'desc')
        ->take(2)
        ->get();
    }

    /**
     * all the top two rants for global.
     *
     * @return Number
     */
    public function scopeGetTopGlobalRants($query)
    {
        return $query->where('scope_id', '=', 1)
        ->where('create_date','>', Carbon::now()->subDay(4))
        ->orderBy('rant_score', 'desc')
        ->take(2)
        ->get();
    }

    /**
     * get the top country rant out of all the countryies.
     *
     * @return Number
     */
    public function scopeGetTopAllCountriesRants($query)
    {
        return $query->where('scope_id', '=', 2)
        ->where('create_date','>', Carbon::now()->subDay(2))
        ->orderBy('rant_score', 'desc')
        ->take(2)
        ->get();
    }

    /**
     * get the top state rant out of all the states.
     *
     * @return Number
     */
    public function scopeGetTopAllStatesRants($query)
    {
        return $query->where('scope_id', '=', 3)
        ->where('create_date','>', Carbon::now()->subMinute(40))
        ->orderBy('rant_score', 'desc')
        ->take(2)
        ->get();
    }



    public function getCreatedAtAttribute($attr)
    {
        return Carbon::parse($attr)->diffForHumans();
    }



}
