<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RantCommentUser extends Model {

	/**
	 * The database table used by the model.
	 *	
	 * @var string
	 */
	protected $table = 'rant_comment_user';
	protected $primaryKey = 'rant_comment_id';
	
	protected $dates = ['created_date'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['rant_comment', 'user_id', 'rant_id','created_date'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */	
	protected $hidden = [];

	/**
     * Get the comments for the blog post.
     */
    public function rant()
    {
        return $this->belongsTo('App\Rant', 'rant_id', 'rant_id');
    }

    /**
     * Get the comments for the blog post.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'user_id');
    }


	/**
	 * get the rant comment.
	 *
	 * @return Number
	 */
	public function scopeGetUserRantComment($query, $id)
	{
     	return $query->with('user')->where('rant_comment_id',$id)->first();
    }

    /**
	 * get the rant comment.
	 *
	 * @return Number
	 */
	public function scopeGetJsonRantComment($query, $rantid)
	{
     	return $query->where('rant_comment_user.rant_id', '=', $rantid)
     	->join('user', 'user.user_id', '=', 'rant_comment_user.user_id')
        ->select('user.user_id','user.first_name', 'user.surname', 'rant_comment_user.rant_comment', 'rant_comment_id')
        ->orderBy('rant_comment_user.created_date', 'asc')
        ->get(); 
    }

   	 public function getCreatedDateAttribute($attr)
    {
        return Carbon::parse($attr)->diffForHumans();
    }
}
