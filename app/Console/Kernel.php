<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // \App\Console\Commands\Inspire::class,
        \App\Console\Commands\RantReminder::class,
        \App\Console\Commands\InviteReminder::class,
        \App\Console\Commands\InvitedFriendsReminder::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('inspire')->hourly();
        $schedule->command('rant_reminder')->weekly()->sundays()->at('12:00');
        $schedule->command('invite_reminder')->weekly();
        $schedule->command('invited_friends_reminder')->weekly();
    }
}
