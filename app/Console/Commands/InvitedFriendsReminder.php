<?php

namespace App\Console\Commands;

use Artisan;
use App\Rant;
use App\User;
use Carbon\Carbon;
use App\InvitedFriends;
use Illuminate\Console\Command;
use Illuminate\Contracts\Mail\Mailer;


class InvitedFriendsReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invited_friends_reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send invite reminder e-mails to users friends and give them a glimps of their friends activity';


    protected $mail;

    protected $user;

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
    public function __construct(Mailer $mail, User $user)
    {
      parent::__construct();

      $this->mail = $mail;

      $this->user = $user->get();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      //check if the invited friend has signed up?
      //-if they have then delete the invited friend from the list
      //-if they have not signed up then send them an email every week of some
      //rants in scrollrant and some info about what their friend is up to eg -number of rants made 
      //that week, comments, watchs etc
      //send them two random rants and statistic about their friend

      foreach ($this->user as $user) 

      { 
        $friends = $user->invited_friends;

        foreach ($friends as $friend) 
        {
          $already_signed_up = User::where('email', '=', $friend->friends_email)->first();

          if ($already_signed_up == null) 
          {
            //user is not signed up yet!
            $random_rants = Rant::with(array('user'=>function($query){
              $query->select('user_id','first_name','surname', 'alias_name');
            },'photo'=>function($query){$query->select('photo_id','rant_id','path');},'rant_face'=>
            function($query){$query->select('rant_face_id','rant_face_name','rant_face_path');}))->orderByRaw("RAND()")->take(2)->select('rant_face_id','rant_id','user_id','rant','status')->get()->toArray();

            $data = [ 'email' => $friend->friends_email, 'name' => $friend->friends_name, 'random_rants' =>$random_rants];


            //dd($random_rants);
            $this->mail->queue('emails.InvitedFriendsReminder', $data, function($message) use ($data)
            { 
              $message->from('no-reply@scrollrant.com', "Scrollrant");
              $message->subject("Here are a couple of rants to read");
              $message->to($data['email'], $data['name']);
            });
          }
          else
          {
            //user has already signed up
            $user_has_signed_up = InvitedFriends::where('friends_email', '=', $friend->friends_email)->first();
            $user_has_signed_up->delete(); 
          }

        }      
      }
    }
  }
