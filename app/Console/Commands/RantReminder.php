<?php

namespace App\Console\Commands;

use Artisan;
use App\User;
use App\Rant;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Contracts\Mail\Mailer;


class RantReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rant_reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminder e-mails to a user';


    protected $mail;

    protected $rant;

    protected $user;

     /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
     public function __construct(Mailer $mail, Rant $rant, User $user)
     {
      parent::__construct();

      $this->mail = $mail;

      $this->rant = $rant;

      $this->user = $user->with(['rants' => function ($query) {
        $query->orderBy('create_date', 'desc');}])->get();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      foreach ($this->user as $user) 

      {

        if (!$user->rants->isEmpty()) 
        {
          if ($user->rants->first()->create_date > Carbon::now()->subWeek(1)) 
          {
  
            $random_rants = Rant::with(array('user'=>function($query){
              $query->select('user_id','first_name','surname', 'alias_name');
            },'photo'=>function($query){$query->select('photo_id','rant_id','path');},'rant_face'=>
            function($query){$query->select('rant_face_id','rant_face_name','rant_face_path');}))->orderByRaw("RAND()")->take(2)->select('rant_face_id','rant_id','user_id','rant','status')->get()->toArray();

            $data = ['email' => $user->email, 'name' => $user->first_name, 'random_rants' =>$random_rants];

            //dd($random_rants);
            $this->mail->queue('emails.ReminderEmail', $data, function($message) use ($data)
            { 

              $message->from('no-reply@scrollrant.com', "Scrollrant");
              $message->subject("What people are ranting about!");
              $message->to($data['email'], $data['name']);
            });
          }
        }
        else
        {
          //dd('no rants for user');
        }
      }


    }
  }
