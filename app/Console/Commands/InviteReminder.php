<?php

namespace App\Console\Commands;

use Artisan;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Contracts\Mail\Mailer;


class InviteReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invite_reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send invite reminder e-mails to a user';


    protected $mail;

    protected $user;

     /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
     public function __construct(Mailer $mail, User $user)
     {
      parent::__construct();

      $this->mail = $mail;

      $this->user = $user->get();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      foreach ($this->user as $user) 

      {

        $data = [ 'email' => $user->email, 'name' => $user->first_name];
        $this->mail->queue('emails.InviteReminder', $data, function($message) use ($data)
        { 

          $message->from('no-reply@scrollrant.com', "Scroll Rant");
          $message->subject("Help us grow");
          $message->to($data['email'], $data['name']);

        });
      }


    }
  }
