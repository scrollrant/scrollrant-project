<?php
namespace App\Console;

use App\Rant;
use Carbon\Carbon;

class RantsDemo
{
    /**
     * Get an inspiring rant.
     * Once there is enough users, select rants for that day only!!
     *
     * @return string
     */
    public static function quote()
    {
        return Rant::orderByRaw("RAND()")->where('create_date','>', Carbon::now()->subDay(1) )->select('rant')->take(1)->first();
    }
}