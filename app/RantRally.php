<?php namespace App;

use App\RallyMembers;
use Illuminate\Database\Eloquent\Model;
use DB;
class RantRally extends Model {

	/**
	 * The database table used by the model.
	 *	
	 * @var string
	 */
	protected $table = 'rant_rally';
	public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_name', 'rally_coordinator_name', 'rally_category', 'rally_title', 'rally_description' ];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['user_name'];

	/**
	 * Set an eloquent relationship between members and rallies. A rally can have many members!
	 *
	 * @return relationship
	 */
	public function rallyMembers()
	{
	  return $this->hasMany('App\RallyMembers', 'rally_id', 'rant_rally_id');
	}
	 
	 /**
	 * Count the number of members in a rally.
	 *
	 * @return count
	 */
	public function MembersCount()
	{
	  return $this->rallyMembers()
	    ->selectRaw('count(distinct rally_members.user_name) as joined_users')
	    ->groupBy('rally_id');
	}	

	/**
	 * count the number of rants in global.
	 *
	 * @return Number
	 */
	public function scopeGetUserRallies($query, $email)
	{
		return $query ->where('rant_rally.user_name','=', $email)
		->join('rally_members', 'rant_rally.rant_rally_id', '=', 'rally_members.rally_id')
        ->select('rant_rally.rally_title AS title', 'rant_rally.rally_description AS description', 'rant_rally.rant_rally_id as rally_id', DB::raw('count(distinct rally_members.user_name) as joined_users'))
        ->groupBy('rant_rally.rant_rally_id')
        ->get();
	}

	/**
	 * count the number of rants in global.
	 *
	 * @return Number
	 */
	public function scopeGetWallRallies($query, $email)
	{
		return $query ->join('rally_members', 'rant_rally.rant_rally_id', '=', 'rally_members.rally_id')
        ->select('rant_rally.rally_title AS title', 'rant_rally.rally_description AS description', 'rant_rally.rant_rally_id as rally_id', DB::raw('count(distinct rally_members.user_name) as joined_users'))
        ->where('rant_rally.user_name','!=', $email)
        // ->where('rally_members.joined_status','!=', 'blocked')
        ->groupBy('rant_rally.rant_rally_id')->get();
	}

	/**
	 * Get all the rants to be used for the public rally wall.
	 *
	 * @return Number
	 */
	public function scopeGetAllRallies($query)
	{
		return $query ->join('rally_members', 'rant_rally.rant_rally_id', '=', 'rally_members.rally_id')
        ->select('rant_rally.rally_title AS title', 'rant_rally.rally_description AS description', 'rant_rally.rant_rally_id as rally_id', DB::raw('count(distinct rally_members.user_name) as joined_users'))
        ->groupBy('rant_rally.rant_rally_id')->get();
	}

	/**
	 * count the number of rants in global.
	 *
	 * @return Number
	 */
	public function scopeGetJoinedRallies($query, $email)
	{
		return $query ->join('rally_members', 'rant_rally.rant_rally_id', '=', 'rally_members.rally_id')
        ->join('manage_rally', 'rant_rally.rant_rally_id', '=', 'manage_rally.rally_id')
        ->select('rant_rally.rally_title AS title', 'rant_rally.rally_description AS description', 'rant_rally.rant_rally_id as rally_id', 'rally_members.rally_member_id as memberid', DB::raw('count(distinct manage_rally.rally_statement) as statements_count'))
        ->groupBy('rant_rally.rant_rally_id')
        ->orderby('title','asc')
        ->where('rally_members.is_coordinator','=', "no")
        ->where('rally_members.joined_status','=', "joined")
        ->where('rally_members.user_name','=', $email)->get();
	}

	/**
	 * count the number of rants in global.
	 *
	 * @return Number
	 */
	public function scopeGetRallyToManage($query, $rallyid)
	{

		return $query ->join('user', 'rant_rally.user_name', '=', 'user.email')
        ->where('rant_rally_id', '=', $rallyid)->select('rally_title','rant_rally_id','user_name', 'user.firstname as fname', 'user.surname as sname', 'rally_coordinator_name as cname')->get();
	}

	 /**
	 * Count the number of members in a rally.
	 *
	 * @return count
	 */
	public function scopeGetCreatedRalliesCount($query, $user_name)
	{
	  return $query ->where('user_name', '=', $user_name)->count();
	}
}
