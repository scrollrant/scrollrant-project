<?php

namespace App\Events;

use App\ReportRant;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class ReportEvent extends Event 
{
    use SerializesModels;
    
    public $report;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ReportRant $report) //
    {
     
        $this->report = $report;   
    }
}
