<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DeleteWatchEvent extends Event implements ShouldBroadcast
{
    
    public $delete_watch;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $delete_watch)
    {
     
        $this->delete_watch = $delete_watch;
        
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
         return ['rant-channel'];
    }
}
