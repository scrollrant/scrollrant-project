<?php

namespace App\Events;

use App\Rant;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RantFaceEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;
    
    public $rant_face;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Rant $rant_face) //
    {
     
        $this->rant_face = $rant_face;   
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        
        return ['rant-channel'];
    }
}
