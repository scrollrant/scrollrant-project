<?php

namespace App\Events;
use Auth;
use App\RallyComment;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RallyCommentEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;
    public $comment;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $comment)
    {
         $this->comment = $comment;
    }


    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['rant-channel'];
    }
}
