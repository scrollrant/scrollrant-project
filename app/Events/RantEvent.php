<?php

namespace App\Events;

use App\Rant;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RantEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;
    public $rant;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($rant) //
    {
     
        $this->rant = $rant;   
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        
        return ['rant-channel'];
    }
}
