<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class WatchEvent extends Event implements ShouldBroadcast
{
    
    public $watch;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $watch)
    {
     
        $this->watch = $watch;
        
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
         return ['rant-channel'];
    }
}
