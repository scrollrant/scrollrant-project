<?php

namespace App\Events;

use App\NotRant;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class NotRantEvent extends Event 
{
    use SerializesModels;
    
    public $not_rant;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(NotRant $not_rant) //
    {
     
        $this->not_rant = $not_rant;   
    }
}
