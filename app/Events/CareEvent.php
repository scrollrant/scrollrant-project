<?php

namespace App\Events;

use App\CareFactor;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CareEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;
    
    public $care;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(CareFactor $care) //
    {
     
        $this->care = $care;   
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        
        return ['rant-channel'];
    }
}
