<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CommentEvent extends Event implements ShouldBroadcast
{

    public $comment;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $comment) //
    {
     
        $this->comment = $comment;   
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        
        return ['rant-channel'];
    }
}
