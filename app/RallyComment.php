<?php namespace App;


use Illuminate\Database\Eloquent\Model;

class RallyComment extends Model {

	/**
	 * The database table used by the model.
	 *	
	 * @var string
	 */
	protected $table = 'rally_comment';
	public $timestamps = false;
	protected $primaryKey = 'rally_comment_id';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['manage_comment_id', 'rally_id', 'rally_comment','statement_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['user_name', 'user_id'];
	/**
	 * Set an eloquent relationship between rally comment and statement.
	 *
	 * @return relationship
	 */
	public function manageRally()
	{
	  return $this->belongsTo('App\ManageRally', 'manage_comment_id', 'statement_id');
	}

	/**
	 * get the rally comment.
	 *
	 * @return Number
	 */
	public function scopeGetUserRallyComment($query, $id)
	{
     	return $query ->where('rally_comment.rally_comment_id', '=', $id)
     	->join('user', 'user.email', '=', 'rally_comment.user_name')
        ->select('user.userID as userID','user.firstname as fname', 'user.surname as sname','user.aliasname as alias', 'rally_comment.rally_comment as comment', 'rally_comment.rally_comment_id as commentID', 'statement_id as statementID')->first();
    }
	
}
