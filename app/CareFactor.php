<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CareFactor extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'care_factor';

	protected $primaryKey = 'care_factor_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id', 'rant_id', 'carefactor_id', 'care_factor'];
	
	protected $hidden = [];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */	
	public function scopeCaresCount($query)
	{
		return  $query-> where('care_factor', '=', 1)->count();
	}

	/**
     * Get the comments for the blog post.
     */
    public function rant()
    {
        return $this->belongsTo('App\Rant', 'rant_id', 'rant_id');
    }

}
