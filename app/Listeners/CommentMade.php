<?php

namespace App\Listeners;

use App\Rant;
use App\Http\Helpers;
use App\Events\CommentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentMade implements ShouldQueue
{
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     * Every time a comment happens update the rant's rant score by one.
     * * Need to update this method so that only increment rant score when its another user that comments.
     * @param  CommentEvent  $event
     * @return void
     */
    public function handle(CommentEvent $event)
    {
        $rant_id = $event->comment['rantID'];
        
        if ($rant_id)
        {
            Rant::whereRantId($rant_id)->firstOrFail()->incrementRantScore(1);

            // $rant = Rant::whereRantId($rant_id)->first();

            // Helpers::upgradeScope($rant);
            
        }
    }
}
