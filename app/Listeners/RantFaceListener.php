<?php

namespace App\Listeners;

use App\Rant;
use App\Http\Helpers;
use App\Events\RantFaceEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RantFaceListener implements ShouldQueue
{
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
      
    }

    /**
     * Handle the event.
     * Every time a care happens update the rant's rant score by one.
     * * Need to update this method so that only increment rant score when its another user that cares.
     * @param  CareEvent  $event
     * @return void
     */
    public function handle(RantFaceEvent $event)
    {
    
        $rant_id = $event->rant_face->rant_id;

        if ($rant_id)
        {
            Rant::whereRantId($rant_id)->firstOrFail()->incrementRantScore(1);

            // $rant = Rant::whereRantId($rant_id)->first();

            // Helpers::upgradeScope($rant);
            
        }


    }
}
