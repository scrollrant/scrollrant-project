<?php

namespace App\Listeners;

use App\Rant;
use App\NotRant;
use App\Http\Helpers;
use App\Events\NotRantEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotRantListener implements ShouldQueue
{
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
      
    }

    /**
     * Handle the event.
     * Every time a care happens update the rant's rant score by one.
     * * Need to update this method so that only increment rant score when its another user that cares.
     * @param  CareEvent  $event
     * @return void
     */
    public function handle(NotRantEvent $event)
    {
    
        $rant_id = $event->not_rant->rant_id;

        if ($rant_id)
        {
            Rant::whereRantId($rant_id)->firstOrFail()->decrementRantScore(1);

            $rant = Rant::whereRantId($rant_id)->first();

            Helpers::notRant($rant);
            
        }


    }
}
