<?php

namespace App\Listeners;

use App\Events\RallyCommentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RallyCommentMade
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RallyCommentEvent  $event
     * @return void
     */
    public function handle(RallyCommentEvent $event)
    {
        //
    }
}
