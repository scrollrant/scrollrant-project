<?php

namespace App\Listeners;

use App\Rant;
use App\Events\DeleteWatchEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteWatchListener implements ShouldQueue
{
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WatchEvent  $event
     * @return void
     */
    public function handle(DeleteWatchEvent $event)
    {
        $rant_id = $event->delete_watch['rant_id'];

        if ($rant_id)
        {
            Rant::whereRantId($rant_id)->firstOrFail()->decrementRantScore(1);          
        }

    }
}
