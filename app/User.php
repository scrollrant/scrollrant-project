<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';
	protected $primaryKey = 'user_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['first_name', 'surname', 'email', 'alias_name', 'date_of_birth','influence','country_id', 'state_id','gender_id', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token', 'surname', 'date_of_birth', 'email','user_id', 'account_status','email_token',
    'gender_id','country_id','state_id','user_status_id','influence','verified','email_token','join_date','created_at','updated_at'];

	// /**
	//  * Hash the password before storing it. THIS DOUBLE HASHES THE PASSWORD
	//  *
	//  * @var array
	//  */
	// public function setPasswordAttribute($password)
 //    {   
 //        $this->attributes['password'] = bcrypt($password);
 //    }

	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
     * User has many rants relationship.
     */
    public function rants()
    {
        return $this->hasMany('App\Rant', 'user_id', 'user_id');
    }

    /**
     * User has one state.
     */
    public function state()
    {
        return $this->hasOne('App\State', 'state_id', 'state_id');
    }

    /**
     * User has one country.
     */
    public function country()
    {
        return $this->hasOne('App\Country', 'country_id', 'country_id');
    }

    /**
     * User has one gender.
     */
    public function gender()
    {
        return $this->hasOne('App\Gender', 'gender_id', 'gender_id');
    }

    /**
     * User has one status.
     */
    public function userStatus()
    {
        return $this->hasOne('App\UserStatus', 'user_status_id', 'user_status_id');
    }

    /**
     * Get the comments for the rant post.
     */
    public function comments()
    {
        return $this->hasMany('App\RantComment', 'username', 'email');
    }

    /**
     * User can have many invited friends.
     */
    public function invited_friends()
    {
        return $this->hasMany('App\InvitedFriends', 'user_id', 'user_id');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($user){

            $user->email_token = str_random(30);

        });
    }
    /**
     * Confirm the user email address
     * @return [type] [description]
     */
    public function confirmEmail()
    {
        $this->verified = true;

        $this->email_token = null;

        $this->save();
    }

}
