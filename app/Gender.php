<?php namespace App;


use Illuminate\Database\Eloquent\Model;

class Gender extends Model{


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'gender';
	protected $primaryKey = 'gender_id';


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [ 'gender'];
}
