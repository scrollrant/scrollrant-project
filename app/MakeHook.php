<?php namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class MakeHook extends Model{


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'make_hook';
	protected $primaryKey = 'make_hook_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['rant_id', 'hook_id'];


	/**
     * a makehook can have a hook
     */
    public function hook()
    {
        return $this->belongsTo('App\Hook', 'hook_id', 'hook_id');
    }

    /**
     * a makehook can have a rant
     */
    public function rant()
    {
        return $this->belongsTo('App\Rant', 'rant_id', 'rant_id')->with('photo','user.userStatus', 'hooks', 'comment','rant_face');
    }
}
