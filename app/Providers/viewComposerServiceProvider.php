<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
class viewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeUserDetails();

        $this->composeUserWatch();

        $this->composeUserStats();

        $this->composeHook();

        $this->composeMostHooked();

        $this->composeUserRants();

        $this->composeAllRants();

        $this->composePopularRants();

        $this->composeCountries();

        $this->composeRantFaces();

        $this->composeUserLinkedAccounts();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Compose the user details
     * 
     */
    public function composeUserDetails()
    {
        view()->composer('partials.user_details', 'App\Http\Composers\UserComposer@composeUserDetails');
    }

    /**
     * Compose the user watch
     * 
     */
    public function composeUserWatch()
    {
        view()->composer('partials.notifications', 'App\Http\Composers\WatchRantComposer');
    }

    /**
     * Compose the user stats
     * 
     */
    public function composeUserStats()
    {
        view()->composer('partials.statistics', 'App\Http\Composers\StatisticsComposer');
    }

    /**
     * Compose the rant hooks
     * 
     */
    public function composeHook()
    {
        view()->composer('partials.make_rant', 'App\Http\Composers\HookComposer@composeHook');
    }

    /**
     * Compose the rant hooks
     * 
     */
    public function composeMostHooked()
    {
        view()->composer('partials.most_hooked', 'App\Http\Composers\HookComposer@composeMostHooked');
    }

    /**
     * Compose the rant hooks
     * 
     */
    public function composeUserRants()
    {
        view()->composer('profile', 'App\Http\Composers\RantComposer@composeUserRant');
    }

    /**
     * Compose the all rants
     * 
     */
    public function composeAllRants()
    {
        view()->composer('rant_wall', 'App\Http\Composers\RantComposer@composeAllRants');
    }

    /**
     * Compose the popular
     * 
     */
    public function composePopularRants()
    {
        view()->composer('partials.trending', 'App\Http\Composers\RantComposer@composePopularRants');
    }

    /**
     * Compose the rant faces and pass them to rant view
     * 
     */
    public function composeRantFaces()
    {
        view()->composer('partials.rant', 'App\Http\Composers\RantComposer@composeRantFace');
    }

     /**
     * Compose the countries
     * 
     */
    public function composeCountries()
    {
        view()->composer('partials.location_modal', 'App\Http\Composers\UserComposer@composeUserCountries');
    }

    public function composeUserLinkedAccounts()
    {
        view()->composer('partials.user_linked_accounts', 'App\Http\Composers\UserLinkedAccountsComposer@compose');

    }
}
