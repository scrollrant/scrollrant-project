<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use DateTime;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('older_than', function($attribute, $value, $parameters) {
            
            //$this->requireParameterCount(1, $parameters, 'older_than');
            $minAge = ( ! empty($parameters)) ? (int) $parameters[0] : 16;
            return (new DateTime)->diff(new DateTime($value))->y >= $minAge;
            // or the same using Carbon:
            // return Carbon\Carbon::now()->diff(new Carbon\Carbon($value))->y >= $minAge;
    });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
