<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\CareEvent' => [
            'App\Listeners\CareListener',
        ],

        'App\Events\CommentEvent' => [
            'App\Listeners\CommentMade',
        ],

        'App\Events\RallyCommentEvent' => [
            'App\Listeners\RallyCommentMade',
        ],

        'App\Events\WatchEvent' => [
            'App\Listeners\WatchListener',
        ],

        'App\Events\DeleteWatchEvent' => [
            'App\Listeners\DeleteWatchListener',
        ],

        'App\Events\ReportEvent' => [
            'App\Listeners\ReportListener',
        ],

        'App\Events\NotRantEvent' => [
            'App\Listeners\NotRantListener',
        ],

        'App\Events\RantFaceEvent' => [
            'App\Listeners\RantFaceListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
