<?php namespace App;


use Illuminate\Database\Eloquent\Model;

class SocialUser extends Model{


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'social_account_login_user';
	protected $primaryKey = 'social_account_login_id';
	public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['social_email', 'social_name', 'socialite_user_id', 'user_id', 'created_at', 'provider_name'];
	protected $hidden = ['social_email', 'social_name'];

}
