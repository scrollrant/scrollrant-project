<?php namespace App;


use Illuminate\Database\Eloquent\Model;

class NotRant extends Model{


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'not_a_rant';
	protected $primaryKey = 'not_a_rant_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id', 'rant_id'];

}
