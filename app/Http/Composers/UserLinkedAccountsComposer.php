<?php namespace  App\Http\Composers;

use Illuminate\Contracts\View\View;
use App\SocialUser;
use Auth;
class UserLinkedAccountsComposer {
	
	public function compose(View $view)
	{
		$view->with('user_linked_accounts', SocialUser::where('user_id',Auth::user()->user_id)->get());
	}

}


