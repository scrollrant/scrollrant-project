<?php namespace  App\Http\Composers;

use Illuminate\Contracts\View\View;
use App\Rant;
use Auth;
class StatisticsComposer {
	
	public function compose(View $view)
	{
		$view->with('rant_stats', Rant::GetUserRants(Auth::user()->user_id));
	}

}


