<?php namespace  App\Http\Composers;

use Auth;
use App\User;
use App\Country;
use Illuminate\Contracts\View\View;

class UserComposer {
	
	public function composeUserDetails(View $view)
	{
		$view->with('details', User::with('gender','state', 'country')->where('user_id', Auth::user()->user_id)->get());
	}

	public function composeUserCountries(View $view)
	{
		$view->with('country', Country::all()->lists('country_id', 'country_name'));
	}

}


