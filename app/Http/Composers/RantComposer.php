<?php namespace  App\Http\Composers;

use Auth;
use App\Rant;
use App\RantFace;
use Illuminate\Contracts\View\View;

class RantComposer {

	 protected $rants;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Rant $rants)
    {
        // Dependencies automatically resolved by service container...
        $this->rants = $rants;
    }
	
    /**
     * compose the user rants for the user profile
     * @param  View   $view 
     * @return view->with('user_rants', $rants)
     */
	public function composeUserRant(View $view)
	{
		
		$rant = $this->rants->with('photo','user.userStatus', 'careFactor', 'hooks', 'comment','watchers','rant_face')->GetUserRants(Auth::user()->user_id);

		$view->with('user_rants', $rant );
	}

    /**
     * compose the all rants for the rants page
     * @param  View   $view 
     * @return view->with('all_rants', $rants)
     */
    public function composeAllRants(View $view)
    {
        if (Auth::user()->state_id  && Auth::user()->country_id != Null )
        {
            $state_rant = $this->rants->with('photo','user', 'careFactor', 'hooks', 'comment')
            ->GetStateRants(Auth::user()->state_id);

            $country_rant = $this->rants->with('photo','user', 'careFactor', 'hooks', 'comment')
            ->GetCountryRants(Auth::user()->country_id, Auth::user()->state_id );

            $global_rant = $this->rants->with('photo','user', 'careFactor', 'hooks', 'comment')
            ->GetGlobalRants(Auth::user()->country_id);

            $rant = $global_rant->merge($country_rant)->merge($state_rant);
        }
        else
        {
            $rant = $this->rants->with('photo','user', 'careFactor', 'hooks', 'comment')
            ->GetAllRants();
        }
        

        $view->with('all_rants', $rant );
    }

    /**
     * compose the popular rants for the side menu
     * @param  View   $view 
     * @return view->with('all_rants', $rants)
     */
    public function composePopularRants(View $view)
    {
        if (Auth::user()->state_id  && Auth::user()->country_id != Null )
        {
            $state_rant = $this->rants->GetTopStateRants(Auth::user()->state_id);

            $country_rant = $this->rants->GetTopCountryRants(Auth::user()->country_id);

            $global_rant = $this->rants->GetTopGlobalRants();

            $popular_rants = $global_rant->merge($country_rant)->merge($state_rant);
        }
        else
        {
            $state_rant = $this->rants->GetTopAllStatesRants();

            $country_rant = $this->rants->GetTopAllCountriesRants();

            $global_rant = $this->rants->GetTopGlobalRants();

            $popular_rants = $global_rant->merge($country_rant)->merge($state_rant);

        }

        $view->with('popular_rants', $popular_rants );
    }

    public function composeRantFace(View $view)
    {

        $rant_faces = RantFace::all();

        $view->with('rant_faces', $rant_faces );
    }


}


