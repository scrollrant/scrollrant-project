<?php namespace  App\Http\Composers;

use Auth;
use App\Hook;
use App\MakeHook;
use Illuminate\Contracts\View\View;

class HookComposer {
	
	public function composeHook(View $view)
	{
		$view->with('hook', Hook::orderBy('hook_name', 'asc')->get());
	}


	public function composeMostHooked(View $view)
	{

		$top_state_hooks = Hook::StateTopHooks(Auth::user()->state_id);

		$top_country_hooks = Hook::CountryTopHooks(Auth::user()->country_id);

		$top_hooks = $top_country_hooks->merge($top_state_hooks);

		$view->with('most_hooked', $top_hooks); 
	}

}


