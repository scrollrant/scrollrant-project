<?php namespace  App\Http\Composers;

use Illuminate\Contracts\View\View;
use App\WatchRant;
use Auth;
class WatchRantComposer {
	
	public function compose(View $view)
	{
		$view->with('watch', WatchRant::GetUserWatch(Auth::user()->user_id));
	}

}


