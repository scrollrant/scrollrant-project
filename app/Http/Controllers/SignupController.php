<?php namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use App\Hook;
use Response;
use Validator;
use App\State;
use App\Company;
use App\Country;
use Illuminate\Http\Request;
use App\Jobs\SendWelcomeEmail;
use App\Jobs\SendVerificationEmail;
use App\Jobs\SendCompanyWelcomeEmail;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\SignupFormRequest;
use App\Jobs\SendCompanyVerificationEmail;

class SignupController extends controller{

    /**
     * Get the signup form
     *      
     * Get all the countries in the database to populate dropdown for country selection.
     *
     * @return Json view with country list
     */
    public function getSignupForm()
    {
    
        return view('auth/sign_up');
    }

    /**
     * Get the company signup form
     *      
     * Get all the countries in the database to populate dropdown for country selection.
     *
     * @return Json view with country list
     */
    public function getCompanySignupForm()
    {
    
        return view('auth/company_signup');
    }

    /**
     * Sign up user and go to their profile
     *
     * Email user welcome email
     *
     * @return redirect them to profile with session message
     *
     * @param SignupFormRequest
     */
    public function signupUser(SignupFormRequest $request)
    { 

        $id = User::create([
            'first_name' => $request['first_name'],
            'surname' => $request['surname'],
            'alias_name' => $request['alias_name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            ])->user_id;

        
            $signed_up_user = User::findOrFail($id);

            $this->dispatch(new SendVerificationEmail($signed_up_user));

            $welcome_email = (new SendWelcomeEmail($signed_up_user))->delay(60 * 5);

            $this->dispatch($welcome_email);
            
            Session::flash('message',"Thanks for signing up, we have emailed you a verification link. Go click it to activate your account.");

            return redirect()->back();
           
    }

    /**
     * Sign up a company 
     *
     *
     * @return redirect them to profile with session message
     *
     * @param SignupFormRequest
     */
    public function signupCompany(Request $request)
    { 
        $this->validate($request, [
            'company_email' => 'required|email|max:255|unique:company,company_email', 'company_password' => 'required',
            'company_name' => 'required'
        ]);

        $id = Company::create([
            'company_email' => $request['company_email'],
            'company_name' => $request['company_name'],
            'password' => bcrypt($request['company_password']),
            ])->company_id;

       
            $signed_up_company = Company::findOrFail($id);

            $this->dispatch(new SendCompanyVerificationEmail($signed_up_company));

            $welcome_email = (new SendCompanyWelcomeEmail($signed_up_company))->delay(60 * 5);

            $this->dispatch($welcome_email);
            
            Session::flash('message',"Thanks for signing up");

            return redirect()->back();          
    }



    /**
     * Confirm the user email to activate account
     *
     * @return Json array
     *
     * @param Request
     */
    public function confirmEmail($token)
    {
        $user = User::whereEmailToken($token)->firstOrFail()->confirmEmail();

        Auth::logout();

        Session::flash('message',"Your account has been verified, you may now login");


        return new RedirectResponse(url('login'));
    }

    /**
     * Confirm the company email to activate account
     *
     * @return Json array
     *
     * @param Request
     */
    public function confirmCompanyEmail($token)
    {
        $company = Company::whereEmailToken($token)->firstOrFail()->confirmEmail();

        Auth::logout();

        Session::flash('message',"Company account has been verified, you may now login");

        return new RedirectResponse(url('company_login'));
    }

    /**
     * Get all the states in the database to populate dropdown for state based on countryID selection.
     *
     * @return Json array
     *
     * @param Request
     */
    public function getCountries(Request $request)
    {
        $country_list = Country::all()->lists('country_id', 'country_name');
        
        return Response::json($country_list);
    }

    /**
     * Get all the states in the database to populate dropdown for state based on countryID selection.
     *
     * @return Json array
     *
     * @param Request
     */
    public function getStates(Request $request)
    {
        $input = $request->input('country');

        $state_list = State::where('country_id', '=', $input)->lists('state_name', 'state_id');

        return Response::json($state_list);
    }

    /**
     * Check if the alias name is already in the database. Function used for jquery validation.
     *
     * @return json
     *
     * @param aliasname
     */
    public function aliasExists($aliasname)
    {
       
        $alias_name = User::where('alias_name', '=', $aliasname)->first();
        
        if ($alias_name) 
        {
            return Response::json(true);
        } 
        
        else 

        {
            return Response::json(false);
        }
    }

    /**
     * Check if the email is already in the database. Function used for jquery validation.
     *
     * @return json
     *
     * @param email
     */
    public function emailExists($email)
    {
       
        $email = User::where('email', '=', $email)->first();
        
        if ($email) 
        {
            return Response::json(true);
        } 

        else 

        {
            return Response::json(false);
        }
    }

    /**
     * Check if the company email is already in the database. Function used for jquery validation.
     *
     * @return json
     *
     * @param email
     */
    public function companyEmailExists($email)
    {
       
        $email = Company::where('company_email', '=', $email)->first();
        
        if ($email) 
        {
            return Response::json(true);
        } 

        else 

        {
            return Response::json(false);
        }
    }

     /**
     * Check if the company name is already in the database. Function used for jquery validation.
     *
     * @return json
     *
     * @param email
     */
    public function companyNameExists($name)
    {
       
        $company_name = Company::where('company_name', '=', $name)->first();
        
        if ($company_name) 
        {
            return Response::json(true);
        } 

        else 

        {
            return Response::json(false);
        }
    }   
}
?>