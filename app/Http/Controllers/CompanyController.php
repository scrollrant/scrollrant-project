<?php namespace App\Http\Controllers;

use Auth;
use Event;
use App\Company;
use App\Rant;
use Illuminate\Http\Request;
use App\Events\RantFaceEvent;
use Illuminate\Routing\Controller;
use Illuminate\Http\RedirectResponse;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\GenderFormRequest;
use App\Http\Requests\LocationFormRequest;
use App\Http\Requests\BirthdayFormRequest;

class CompanyController extends controller{

    
    public function __construct()
    {
    	 $this->user = "company";

        $this->middleware('company');
    }


    /**
     * Authorize the company and redirct them to their profile.
     *
     * @return view(profile) with array
     */
    public function getCompanyDetails(Request $request)
    {

        return view('company_profile');    
    }


    /**
     * Log the user out
     * 
     * @return redirect
     */   
    public function logOutCompany()
    {
        Auth::logout('company');

        return new RedirectResponse(url('home'));
    }

    


}