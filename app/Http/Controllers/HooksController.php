<?php namespace App\Http\Controllers;

use Auth;
use App\Hook;
use App\Rant;
use Response;
use App\MakeHook;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Http\RedirectResponse;

class HooksController extends controller{

    public function __construct()
    {
       
    }

    /**
     * [test description]
     * @param  [type] $id   [description]
     * @param  [type] $name [description]
     * @return [type]       [description]
     */
    public function getHooks($id, $name){


       $make_hook =  MakeHook::where('hook_id', '=', $id)->get();
       $hooked_rants = collect([]);

       $make_hook->load(['rant' => function ($q) use ( &$hooked_rants ) {
            
            if (Auth::check())
            {
                $hooked_rants = $q->get();
            }
            else{

                $hooked_rants = $q->take(5)->get();
            }
                
        }]);

        return view('hooks', compact('hooked_rants'));
    }

            /**
     * Get all the hooks for search functionality
     *
     * @return Json array
     *
     * @param Request
     */
    public function searchHooks($hook_name)
    {
        $hook_list = Hook::where('hook_name', 'like', '%'.$hook_name.'%')->select( 'hook_id','hook_name')->get();
        
        $hook_list->map(function ($item, $key) {
            return $item['html_url'] = 'https://scrollrant.com/hooked/'.$item['hook_id'].'/'.$item['hook_name'];  
        });

        return Response::json(['results' => $hook_list]);
    }

    //other controller methods...
}

?>