<?php namespace App\Http\Controllers;

use Auth;
use Mail;
use Session;
use App\User;
use Validator;
use App\SocialUser;
use App\InvitedFriends;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\FeedbackFormRequest;

class ManageAccountController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application feedback form to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('manage_account');
	}

	/**
	 * Send the user feedback to the coordinator
	 * @param  FeedbackFormRequest $request [description]
	 * @return [type]                       [description]
	 */
	public function sendFeedback(FeedbackFormRequest $request)
	{
		$type;
		
		if($request->feedback_type == 01)
		{
			$type= 'Feature Sugguestion';
		}

		elseif ($request->feedback_type == 02) 
			
		{
			$type= 'Functionality';
		}

		elseif ($request->feedback_type == 03) 

		{
			$type= 'Community Evaluation';
		}

		else
			
		{
			$type= 'Bug';
		}

		$data = [ 'email' => 'aleer_soccerboy@hotmail.com', 'name' => $request->name, 'user' => Auth::user()->email, 'user_message' => $request->message, 'type' => $type];
		
		Mail::queue('emails.Feedback',$data, function($message) use ($data)
		{
			$message->from('no-reply@scrollrant.com', "Scroll Rant");
			$message->subject("Feedback");
			$message->to($data['email']);
		});
		
		Session::flash('message', 'Thank you for your feedback'.' '.Auth::user()->first_name);
		
		return new RedirectResponse(url('profile'));
	}

	public function inviteFriend(Request $request)
	{
		$messages = [
		'friend_email.unique' => 'This friend has already been invited, see if you can invite a new one!',
		];
		$this->validate($request, [
			'friend_email' => 'required|email|unique:invited_friends,friends_email', 'friend_name' => 'required',
			], $messages);

		$invited_friend = new InvitedFriends;
		$invited_friend->friends_name = $request->friend_name;
		$invited_friend->friends_email = $request->friend_email;
		$invited_friend->user_id = Auth::user()->user_id;

		$already_signed_up = User::where('email', '=', $request->friend_email)->first();
		if ($already_signed_up == null) 
		{
			$invited_friend->save();

			$data = [ 'email' => $request->friend_email, 'name' => $request->friend_name, 'user_firstname' => Auth::user()->first_name,  'user_surname' => Auth::user()->surname ];
			
			Mail::queue('emails.Invite',$data, function($message) use ($data)
			{
				$message->from('no-reply@scrollrant.com', "Scrollrant");
				$message->subject($data['user_firstname'].' '.$data['user_surname'].' invited you to Scrollrant.');
				$message->to($data['email']);
			});

			Session::flash('message', 'Thank you for inviting a friend'.' '.Auth::user()->first_name);

			return new RedirectResponse(url('profile'));
		}

		return redirect()->back()->withErrors(['email' => 'This friend has already signed up, try another friend']);   		
	}

	public function unlinkAccounts(Request $request)
	{
		//
		$validator = Validator::make($request->all(), [
			'social_account_id' => 'required|numeric',
			]
			);

		$social_user = SocialUser::where('social_account_login_id', $request->social_account_id)->first();


		if ($social_user->user_id === Auth::user()->user_id) 
		{
        	$social_user->delete();
		}

		return redirect()->intended('manage');
	}

}
