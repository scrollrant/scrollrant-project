<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Auth;
use App\Rant;
use App\RantComment;
use App\User;
use Session;
use DateTime;
use DB;
use App\CareFactor;
use App\Http\Requests\ReactionFormRequest;
use App\Http\Requests\CommentFormRequest;
use App\Http\Requests\ShowCommentFormRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Routing\Controller;
use Illuminate\Http\RedirectResponse;
use Event;
use App\Events\CommentEvent;
//REFACTORED 1/9/2015
class RantWallController extends controller{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * get the current users id so it can be used to insert a reaction!
     * get the user state and country 
     * set the scopes for the where clause
     * get the date to be used to select the current date's rants only
     * select state rants, country rants and global rants in different variables and pass to view
     * @return view with array
     */
    public function getRantWall()
    {
  
        $all_rants = Rant::get();
    
        return view('rant_wall',compact('all_rants')); 
    }

     
    //other controller methods...
}

?>