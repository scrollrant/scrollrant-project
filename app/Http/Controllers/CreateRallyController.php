<?php namespace App\Http\Controllers;
use App\RantRally;
use App\RallyMembers;
use Illuminate\Http\Request;
use Response;
use Auth;
//use Session;
use Illuminate\Routing\Controller;
use App\Http\Requests\CreateRallyFormRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\RedirectResponse;
//REFACTORED 1/9/2015
class CreateRallyController extends controller{
   
  public function __construct()
  {
     $this->middleware('auth');
  } 
   
  /**
   * show the create crally page if a user existers
   *
   * @return view(create_rally) with array
  */
  public function getRantRallyForm()
  {

      return view('create_rally');

  }

  /**
   * authorize the user and create the rally.
   *
   * @return view(profile) with message
  */
  public function createRally(CreateRallyFormRequest $request)
  {
    $user_name = Auth::user()->email;
   // $created_rallies = RantRally::GetCreatedRalliesCount($user_name);
   // if($created_rallies <= 2)
   // {
      $rant_rally = new RantRally;
      $data = array('user_name'=>Auth::user()->email,
        'rally_coordinator_name'=> $request->coordinator_name,
        'rally_title'=>$request->rally_title, 
        'rally_description'=>$request->rally_description);  
      $id = $rant_rally->insertGetId($data);
      $is_coordinator = "yes";
      $rally_member = new RallyMembers;
      $rally_member->rally_id = $id;
      $rally_member->user_name = Auth::user()->email;
      $rally_member->is_coordinator = $is_coordinator;
      $rally_member->save();
      return redirect('profile')->with('message', 'New Rally Created');
   /* }
    else
      { 
        //Session::flash('message', 'You can not create more than 3 rallies');
        return Redirect::back()->with('message', 'You can not create more than 3 rallies');
      }*/
  }

  //other controller methods...
}
    

?>