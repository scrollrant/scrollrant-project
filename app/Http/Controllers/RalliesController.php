<?php namespace App\Http\Controllers;
use App\RantRally;
use App\ManageRally;
use App\RallyComment;
use App\RallyMembers;
use App\User;
use Illuminate\Http\Request;
use Response;
use Session;
use Auth;
use DB;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\RantFormRequest;
use App\Http\Requests\StatementFormRequest;
use App\Http\Requests\StatementCommentFormRequest;
use Illuminate\Http\RedirectResponse;
use Event;
use App\Events\RallyCommentEvent;
//REFACTORED 1/9/2015
class RalliesController extends controller{

    public function __construct()
    {
        $this->middleware('auth',['except' => 'getAllRallies']);
    }

    /**
     * authorize the user and get the users rallies.
     *
     * @param Request $request
     *
     * @return view(my_rallies) with array
     */
    public function getRallyDetails(Request $request)
    {

        $email= Auth::user()->email;
        $user_rallies = RantRally::GetUserRallies($email);

        return view('my_rallies', compact('user_rallies'));
    }

     /**
     * authorize the user and get all the rallies. the user will be able to join rallies if logged in but just view them if not.
     *
     * @param Request $request
     *
     * @return view(rally_wall) with array
     */
    public function getAllRallies(Request $request)
    {
        if (Auth::check())
        {
            $email= Auth::user()->email;

            $wall_rallies = RantRally::GetWallRallies($email);

            return view('rally_wall', compact('wall_rallies'));
        }
        else
        {
        
            $wall_rallies = RantRally::GetAllRallies();

            Session::flash('message', 'Log in if you want to join rallies, its fun and easy!');
            return view('rally_wall', compact('wall_rallies'));
        }
    }

     /**
     * authorize the user and get the users joined rallies.
     *
     * @param Request $request
     *
     * @return view(joined_rallies) with array
     */
    public function joinedRallies(Request $request)
    {

        $email= Auth::user()->email;

        $user_Joined_rallies = RantRally::GetJoinedRallies($email);

        return view('joined_rallies', compact('user_Joined_rallies'));
    }

     /**
     * get the rally that the user wants to manage. the title ,id and user name will be used to insert statements for that rally.
     *
     * @param rallyid
     *
     * @return view(manage_rally) with array
     */
    public function manageRally($rallyid)
    {
        $email= Auth::user()->email;

        $user_id = User::where('email','=', $email)->select('userID')->first();
        
        $rally = RantRally::GetRallyToManage($rallyid);

        $coordinator_statements = ManageRally::leftjoin('rally_comment', 'rally_comment.statement_id', '=', 'manage_rally.manage_rally_id')
        ->where('manage_rally.rally_id', '=', $rallyid)
        ->select('manage_rally.*' ,DB::raw('count(distinct rally_comment.rally_comment_id) as comments_count'))
        ->orderby('created_date','desc')
        ->groupBy('created_date')
        ->get();

        return view('manage_rally', compact('rally','email', 'user_id', 'coordinator_statements'));
    }

    /**
     * insert a new statement to a rally, only coordinator can do this.
     *
     * @param StatementFormRequest $request, rallyid
     *
     * @return view(view_rally) with array
     */
    public function insertStatement($rallyid, StatementFormRequest $request)
    {        
        $rally_statement = new ManageRally;
        $rally_statement->rally_id = $rallyid;
        $rally_statement->rally_statement = $request->rally_statement;
        $rally_statement->save();
        return new RedirectResponse(url('view_rally', compact('rallyid')));
    }

     /**
     * authorize user and insert a new statement comment to a statement. then broadcast the event
     *
     * @param StatementCommentFormRequest $request
     *
     * @return redirect
     */
    public function insertStatementComment(StatementCommentFormRequest $request)
    {
 
        $statementcomment = new RallyComment;

        $data = array('user_name'=>Auth::user()->email,
                'rally_id'=> $request->rally_id,
                'user_id'=>$request->user_id, 
                'statement_id'=>$request->statement_id,
                'rally_comment'=>$request->comment);
        $id = $statementcomment->insertGetId($data);

        $userRallyComment = RallyComment::GetUserRallyComment($id);
        $statement_comment = ['firstname' => $userRallyComment['fname'],
                 'surname' => $userRallyComment['sname'],
                'comment' => $userRallyComment['comment'],
                'statement_id' => $userRallyComment['statementID'],
                'count' => '1'
                ];

        Event::fire(new RallyCommentEvent($statement_comment));
        return Redirect::back();      
    }

    /**
     * authorize user and delete a rally based on rallyid. 
     * Only the coordinator can delete their own rallies!
     *
     * @param $rallyid
     *
     * @return redirect
     */
    public function destroy($rallyid)
    {
        if (Auth::check())
        {
            $rally = RantRally::where('rant_rally_id','=',$rallyid);
            $rally->delete();

            Session::flash('message', 'Successfully deleted the Rally!');
            return new RedirectResponse(url('my_rallies')); 
        }
    }
    
    /**
     * authorize user and delete a statement based on statementid. 
     * only coordinator can see and delete statements
     *
     * @param  $statementid
     *
     * @return redirect
     */
    public function destroyStatement($statementid)
    {

            $rally = ManageRally::where('manage_rally_id','=',$statementid);
            $rally->delete();

            Session::flash('message', 'Successfully deleted the Statement!');
            return Redirect::back(); 
    }

    /**
     * authorize user and delete or (unjoin) from a rally. 
     * the relationship will be deleted but no cascding effect should take place.
     *
     * @param $memberid
     *
     * @return redirect
     */
    public function leaveRally($memberid)
    {

            $member = RallyMembers::where('rally_member_id','=',$memberid);
            $member->delete();

            Session::flash('message', 'Successfully left the rally!');
            return Redirect::back(); 
    }

    /**
     * allow the user to join a rally. 
     * 
     * @param $rallyid
     *
     * @return redirect
     */
    public function joinRally($rallyid)
    {
        $email= Auth::user()->email;
        $duplicate = RallyMembers::where('rally_id', '=', $rallyid)->where('user_name', '=', $email)->first();
        $blocked = RallyMembers::where('joined_status', '=', "blocked")->where('user_name', '=', $email)->first();
        $rally_memeber = new RallyMembers;
        $rally_memeber->rally_id = $rallyid;
        $rally_memeber->user_name = $email;
        if(!$duplicate)
        {
            $rally_memeber->joined_status = "joined";
            $rally_memeber->save();
            Session::flash('message', 'Successfully joined the Rally!');
            return Redirect::back(); 
        }
        else if($blocked)
        {
            Session::flash('message', 'You have been blocked from this Rally!');
            return Redirect::back();
        }
        else
        {
            Session::flash('message', 'Already joined the Rally!');
            return Redirect::back();
        }
    }

    /**
     * get all the statement comments based on statement id. function used by jquery. 
     * 
     * @param $statementid
     *
     * @return json
     */
    public function getRallyComments($statementid)
    {
        $rally_comments = RallyComment::join('user', 'user.userID', '=', 'rally_comment.user_id')
        ->join('rant_rally', 'rant_rally.rant_rally_id', '=', 'rally_comment.rally_id')
        ->where('rally_comment.statement_id', '=', $statementid)
        ->select('user.userID','user.firstname', 'user.surname','user.aliasname', 'rally_comment.rally_comment', 'rally_comment_id', 'rant_rally.rally_coordinator_name as cname')
        ->orderBy('rally_comment.created_date', 'asc')
        ->get();          
        return Response::json($rally_comments);     
    }
 
    //other controller methods...
}

?>