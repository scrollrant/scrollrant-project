<?php namespace App\Http\Controllers;

use Auth;

use Event;
use App\Rant;
use App\MakeHook;
use App\RantPhoto;
use Carbon\Carbon;
use App\Events\RantEvent;
use Illuminate\Routing\Controller;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\RantFormRequest;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ReactionFormRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class RantController extends controller{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * authorize the user and then allow them to insert a rant.
     * 
     * @return redirect
     */
    public function createRant(RantFormRequest $request)
    
    {

        $rants = new Rant;

        $data = array('user_id'=>Auth::user()->user_id,

        'rant'=> $request->rant,
        'country_id'=> Auth::user()->country_id,
        'state_id'=> Auth::user()->state_id,
        'created_at'=> Carbon::now(),
        ); 

        $id = $rants->insertGetId($data);

        Event::fire(new RantEvent($id));
        
        if($request->file('file'))

        {

            $photo = $this->makePhoto($request->file('file'));

            Rant::findorFail($id)->addPhoto($photo);
        }

        if ($request->hook_id)
        {

            $this->makeHook($id, $request->hook_id);
        }


        return new RedirectResponse(url('profile'));   
    }

    /**
     * [makePhoto description]
     * @param  UploadedFile $file [description]
     * @return [type]             [description]
     */
    protected function makePhoto(UploadedFile $file)
    
    {
        return  RantPhoto::named($file->getClientOriginalName())

        ->move($file);
    }

    /**
     * [makeHook description]
     * @param  [type] $rant_id [description]
     * @param  [type] $hook_id [description]
     * @return [type]          [description]
     */
    protected function makeHook($rant_id, $hook_id)
    {
        $make_hook = new MakeHook;

        $make_hook->rant_id = $rant_id;

        $make_hook->hook_id = $hook_id;

        $make_hook->save();

        $update_rant_status = Rant::where('rant_id', '=', $rant_id)->update(['status' =>  'public']);
    }

}