<?php namespace App\Http\Controllers;

use Auth;
use Event;
use App\User;
use App\Rant;
use Illuminate\Http\Request;
use App\Events\RantFaceEvent;
use Illuminate\Routing\Controller;
use Illuminate\Http\RedirectResponse;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\GenderFormRequest;
use App\Http\Requests\LocationFormRequest;
use App\Http\Requests\BirthdayFormRequest;

class UserController extends controller{

    
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Update the user gender_id column of the user table
     * @param  GenderFormRequest $request
     * @return Redirect::back()
     */
    public function updateGender(GenderFormRequest $request)
    {   
        $id = Auth::user()->user_id;

        $update_user = User::find($id);

        $update_user->gender_id = $request->gender;

        $update_user->save();

        return Redirect::back();
    }

    /**
     * Update the country_id and state_id column of the user table
     * @param  LocationFormRequest $request 
     * @return Redirect::back()
     */
    public function updateLocation(LocationFormRequest $request)
    {   
        $id = Auth::user()->user_id;

        $update_user = User::find($id);

        $update_user->country_id = $request->country_id;

        $update_user->state_id = $request->state_id;

        $update_user->save();

        $this->updateRantLocation($id, $update_user->country_id, $update_user->state_id);

        return Redirect::back();
    }

    private function updateRantLocation($user_id, $country_id, $state_id)
    {
        $update_rant_location = Rant::where('user_id',$user_id)
        ->update(array('country_id' => $country_id, 'state_id' => $state_id));

    }

    /**
     * Update the user date_of_birth column of the user table
     * @param  GenderFormRequest $request
     * @return Redirect::back()
     */
    public function updateBirthday(BirthdayFormRequest $request)
    {   
        $id = Auth::user()->user_id;

        $update_user = User::find($id);

        $update_user->date_of_birth = $request->date_of_birth;

        $update_user->save();

        return Redirect::back();
    }

    /**
     * allows the user to pick a rant face for their rant!
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateRantFace(Request $request)
    {

        $update_rant_face = Rant::find($request->rant_id);

        if($update_rant_face->user_id == Auth::user()->user_id && $request->rant_face_id != Null)
        {
           $update_rant_face->rant_face_id = $request->rant_face_id; 

           $update_rant_face->save();

           Event::fire(new RantFaceEvent($update_rant_face));
        }
        
        return Redirect::back() ;
    }

}