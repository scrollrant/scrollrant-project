<?php namespace App\Http\Controllers;

use Auth;
use Event;
use App\Rant;
use Carbon\Carbon;
use App\RantCommentUser;
use App\Events\CommentEvent;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\CommentFormRequest;

class RantCommentController extends controller{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * authorize the user and then allow them to insert a rant commment.
     *
     * fire a new event that accepts array of data
     *@param CommentFormRequest $request
     *
     * @return redirect
     */
    public function createRantCommentUser(CommentFormRequest $request)
    {

        $rantcomment = new RantCommentUser;

        $data = array(
            'user_id'=>$request->user_id, 
            'rant_id'=>$request->rant_id,
            'rant_comment'=>$request->comment,
            'created_at'=> Carbon::now(),
            'created_date'=> Carbon::now());          
        $id = $rantcomment->insertGetId($data);

        $userRantComment = RantCommentUser::GetUserRantComment($id);

        $rant =  ['firstname' => $userRantComment->user->first_name,
        'surname' => $userRantComment->user->surname,
        'comment' => $userRantComment['rant_comment'],
        'rantID' => $userRantComment['rant_id'],
        'create_date' => $userRantComment['created_date']
        ];

        

        Event::fire(new CommentEvent($rant));

        return redirect()->back();
    }

    /**
     * get all the comments based on rantid. Function is used by the jquery
     *
     *@param  $rantid
     *
     * @return json
     */
    public function getComments($rantid)
    {
            
        $comments = RantComment::GetJsonRantComment($rantid);

        return Response::json($comments);
    }

    //other controller methods...
}

?>