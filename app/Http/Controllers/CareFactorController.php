<?php namespace App\Http\Controllers;

use Auth;
use Event;
use Session;
use App\Rant;
use App\CareFactor;
use App\Events\CareEvent;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\CareFactorFormRequest;

class CareFactorController extends controller{

    public function __construct()
    {
        $this->middleware('auth');
    }


	/**
     * authorize user and insert a rant reaction "care" or "dont care"!
     * can only choose one option at a time
     * @return redirect
     */    
    public function rantReaction(CareFactorFormRequest $request)
    {   
        if ($request)

        {
           $this->Care( $request->rant_id, $request->user_id, $request->carefactor);
       }


       return redirect()->back();
   }

   protected function Care($rant_id, $user_id, $care_factor)
   {
       $care = new CareFactor;

       $duplicate  = CareFactor::where('rant_id', '=', $rant_id)->where('user_id', '=', $user_id)->first();

       $rant_exists = Rant::where('rant_id', '=', $rant_id)->firstOrFail();

       if (!$duplicate && $rant_exists)
       {
            $care->rant_id = $rant_id;

            $care->user_id = $user_id;

            $care->care_factor = $care_factor;

            if ($care_factor == 1)
            {
                Session::flash('message', 'Successfully Care');
                
                $care->save();

                Event::fire(new CareEvent($care));
            }
            else
            {
                Session::flash('message', 'Successfully Dont Care');
                
                $care->save();

            }
        }
        else
        {
            $updateCareFactor = CareFactor::where('rant_id', '=', $rant_id)->where('user_id', '=', $user_id)
            ->update(['care_factor' =>  $care_factor]);
        }
    }
}