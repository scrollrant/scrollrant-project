<?php namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Rant;
use App\CareFactor;
use App\MakeHook;
//use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Http\RedirectResponse;

class ProfileController extends controller{

    public function __construct()
    {
        $this->user = "user";

        $this->middleware('auth');
    }

    /**
     * Authorize the user and get the users details.
     *
     * @return view(profile) with array
     */
    public function getUserDetails(Request $request)
    {

        return view('profile');    
    }

    /**
     * Log the user out
     * 
     * @return redirect
     */   
    public function logOutUser()
    {
        Auth::logout();

        return new RedirectResponse(url('home'));
    }
    
    //other controller methods...
}

?>