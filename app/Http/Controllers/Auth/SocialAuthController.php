<?php namespace App\Http\Controllers\Auth;


use Illuminate\Http\Request;
use App\Http\AuthenticateUser;
use App\Http\Controllers\Controller;

class SocialAuthController extends Controller
{

	public function login(AuthenticateUser $authenticateUser, Request $request)
	{
		$authenticateUser->execute($request->has('code'));
	}



}