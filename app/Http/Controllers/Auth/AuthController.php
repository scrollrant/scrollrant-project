<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Session;
use Validator;
use Illuminate\Http\Request;
use App\Http\AuthenticateSocialUser;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Sarav\Multiauth\Foundation\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    //when a user is successfully logged in they are redirected to profile
    protected $redirectPath = '/profile';
    //when a user fails log in
    protected $loginPath = '/login';
    //control the number of attemped logins before lock, default is 5
    private $maxLoginAttempts = 5;
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = "user";
        
        $this->middleware('guest', ['except' => ['handleProviderCallback', 'getLogin']]);
    }

    /**
     * Socialite login for the user and linking social accounts.
     *
     * @return Response
     */
    public function handleProviderCallback($provider= null, AuthenticateSocialUser $authenticateUser, Request $request)
    {   

       if (Auth::check())
       {
            //dd("the user is authenticated therefore wants to link their accounts");
            return $authenticateUser->linkAccounts($request->all(),$provider, Auth::user()->user_id);;
       }  

       return $authenticateUser->execute($request->all(),$provider);
    }

    /**
     * Log the user in
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }



        if (Auth::attempt("user",$this->getCredentials($request), $request->remember)) 
        {
          return redirect()->intended('profile');
        }
        

        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }
    
    /*
     * Show the application home to the user.
     *
     * @return Response
     */
    public function index()
    {
        return view('home');
    }


    /**
     * Show login page.
     *
     * @return Response
     */
    public function getLogin()
    {
        return view('auth/login');
    }

     /**
     * Get the user credentials
     *
     * @return Response
     */
    public function getCredentials(Request $request)
    {
       return [
        'email' => $request->input('email'),

        'password' => $request->input('password'),

        'verified' => true

       ];
    }
}
