<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Session;
use Validator;
use App\Company;
use Illuminate\Http\Request;
use App\Http\AuthenticateSocialCompany;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Sarav\Multiauth\Foundation\AuthenticatesAndRegistersUsers;

class CompanyAuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    //when a user is successfully logged in they are redirected to profile
    protected $redirectPath = '/company_profile';
    //when a user fails log in
    protected $loginPath = '/company_login';
    //control the number of attemped logins before lock, default is 5
    private $maxLoginAttempts = 5; 
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = "company";
        
        //$this->socialite = $socialite;

        $this->middleware('company.guest', ['except' => 'getLogout']);
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback($provider= null, AuthenticateSocialCompany $authenticateUser, Request $request)
    {
              
       return $authenticateUser->execute($request->all(),$provider);
    }

    /**
     * Log the user in
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCompanyLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }


        if (Auth::attempt("company",$this->getCredentials($request), $request->remember)) 
        {
          return redirect()->intended('company_profile');
        }
        

        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }
    

    /**
     * Show the company login page.
     *
     * @return Response
     */
    public function getCompanyLogin()
    {

        return view('auth/company_main');
    }

    public function findLinkUser(Request $request)
    {   
        // $this->validate($request, [
        //     'email' => 'required|email', 'password' => 'required',
        //     'social_name' => 'required', 'social_id' => 'required',
        // ]);
        // 
        // needs validation!!!

        $socialite_user = $request->all();
        
        //dd($socialite_user);

        return view('auth/find_link_user', compact('socialite_user'));
    }

    public function linkAccounts(Request $request)
    {
        $socialite_user = $request->all();
        
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }


        if (Auth::attempt("company",$this->getCredentials($request), $request->remember)) 
        {
          dd($request->all());
        }
        

        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

    
       return response()->view('auth/find_link_user', compact('socialite_user'))
            ->withInput($request->all())
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }

     /**
     * Get the user credentials
     *
     * @return Response
     */
    public function getCredentials(Request $request)
    {
       return [
        'company_email' => $request->input('email'),

        'password' => $request->input('password'),

        'verified' => true

       ];
    }
}
