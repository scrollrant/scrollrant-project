<?php namespace App\Http\Controllers;

use Auth;
use App\Rant;
use Session;
use App\WatchRant;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Http\RedirectResponse;

class WatchController extends controller{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get the rant that the user is watching and return any other rants made by the ranter.
     *@param $id , Request $request
     * @return view(profile) with array
     */
    public function getWatch($id, Request $request)
    {
        $rant = Rant::with('photo','user', 'careFactor', 'hooks', 'comment')->where('rant_id','=',$id)->get();
              
        if (!$rant->isEmpty())
        {
            foreach ($rant as $user_rant) 
            {
               
                $user_id = $user_rant->user->user_id;

                $status = $user_rant->status;

                $rant_id = $user_rant->rant_id;
            }

            $other_rants = Rant::with('photo','user', 'careFactor', 'hooks', 'comment')
            ->GetOtherRants($user_id, $status, $rant_id);

           return view('watch_rant', compact('rant','other_rants')); 
        }
        else
        {
            Session::flash('message', 'The rant has been deleted and no longer exists.');

            return new RedirectResponse(url('rants'));
        }

        
            
    }

    //other controller methods...
}

?>