<?php namespace App\Http\Controllers;

use Auth;
use File;
use Event;
use Session;
use App\Rant;
use Validator;
use App\NotRant;
use App\RantPhoto;
use App\WatchRant;
use App\ReportRant;
use App\Events\WatchEvent;
use App\Events\ReportEvent;
use App\Events\NotRantEvent;
use Illuminate\Http\Request;
use App\Events\DeleteWatchEvent;
use Illuminate\Routing\Controller;

class RantActionController extends controller{

    public function __construct()
    {
        $this->middleware('auth');
    }

   /**
    * Create various actions on a rant based on action
    * @param  Request $request [description]
    * @return redirect()->back()
    */
    public function createAction(Request $request)    
    {

        $validator = Validator::make($request->all(), [
            'rant_id' => 'required|numeric',
            'user_id' => 'required|numeric',
            'action' => 'required|alpha',
            ]);

        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }

        if ($request->action == 'Watch')

        {
            $watch = $this->createWatch($request->rant_id,$request->user_id);

        }

        if ($request->action == 'Delete')

        {
            $delete = $this->deleteRant($request->rant_id, $request->user_id);
        }

        if ($request->action == 'Report')

        {
            $report = $this->createReport($request->rant_id, $request->user_id);
        }

        if ($request->action == 'Not_Rant')

        {
            $report = $this->createNotRant($request->rant_id, $request->user_id);
        }

        return redirect()->back();
    }

    /**
     * Watch a rant to get notifications
     * Can only watch 5 max
     * @param  [int] $rant_id 
     * @param  [int] $user_id
     * 
     */
    protected function createWatch($rant_id, $user_id)
    {
        $watch = new WatchRant;

        $rant_exists = Rant::where('rant_id','=',$rant_id)->firstOrFail();
        
        $already_watching  = WatchRant::where('rant_id', '=', $rant_id)->where('user_id', '=', $user_id)->first();
        
        $watch_count = WatchRant::where('user_id','=', Auth::user()->user_id)->count();

        if (!$already_watching && $watch_count < 5 && $rant_exists)
        {
            $watch->rant_id = $rant_id;

            $watch->user_id = $user_id;

            $watch->save();

            $watch_rant =  ['watch_id' => $watch->watch_id,
            'rant_id' => $watch->rant_id,
            'user_id' => $watch->user_id,
            'rant' => str_limit($watch->rant->rant, 20) 
            ];

            Event::fire(new WatchEvent($watch_rant));
        }
        else
        {
            Session::flash('message', 'Already watching this rant!');
        }
    }
    /**
     * delete a  watch and send a rant id to the delete event so watch can be delete from that rant
     * @param  watch_id
     * @param  Request $request
     * @return redirect back
     */
    public function deleteWatch($watch_id, Request $request)
    {
        WatchRant::where('watch_id', $watch_id)->delete();

        Session::flash('message', 'You are no longer watching rant');

        $rant_id = [
        'rant_id' => $request->rant_id
        ];

        Event::fire(new DeleteWatchEvent($rant_id));

        return redirect()->back();
    }
    /**
     * Report a rant
     * @param  [int] $rant_id [description]
     * @param  [int] $user_id [description]
     * 
     */
    protected function createReport($rant_id, $user_id)
    {
        $report = new ReportRant;

        $rant_exists = Rant::where('rant_id','=',$rant_id)->firstOrFail();
        
        $already_reported  = ReportRant::where('rant_id', '=', $rant_id)->where('user_id', '=', $user_id)->first();
        
        if (!$already_reported && $rant_exists)
        {
            $report->rant_id = $rant_id;

            $report->user_id = $user_id;

            $report->save();

            Event::fire(new ReportEvent($report));
            
            Session::flash('message', 'You have reported this rant!');
        }
        else
        {
            Session::flash('message', 'You have already reported this rant!');
        }
    }

    /**
     * declare that a rant isn't a rant
     * @param  [int] $rant_id [description]
     * @param  [int] $user_id [description]
     * 
     */
    protected function createNotRant($rant_id, $user_id)
    {
        $not_rant = new NotRant;

        $rant_exists = Rant::where('rant_id','=',$rant_id)->firstOrFail();
        
        $already_declared  = NotRant::where('rant_id', '=', $rant_id)->where('user_id', '=', $user_id)->first();
        
        if (!$already_declared && $rant_exists)
        {
            $not_rant->rant_id = $rant_id;

            $not_rant->user_id = $user_id;

            $not_rant->save();

            Event::fire(new NotRantEvent($not_rant));
            
            Session::flash('message', 'You have declared this not rant!');
        }
        else
        {
            Session::flash('message', 'You have already declared this rant!');
        }
    }


    /**
     * authorize user and delete a rant based on (rantid) and its associated pictures in publie folder. 
     * Only the creator can delete their own rant!
     * @return redirect
     */
    protected function deleteRant($rantid, $user_id)
    {

        $user_owns_rant = Rant::where('rant_id','=',$rantid)->where('user_id','=',$user_id)->firstOrFail();
        
        $rant_photo = RantPhoto::where('rant_id', $rantid)->first();

        if ($user_owns_rant)  

        {
            $user_owns_rant->delete(); 
            
            if ($rant_photo)
            {
                File::delete('images/'.$rant_photo->name);
            }

            Session::flash('message', 'Successfully deleted the rant!');
        }  

           
    }

    //other controller methods...
}

?>