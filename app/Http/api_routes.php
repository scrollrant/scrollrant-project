<?php

$api = app('Dingo\Api\Routing\Router');
// All Free Routes
$api->version('v1', ['prefix' => 'api', 'domain' => 'scrollrant.com', 'middleware' => 'cors'], function ($api) {
	// Authentication Module routes
	$api->post('signup', 'App\Api\V1\Controllers\ApiAuthController@signup');
	$api->post('auth/recovery', 'App\Api\V1\Controllers\ApiAuthController@recovery');
	$api->post('auth/reset', 'App\Api\V1\Controllers\ApiAuthController@reset');
});

//Free login route...
$api->version('v1', ['prefix' => 'api', 'domain' => 'scrollrant.com', 'middleware' => ['cors','api.throttle']], function ($api) {
	// Login route...
	$api->get('login','App\Api\V1\Controllers\ApiAuthController@login');

});

// All protected routes
$api->version('v1', ['prefix' => 'api', 'domain' => 'scrollrant.com', 'middleware' => ['cors','api.auth'], 'providers' => ['oauth']], function ($api) {

	//User routes...
	$api->get('user_details', 'App\Api\V1\Controllers\ApiUserController@getUserDetails');
	$api->get('rant_faces', 'App\Api\V1\Controllers\ApiRantsController@getRantFaces');
	$api->get('hooks', 'App\Api\V1\Controllers\ApiRantsController@getHooks');
	$api->get('user_email', 'App\Api\V1\Controllers\ApiUserController@getUserEmail');
	$api->get('user_aliasname', 'App\Api\V1\Controllers\ApiUserController@getUserAliasName');
	
	//rants routes...
	$api->get('user_rants', 'App\Api\V1\Controllers\ApiRantsController@getUserRants');
	$api->get('rants', 'App\Api\V1\Controllers\ApiRantsController@getRants');
	$api->post('rant', 'App\Api\V1\Controllers\ApiRantsController@createRant');
	$api->post('update_rant_face', 'App\Api\V1\Controllers\ApiRantsController@updateRantFace');
	$api->post('rant_action', 'App\Api\V1\Controllers\ApiRantActionController@createAction');
	$api->post('care_factor', 'App\Api\V1\Controllers\ApiCareFactorController@rantReaction');
	$api->post('rant_comment', 'App\Api\V1\Controllers\ApiUserRantCommentController@createRantCommentUser');

	//logout route...
	$api->post('logout', 'App\Api\V1\Controllers\ApiAuthController@logout');
});