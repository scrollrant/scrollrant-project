<?php  namespace App\Http;

use Auth;
use Session;
use App\User;
use Carbon\Carbon;
use App\SocialUser;
use App\Jobs\SendWelcomeEmail;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard; 
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Contracts\Factory as Socialite;


class AuthenticateSocialUser extends Controller
{
	private $user;
	private $socialite;
	private $auth;

	public function __construct(User $user, Socialite $socialite, Guard $auth)
	{
		$this->user = $user;
		$this->socialite = $socialite;
		$this->auth = $auth;
	}

	/**
	 * Get token and execute the logic to log a user in
	 * @param  [type] $request  [description]
	 * @param  [type] $provider [description]
	 * @return [type]           [description]
	 */
	public function execute($request, $provider)
	{

		if ( !$request) return $this->getAuthorizationFirst($provider);

		$social_user = $this->socialite->with($provider)->user();

		//check if the socialite user exists if email and socialite user id
		$socialite_user_exists = SocialUser::where('social_email', '=', $social_user->email)->where('socialite_user_id','=', $social_user->id)->first();

		if ($socialite_user_exists) 
		{

			//authenticate the user with user id of social user table
			Auth::loginUsingId($socialite_user_exists->user_id);

            //redirect to the profile
            return new RedirectResponse(url('profile'));
		}

		//check if the user exists
		$user_exists = $this->user->whereEmail($social_user->getEmail())->first();

		if ($user_exists) 
		{
			//get the id of the user and log them in by id n redirect to profile
			//check to see if they are activated 
			//dd($user_exists);
			$user = User::find($user_exists->user_id);

			if ($user->verified === 0) 
			{
				//verify the user
			  	$user->confirmEmail();
			}			

            //create the social user
            SocialUser::create([
            	'social_email' => $social_user->email,
            	'social_name' => $social_user->name,
            	'socialite_user_id' => $social_user->id,
            	'user_id' => $user->user_id,
            	'created_at' => Carbon::now(),
            	'provider_name' => $provider,
            	]);
            //Authenticate the user
            Auth::loginUsingId($user->user_id);

            //
            Session::flash('message', 'Accounts linked, you can now sign in via '.$provider.' with one click!');
            //redirect to the profile
            return new RedirectResponse(url('profile'));
		}

		//dd($user_exist);

		if (! $user_exists) return $this->createNewUser($social_user, $provider);
	}

	/**
	 * Link the authenticated user's social accounts to one account
	 * @param  [type] $request  [description]
	 * @param  [type] $provider [description]
	 * @return [type]           [description]
	 */
	public function linkAccounts($request, $provider, $authenticated_user_id)
	{
		//login to socialite and return a user
		if ( !$request) return $this->getAuthorizationFirst($provider);
		$social_user = $this->socialite->with($provider)->user();
		
		//check if those details have already been saved in the social user table
		$socialite_user_exists = SocialUser::where('social_email', '=', $social_user->email)
		->where('user_id', $authenticated_user_id)
		->where('socialite_user_id','=', $social_user->id)->first();

		//if not then create the new social account
		if (!$socialite_user_exists) 
		{
			 //create the social user
            SocialUser::create([
            	'social_email' => $social_user->email,
            	'social_name' => $social_user->name,
            	'socialite_user_id' => $social_user->id,
            	'user_id' => $authenticated_user_id,
            	'created_at' => Carbon::now(),
            	'provider_name' => $provider,
            	]);
            //redirect the user to proile page with a flash message
          	Session::flash('message', 'Accounts linked, you can now sign in via '.$provider.' with one click!');
	        //redirect to the profile(better to redirect to settings page where user can manage their account)
	       return new RedirectResponse(url('manage'));
		}
		
		Session::flash('warning_message', 'This account is already linked to another user, delete one user and link all you social accounts under one user');
        //redirect to the profile(better to redirect to settings page where user can manage their account)
       return new RedirectResponse(url('manage'));

	}

	private function getAuthorizationFirst($provider)
	{
	
		return $this->socialite->with($provider)->redirect();
	}


	private function createNewUser($socialite_user, $provider)
	{

		
		if ($provider === 'facebook') {
			
			$name = explode(' ', $socialite_user->name);

			//create the user
		  	$user = User::create([
            'first_name' => $name[0],
            'surname' => $name[1],
            'alias_name' => "Anonymous",
            'email' => $socialite_user->email,
            'password' => bcrypt(str_random(8)),
            ]);

            //send welcome email and password
            $signed_up_user = User::findOrFail($user->user_id);

            $welcome_email = (new SendWelcomeEmail($signed_up_user))->delay(60 * 5);

            $this->dispatch($welcome_email);

		  	//verify the user
		  	$user->confirmEmail();

            //create the social user
            SocialUser::create([
            	'social_email' => $socialite_user->email,
            	'social_name' => $socialite_user->name,
            	'socialite_user_id' => $socialite_user->id,
            	'user_id' => $user->user_id,
            	'created_at' => Carbon::now(),
            	'provider_name' => $provider,
            	]);
            //Authenticate the user
            Auth::loginUsingId($user->user_id);

            //
            Session::flash('socialite_message', 'New Account has been created, link your other social accounts');
            //redirect to the profile
            return new RedirectResponse(url('profile'));
		}
		else if ($provider === 'twitter') {
			$name = explode(' ', $socialite_user->name);

			$first_name = $name[0];

			if (sizeof($name) > 1) 
			{
				$surname = $name[1];
			}
			else
			{
				$surname = $name[0];
			}
			//create a new user
			$user = User::create([
            'first_name' => $first_name,
            'surname' => $surname,
            'alias_name' => "Anonymous",
            'email' => $socialite_user->email,
            'password' => bcrypt(str_random(8)),
            'verified' => true,
            ]);

            //send welcome email and password
			 $signed_up_user = User::findOrFail($user->user_id);

            $welcome_email = (new SendWelcomeEmail($signed_up_user))->delay(60 * 5);

            $this->dispatch($welcome_email);
            //verify the user
		  	$user->confirmEmail();

            //create the social user
            SocialUser::create([
            	'social_email' => $socialite_user->email,
            	'social_name' => $socialite_user->name,
            	'socialite_user_id' => $socialite_user->id,
            	'user_id' => $user->user_id,
            	'created_at' => Carbon::now(),
            	'provider_name' => $provider,
            	]);
            //Authenticate the user
            Auth::loginUsingId($user->user_id);

            //
            Session::flash('socialite_message', 'New Account has been created, link your other social accounts');
            //redirect to the profile
            return redirect()->intended('profile');
		}
		else
		{
			//its a google a account
			dd("its a google account");
			$name = explode(' ', $socialite_user->name);

			$first_name = $name[0];

			if (sizeof($name) > 1) 
			{
				$surname = $name[1];
			}
			else
			{
				$surname = $name[0];
			}
			//create a new user
			$user = User::create([
            'first_name' => $first_name,
            'surname' => $surname,
            'alias_name' => "Anonymous",
            'email' => $socialite_user->email,
            'password' => bcrypt(str_random(8)),
            'verified' => true,
            ]);

            //send welcome email and password
			$signed_up_user = User::findOrFail($user->user_id);

            $welcome_email = (new SendWelcomeEmail($signed_up_user))->delay(60 * 5);

            $this->dispatch($welcome_email);
            //verify the user
		  	$user->confirmEmail();

            //create the social user
            SocialUser::create([
            	'social_email' => $socialite_user->email,
            	'social_name' => $socialite_user->name,
            	'socialite_user_id' => $socialite_user->id,
            	'user_id' => $user->user_id,
            	'created_at' => Carbon::now(),
            	'provider_name' => $provider,
            	]);
            //Authenticate the user
            Auth::loginUsingId($user->user_id);

            //
            Session::flash('socialite_message', 'New Account has been created, link your other social accounts');
            //redirect to the profile
            return redirect()->intended('profile');
		}

	}

}