<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('/', 'Auth\AuthController@index');
Route::get('home', 'Auth\AuthController@index');
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
// Route::get('company_login', 'Auth\CompanyAuthController@getCompanyLogin');
// Route::post('company_login', 'Auth\CompanyAuthController@postCompanyLogin');
Route::post('logout', 'ProfileController@logOutUser');
// Route::post('company_logout', 'CompanyController@logOutCompany');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

//Signup routes for user and company...
Route::get('register', 'SignupController@getSignupForm');
Route::post('user', 'SignupController@signupUser');
// Route::get('register_company', 'SignupController@getCompanySignupForm');
// Route::post('company', 'SignupController@signupCompany');

//Verification routes...
Route::get('verify_account/{token}', 'SignupController@confirmEmail');
// Route::get('verify_company_account/{token}', 'SignupController@confirmCompanyEmail');

// Signup completion(AJAX javascript) routes...
Route::get('register/states','SignupController@getStates');
Route::get('alias_exist/{id}','SignupController@aliasExists');
Route::get('email_exist/{id}','SignupController@emailExists');
Route::get('company_email_exist/{id}','SignupController@companyEmailExists');
Route::get('company_name_exist/{id}','SignupController@companyNameExists');

// Hooks search routes..
Route::get('search/hooks/{hook_name}','HooksController@searchHooks');
Route::get('hooked/{id}/{hook_name}','HooksController@getHooks'); 

// Profile routes for user and company...
Route::get('profile', 'ProfileController@getUserDetails');
Route::get('company_profile', 'CompanyController@getCompanyDetails');
Route::post('rant',  ['as' => 'profile', 'uses' => 'RantController@createRant']);
//Route::resource('deleteRant', 'RantController@destroy');
Route::post('carefactor', 'RantController@rantReaction');

// Solicalite login routes...
Route::get('login/callback/{provider?}','Auth\AuthController@handleProviderCallback');
// Route::get('find_user','Auth\CompanyAuthController@findLinkUser');
Route::post('unlink_social_account','ManageAccountController@unlinkAccounts');

// Update user routes...
Route::post('update_gender', 'UserController@updateGender');
Route::post('update_location', 'UserController@updateLocation');
Route::post('update_birthday', 'UserController@updateBirthday');
Route::post('update_rant_face', 'UserController@updateRantFace');

// Rant action forms route...
Route::post('rant_action', 'RantActionController@createAction');
Route::post('delete_watch/{id}', 'RantActionController@deleteWatch');

// Carefactor route...
Route::post('carefactor', 'CareFactorController@rantReaction');

// Make a comment route...
Route::post('comment',  'RantCommentController@createRantCommentUser');


// Rant Wall routes...
Route::get('rants', 'RantWallController@getRantWall');


// Watch Rant routes...
Route::get('watch/{id}', ['as' => 'watch', 'uses' =>  'WatchController@getWatch']);


//The scrollrant documents
Route::get('guide', function(){
	return view('scrollrant_guide');
});
Route::get('terms', function(){
	return view('scrollrant_terms');
});
Route::get('privacy', function(){
	return view('scrollrant_privacy');
});

//Feedback and invite a friend routes..
Route::get('manage', 'ManageAccountController@index');
Route::post('feedback', 'ManageAccountController@sendFeedback');
Route::post('invite', 'ManageAccountController@inviteFriend');





