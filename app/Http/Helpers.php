<?php 

namespace App\Http;

use App\User;
use App\NotRant;
use Carbon\Carbon; 
use App\ReportRant;
use App\Jobs\SendRantBlockedEmail;
use Bus;

class Helpers {

	/**
	 * upgrade the rant score everytime an event is fired if the 
	 * @param  [type] $rant [description]
	 * @return [type]       [description]
	 */
	public static function upgradeScope($rant)
	{
			//check the scope of the rant

		if ($rant->scope_id == 3)
		{

			if ($rant->state_id != Null)
			{
				$number_of_state_user = User::where('state_id', $rant->state_id)->count();
			}
			else
			{
				$number_of_state_user = User::all()->count();
			}
			

			if ( $rant->rant_score > $number_of_state_user * 0.05 )
			{

				$rant->scope_id = 2;

				$rant->rant_score = 0;

				$rant->save();
			}

		}
		elseif ($rant->scope_id == 2)
		{

			if ($rant->country_id != Null)
			{
				$number_of_country_users = User::where('country_id', $rant->country_id)->count();
			}
			else
			{
				$number_of_country_users = User::all()->count();
			}

			if ( $rant->rant_score > $number_of_country_users * 0.10 )
			{
				$rant->scope_id = 1;

				$rant->rant_score = 0;

				$rant->save();	
			}
		}
	}
	/**
	 * block a rant if it's rant score is less than the amount of reports it has 
	 * and its 1 hour old.
	 * @param  [type] $rant [description]
	 * @return [type]       [description]
	 */
	public static function blockRant($rant)
	{
		$reports_count = ReportRant::where('rant_id',$rant->rant_id)->count();

		if ( $rant->rant_score < $reports_count && $rant->create_date < Carbon::now()->subHour(1) )
		{
			$rant->scope_id = 4;

			$rant->rant_score = 0;

			$rant->save();

			Bus::dispatch(new SendRantBlockedEmail($rant));	
		}
	} 
	/**
	 * A rant is not considered a legitatmate rant if its rant score
	 * is less than the amount of times users have taged it not a rant.
	 * @param  [type] $rant [description]
	 * @return [type]       [description]
	 */
	public static function notRant($rant)
	{
		
		$not_rant_count = NotRant::where('rant_id',$rant->rant_id)->count();

		if ( $rant->rant_score < $not_rant_count && $rant->create_date < Carbon::now()->subHour(1) )
		{
			$rant->scope_id = 5;

			$rant->rant_score = 0;

			$rant->save();	
		}
	}  




}