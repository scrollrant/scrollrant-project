<?php  namespace App\Http;

use Session;
use App\Company;
use Illuminate\Contracts\Auth\Guard; 
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Contracts\Factory as Socialite;


class AuthenticateSocialCompany 
{
	private $company;
	private $socialite;
	private $auth;

	public function __construct(Company $company, Socialite $socialite, Guard $auth)
	{
		$this->company = $company;
		$this->socialite = $socialite;
		$this->auth = $auth;
	}


	public function execute($request, $provider)
	{

		if ( !$request) return $this->getAuthorizationFirst($provider);

		$company = $this->socialite->with($provider)->user();

		$company_exist = $this->company->whereCompanyEmail($company->getEmail())->first();

		//dd($company_exist);
		if (! $company_exist ) return $this->pickAccountStatus($company);
	}

	private function getAuthorizationFirst($provider)
	{
	
		return $this->socialite->with($provider)->redirect();
	}


	private function pickAccountStatus($socialite_user)
	{

		Session::flash('account_status_message', 'We did not find an scrollrant account that matches your social login. Please pick one of these two options. You only have to do this once!');

		return response()->view('auth/company_status', compact('socialite_user'));
	}

}