<?php
 
namespace App\Http\Middleware;
 
use Closure;
 
class CompanyAuthenticate
{
   
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::guest("company")) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('company_login');
            }
        }
 
        return $next($request);
    }
}