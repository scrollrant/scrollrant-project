<?php namespace App\Http\Requests;
    
    use Response;
    use Illuminate\Foundation\Http\FormRequest;
    use Validator; 
    class CommentFormRequest extends FormRequest {
        
    
        public function rules()
        {   

            return [
            'rant_id' => 'required|numeric',
            'user_id' => 'required|numeric',
            'comment' => 'required'
            ];
        }

        public function authorize()
        {
            return true;
        }
     
    }