<?php namespace App\Http\Requests;
    
    use Response;
    use Illuminate\Foundation\Http\FormRequest;
    use Validator; 
    use Session;
    class CreateRallyFormRequest extends FormRequest {
        
    
        public function rules()
        {   

            return [
            'coordinator_name' => 'required|min:2|max:50',
            'rally_title' => 'required|min:2|max:50',
            'rally_category' => 'required',
            'rally_description' => 'required',
            ];
        }
     
        public function authorize()
        {
            return true;
          
        }
     
    }