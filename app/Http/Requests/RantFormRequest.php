<?php namespace App\Http\Requests;
    
    use Response;
    use Illuminate\Foundation\Http\FormRequest;
    use Validator; 
    use Session;
    class RantFormRequest extends FormRequest {
        
      

        public function rules()
        {  
            return [
            'rant' => 'required|max:500',
            'file' => 'image|max:900',
            'hook_id' => 'numeric' 
            ];
        }
     
        public function authorize()
        {
            return true;
          //$request->session()->put(['email' => $request->email]);
        }
     
    }