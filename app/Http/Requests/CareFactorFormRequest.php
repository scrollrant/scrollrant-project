<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CareFactorFormRequest extends FormRequest {


    public function rules()
    {   

        return [
        'rant_id' => 'required|numeric',
        'carefactor' => 'required|numeric',
        'user_id' => 'required|numeric'
        ];
    }

    public function authorize()
    {
        return true;
    }

}