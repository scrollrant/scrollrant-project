<?php namespace App\Http\Requests;
    
    use Response;
    use Illuminate\Foundation\Http\FormRequest;
    use Validator; 
    class LoginFormRequest extends FormRequest {
        
      

        public function rules()
        {   
            return [
            'email' => 'required|email',
            'password' => 'required',
            'remember' => '',
            
            ];
        }
     
        public function authorize()
        {
            return true;
          
        }
     
    }