<?php namespace App\Http\Requests;
    
    use Response;
    use Illuminate\Foundation\Http\FormRequest;
    use Validator; 
    class StatementFormRequest extends FormRequest {
        
    
        public function rules()
        {   

            return [
            'rally_statement' => 'required',
            
            ];
        }

         public function messages()
        {   

            return [
            'required' => 'You can not post an empty statement, what impact will that do?',           
            ];
        }
     
     
        public function authorize()
        {
            return true;
        }
     
    }