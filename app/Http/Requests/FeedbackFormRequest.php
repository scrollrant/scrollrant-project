<?php namespace App\Http\Requests;
    
    use Response;
    use Illuminate\Foundation\Http\FormRequest;
    use Validator; 
    class FeedbackFormRequest extends FormRequest {
        
      

        public function rules()
        {   
            return [
            'name' => 'required|alpha',
            'feedback_type' => 'required',
            'message' => 'required',
            
            ];
        }
     
        public function authorize()
        {
            return true;
          
        }
     
    }