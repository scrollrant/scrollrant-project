<?php namespace App\Http\Requests;
    
    use Response;
    use Illuminate\Foundation\Http\FormRequest;
    use Validator; 
    class ShowCommentFormRequest extends FormRequest {
        
    
        public function rules()
        {   

            return [
            'show_rant_id' => 'required|numeric',
            ];
        }

         public function messages()
        {   

            return [
            'show_rant_id' => 'I need a number you naughty thing',
           
            ];
        }
     
     
        public function authorize()
        {
            return true;
        }
     
    }