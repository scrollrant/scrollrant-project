<?php namespace App\Http\Requests;
    
    use Response;
    use Illuminate\Foundation\Http\FormRequest;
    use Validator; 
    use Session;
    class PicFormRequest extends FormRequest {
        
        public function rules()
        {   

            return [
           
             'image' => 'required|mimes:jpeg,bmp,png'
            ];
        }
     
        public function authorize()
        {
            return true;
         
        }
     
    }