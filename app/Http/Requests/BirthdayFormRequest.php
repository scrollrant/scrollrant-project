<?php namespace App\Http\Requests;
    
    use Response;
    use Illuminate\Foundation\Http\FormRequest;
    use Validator; 
    class BirthdayFormRequest extends FormRequest {
        
      

        public function rules()
        {  
            return [
            'month' => 'required|numeric|digits_between:01,12',
            'day' => 'required|digits_between:1,2|max:31|numeric',
            'year' => 'required|digits:4|numeric',
            'date_of_birth' => 'required|date',
            ];
        }
     
        public function authorize()
        {
            return true;
        }

        /**
         * used to overide error messages 
         * @return [field name] [error message]
         */
        public function messages()
        {   

            return [
            'date_of_birth.required' => 'You need to enter a date of birth!',
            ];
        }

        /**
         * Concatinates the separate date inputs into one valid date
         * @return parent::all()
         */
        public function all()
        {
            $input = parent::all();
            if( is_numeric($input['year']) && $input['month'] && is_numeric($input['day']) != null)
            {
                $input['date_of_birth'] = $input['year'].'-'.$input['month'].'-'.$input['day'];
                $this->replace($input);
                return parent::all();
            }
            else
            {
                return parent::all();
            }   
        }
     
    }