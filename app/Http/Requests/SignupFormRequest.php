<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignupFormRequest extends FormRequest {

    public function rules()
    {   
        return [
        'first_name' => 'required|min:2|max:50|alpha',
        'surname' => 'required|min:2|max:50|alpha',
        'alias_name' => 'max:25|unique:user,alias_name|required',
        'email' => 'required|email|max:255|unique:user,email',
        'password' => 'required|confirmed|min:6',
        'password_confirmation' =>'required|min:6',
        ];
    }

    public function authorize()
    {
        return true;

    }

}