<?php namespace App\Http\Requests;
    
    use Response;
    use Illuminate\Foundation\Http\FormRequest;
    use Validator; 
    class LocationFormRequest extends FormRequest {
        
      

        public function rules()
        {  
            return [
            'country_id' => 'required|numeric',
            'state_id' => 'required|numeric',
            ];
        }
     
        public function authorize()
        {
            return true;
        }
     
    }