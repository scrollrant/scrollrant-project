<?php namespace App\Http\Requests;

use Response;
use Illuminate\Foundation\Http\FormRequest;
use Validator; 
class GenderFormRequest extends FormRequest {
    
  

    public function rules()
    {  
        return [
        'gender' => 'required|numeric', 
        ];
    }
    
    public function authorize()
    {
        return true;
    }
    
}