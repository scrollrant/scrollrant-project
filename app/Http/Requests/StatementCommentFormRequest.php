<?php namespace App\Http\Requests;
    
    use Response;
    use Illuminate\Foundation\Http\FormRequest;
    use Validator; 
    class StatementCommentFormRequest extends FormRequest {
        
    
        public function rules()
        {   

            return [
            'statement_id' => 'required|numeric',
            'user_id' => 'required|numeric',
            'rally_id' => 'required|numeric',
            'comment' => 'required|max:200'
            ];
        }

    
        public function authorize()
        {
            return true;
        }
     
    }