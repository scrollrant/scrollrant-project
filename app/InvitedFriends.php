<?php namespace App;


use Illuminate\Database\Eloquent\Model;

class InvitedFriends extends Model{


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'invited_friends';
	protected $primaryKey = 'invited_friends_id';
	public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['friends_email', 'friends_name'];

}
