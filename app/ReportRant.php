<?php namespace App;


use Illuminate\Database\Eloquent\Model;

class ReportRant extends Model{


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'report_rant';
	protected $primaryKey = 'report_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id', 'rant_id'];

}
