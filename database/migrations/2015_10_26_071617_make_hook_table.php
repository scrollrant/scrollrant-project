<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeHookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('make_hook', function (Blueprint $table) {
            $table->increments('make_hook_id');
            $table->integer('rant_id')->unsigned();
            $table->integer('hook_id')->unsigned();
            $table->timestamps();

            $table->foreign('rant_id')->references('rant_id')->on('rant')
            ->onDelete('cascade');
            $table->foreign('hook_id')->references('hook_id')->on('hook')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('make_hook');
    }
}
