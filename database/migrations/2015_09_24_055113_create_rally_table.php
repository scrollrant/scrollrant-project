<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRallyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rally', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('rally_id');
            $table->integer('company_id')->unsigned();
            $table->integer('rally_type_id');
            $table->string('rally_title',50);
            $table->text('rally_description')->nullable();
            $table->text('rally_cover_pic')->nullable();
            $table->timestamps();

            $table->foreign('company_id')->references('company_id')->on('company')
           ->onDelete('cascade')
           ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rant_rally');
    }
}
