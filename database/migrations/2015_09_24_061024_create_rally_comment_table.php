<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRallyCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rally_comment_user', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('rally_comment_id');
            $table->text('rally_comment');
            $table->integer('statement_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->text('media')->nullable();
            $table->timestamps();

            $table->foreign('statement_id')->references('statement_id')->on('statement')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('user_id')->references('user_id')->on('user')
            ->onDelete('cascade')
            ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rally_comment_user');
    }
}
