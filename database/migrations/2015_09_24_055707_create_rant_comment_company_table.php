<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRantCommentCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rant_comment_company', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('rant_comment_id');
            $table->text('rant_comment');
            $table->integer('rant_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->text('media')->nullable();
            $table->timestamps();

            $table->foreign('rant_id')->references('rant_id')->on('rant')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('user_id')->references('user_id')->on('user')
            ->onDelete('cascade')
            ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rant_comment_company');
    }
}
