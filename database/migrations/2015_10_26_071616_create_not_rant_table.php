<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotRantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('not_a_rant', function (Blueprint $table) {
            $table->increments('not_a_rant_id');
            $table->integer('rant_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('rant_id')->references('rant_id')->on('rant')
            ->onDelete('cascade');
            $table->foreign('user_id')->references('user_id')->on('user')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('not_a_rant');
    }
}
