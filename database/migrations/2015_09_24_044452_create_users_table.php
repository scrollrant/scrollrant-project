<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('user_id');
            $table->string('first_name',50);
            $table->string('surname',50);
            $table->string('alias_name',50);
            $table->string('password',100);
            $table->string('email',100)->unique();
            $table->integer('gender_id')->nullable()->unsigned();
            $table->integer('country_id')->nullable()->unsigned();
            $table->integer('state_id')->nullable()->unsigned();
            $table->integer('user_status_id')->nullable()->unsigned();
            $table->string('account_status',10)->default('user');
            $table->date('date_of_birth')->nullable();
            $table->integer('influence')->default(0);
            $table->boolean('verified')->default(false);
            $table->string('email_token')->nullable();
            $table->timestamp('join_date')->default(DB::raw('CURRENT_TIMESTAMP'));          
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('country_id')->references('country_id')->on('country')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->foreign('gender_id')->references('gender_id')->on('gender')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->foreign('state_id')->references('state_id')->on('state')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->foreign('user_status_id')->references('user_status_id')->on('user_status')
            ->onDelete('restrict')
            ->onUpdate('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user');
    }
}
