<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hook', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('hook_id');
            $table->string('hook_name',100);
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('country_id')->nullable()->unsigned();
            $table->integer('state_id')->nullable()->unsigned();
            $table->integer('company_id')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('company_id')->references('company_id')->on('company')
            ->onDelete('no action')
            ->onUpdate('no action');
            $table->foreign('user_id')->references('user_id')->on('user')
            ->onDelete('no action')
            ->onUpdate('no action');
            $table->foreign('country_id')->references('country_id')->on('user')
            ->onDelete('no action')
            ->onUpdate('no action');
            $table->foreign('state_id')->references('state_id')->on('user')
            ->onDelete('no action')
            ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hook');
    }
}
