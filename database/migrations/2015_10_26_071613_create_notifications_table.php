<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('notification_id');
            $table->text('notification');
            $table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('country_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->string('email',100);
            $table->timestamps();

            $table->foreign('country_id')->references('country_id')->on('country');
            $table->foreign('state_id')->references('state_id')->on('state');
            $table->foreign('email')->references('email')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notification');
    }
}
