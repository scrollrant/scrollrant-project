<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRantCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rant_category', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('category_id');
            $table->integer('rant_id')->unsigned();
            $table->string('category',100);
            $table->timestamps();
            $table->foreign('rant_id')->references('rant_id')->on('rant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rant_category');
    }
}
