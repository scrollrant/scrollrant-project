<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rant', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('rant_id');
            $table->text('rant');
            $table->integer('scope_id')->unsigned()->default(3);
            $table->integer('user_id')->unsigned();
            $table->integer('hook_id')->nullable()->unsigned();
            $table->integer('rant_score')->nullable()->default(0);
            $table->timestamp('create_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('country_id')->nullable()->unsigned();
            $table->integer('state_id')->nullable()->unsigned();
            $table->integer('rant_face_id')->nullable()->unsigned();
            $table->string('status',7)->default('private');
            $table->timestamps();

            $table->foreign('scope_id')->references('scope_id')->on('scope')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->foreign('user_id')->references('user_id')->on('user')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('hook_id')->references('hook_id')->on('hook')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->foreign('rant_face_id')->references('rant_face_id')->on('rant_face')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->foreign('country_id')->references('country_id')->on('country')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->foreign('state_id')->references('state_id')->on('state')
            ->onDelete('restrict')
            ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rant');
    }
}
