<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRallyMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rally_members', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('rally_member_id');
            $table->integer('rally_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('joined_status',10)->nullable();
            $table->timestamps();

            $table->foreign('rally_id')->references('rally_id')->on('rally')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('user_id')->references('user_id')->on('user')
            ->onDelete('no action')
            ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rally_members');
    }
}
