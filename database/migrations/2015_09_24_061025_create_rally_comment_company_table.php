<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRallyCommentCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rally_comment_company', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('rally_comment_id');
            $table->text('rally_comment');
            $table->integer('statement_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->text('media')->nullable();
            $table->timestamps();

            $table->foreign('statement_id')->references('statement_id')->on('statement')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('company_id')->references('company_id')->on('company')
            ->onDelete('cascade')
            ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rally_comment_company');
    }
}
