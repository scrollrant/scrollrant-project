<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('watch_rant', function (Blueprint $table) {
            $table->increments('watch_id');
            $table->integer('rant_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('rant_id')->references('rant_id')->on('rant')
            ->onDelete('cascade');
            $table->foreign('user_id')->references('user_id')->on('user')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('watch_rant');
    }
}
