<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRantPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rant_photos', function (Blueprint $table) {
            $table->increments('photo_id');
            $table->integer('rant_id')->unsigned();
            $table->text('path');
            $table->text('thumnail_path');
            $table->text('name');
            $table->timestamps();

            $table->foreign('rant_id')->references('rant_id')->on('rant')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rant_photos');
    }
}
