<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('company_id');
            $table->string('company_name',50);
            $table->string('user_name',15)->nullable();
            $table->string('password',100);
            $table->string('company_email',100)->unique();
            $table->integer('country_id')->nullable()->unsigned();
            $table->integer('state_id')->nullable()->unsigned();
            $table->integer('company_type_id')->nullable()->unsigned();
            $table->integer('phone_number')->default(0);
            $table->boolean('verified')->default(false);
            $table->string('email_token')->nullable();
            $table->timestamp('join_date')->default(DB::raw('CURRENT_TIMESTAMP'));          
            $table->string('status',50)->nullable();
            $table->text('company_logo')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('country_id')->references('country_id')->on('country')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->foreign('company_type_id')->references('company_type_id')->on('company_type')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->foreign('state_id')->references('state_id')->on('state')
            ->onDelete('restrict')
            ->onUpdate('cascade');           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company');
    }
}
