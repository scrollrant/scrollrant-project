<?php

use Illuminate\Database\Seeder;

class ScopeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('scope')->insert([
            'scope_name' => 'Global',    
        ]);

        DB::table('scope')->insert([
            'scope_name' => 'Country',    
        ]);

        DB::table('scope')->insert([
            'scope_name' => 'State',    
        ]);

        DB::table('scope')->insert([
            'scope_name' => 'Blocked',    
        ]);

        DB::table('scope')->insert([
            'scope_name' => 'NotRant',    
        ]);
    }
}
