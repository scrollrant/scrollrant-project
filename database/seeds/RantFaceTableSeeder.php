<?php

use Illuminate\Database\Seeder;

class RantFaceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('rant_face')->insert([
            'rant_face_name' => 'Angry',
            'rant_face_path' => 'images/rant_faces/angry-01.png'    
        ]);

        DB::table('rant_face')->insert([
            'rant_face_name' => 'Angry',
            'rant_face_path' => 'images/rant_faces/angry-02.png'    
        ]);

        DB::table('rant_face')->insert([
            'rant_face_name' => 'Angry',
            'rant_face_path' => 'images/rant_faces/angry-03.png'    
        ]);

        DB::table('rant_face')->insert([
            'rant_face_name' => 'Annoyed',
            'rant_face_path' => 'images/rant_faces/annoyed-01.png'    
        ]);

        DB::table('rant_face')->insert([
            'rant_face_name' => 'Irritated',
            'rant_face_path' => 'images/rant_faces/irritated-01.png'    
        ]);

        DB::table('rant_face')->insert([
            'rant_face_name' => 'Cheeky',
            'rant_face_path' => 'images/rant_faces/cheeky-01.png'    
        ]);

        DB::table('rant_face')->insert([
            'rant_face_name' => 'Confused',
            'rant_face_path' => 'images/rant_faces/confused-01.png'    
        ]);


        DB::table('rant_face')->insert([
            'rant_face_name' => 'Happy',
            'rant_face_path' => 'images/rant_faces/happy-01.png'    
        ]);

        DB::table('rant_face')->insert([
            'rant_face_name' => 'Happy',
            'rant_face_path' => 'images/rant_faces/happy-02.png'    
        ]);

        DB::table('rant_face')->insert([
            'rant_face_name' => 'Meh!',
            'rant_face_path' => 'images/rant_faces/meh-01.png'    
        ]);

        DB::table('rant_face')->insert([
            'rant_face_name' => 'Relief',
            'rant_face_path' => 'images/rant_faces/relief-01.png'    
        ]);

        DB::table('rant_face')->insert([
            'rant_face_name' => 'Sad',
            'rant_face_path' => 'images/rant_faces/sad-01.png'    
        ]);

        DB::table('rant_face')->insert([
            'rant_face_name' => 'Scared',
            'rant_face_path' => 'images/rant_faces/scared-01.png'    
        ]);

        DB::table('rant_face')->insert([
            'rant_face_name' => 'Unwell',
            'rant_face_path' => 'images/rant_faces/unwell-01.png'    
        ]);
    }
}
