<?php

use Illuminate\Database\Seeder;

class HookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('hook')->insert([
            'hook_name' => 'Bunnings Warehouse',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Coles Group',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'David Jones Limited',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Dick Smith Electronics',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Dymocks Booksellers',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Eagle Boys',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Dominoes',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Harvey Norman',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'IGA',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'JB HiFi',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Myer',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Officeworks',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Oporto',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Red Rooster',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Target Australia',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Woolworths Limited',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Foxtel',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Network Ten',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Nine Network',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'MacDonalds',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Nandos',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Vodaphone',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Telstra',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Optus',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Microsoft',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Apple',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'K-Mart',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'National Australia Bank',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Centrelink',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Commonwealth Bank',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'St George Bank',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'St George Bank',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Subway',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'RAC Insurance',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Allianz',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Medibank',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Ikea',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'The Good Guys',    
        ]);
        DB::table('hook')->insert([
            'hook_name' => 'Samsung',    
        ]);

    }
}
