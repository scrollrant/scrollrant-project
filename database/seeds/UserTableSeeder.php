<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('user')->insert([
            'firstname' => 'reserve',
            'surname' => 'name',
            'aliasname' => 'Blackmamba',
            'genderID' => 1, 
            'countryID' => 1, 
            'stateID' => 1,  
            'password' => bcrypt('youshallnotpassyoung14youarenotauthorized'),
            'email' => 'scrollrant@exmple.com', 
            'dateofbirth' => '23/11/1992',        
        ]);
        DB::table('user')->insert([
            'firstname' => 'reserve',
            'surname' => 'name',
            'aliasname' => 'Scrollrant',
            'genderID' => 1, 
            'countryID' => 1, 
            'stateID' => 1,  
            'password' => bcrypt('youshallnotpassyoung14youarenotauthorized'),
            'email' => 'scrollrant@exmple1.com', 
            'dateofbirth' => '23/11/1992',        
        ]);
        DB::table('user')->insert([
            'firstname' => 'reserve',
            'surname' => 'name',
            'aliasname' => 'Scrollrant Official',
            'genderID' => 1, 
            'countryID' => 1, 
            'stateID' => 1,  
            'password' => bcrypt('youshallnotpassyoung14youarenotauthorized'),
            'email' => 'scrollrant@exmple2.com', 
            'dateofbirth' => '23/11/1992',        
        ]);
        DB::table('user')->insert([
            'firstname' => 'reserve',
            'surname' => 'name',
            'aliasname' => 'Scrollrant Team',
            'genderID' => 1, 
            'countryID' => 1, 
            'stateID' => 1,  
            'password' => bcrypt('youshallnotpassyoung14youarenotauthorized'),
            'email' => 'scrollrant@exmple3.com', 
            'dateofbirth' => '23/11/1992',        
        ]);

    }
}
