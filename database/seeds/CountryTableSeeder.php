<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('country')->insert([
            'country_name' => 'Australia',    
        ]);
        DB::table('country')->insert([
            'country_name' => 'America',    
        ]);
        DB::table('country')->insert([
            'country_name' => 'England',    
        ]);
        DB::table('country')->insert([
            'country_name' => 'Not Listed',    
        ]);
    }
}
