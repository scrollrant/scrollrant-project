<?php

use Illuminate\Database\Seeder;

class CompanyTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('company_type')->insert([
            'company_type' => 'Retail',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Media',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Banks',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Financial services',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Insurance',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Real Estate',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Telecommunications',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Travel and leisure',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Automobiles',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Food and beverage',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Education',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Entertainment',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Health & Medicine',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Home & Garden',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Legal & Financial',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Personal Care & Services',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Construction & Contractors',    
        ]);
        DB::table('company_type')->insert([
            'company_type' => 'Computers & Electronics',    
        ]);
    }
}
