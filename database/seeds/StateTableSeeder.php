<?php

use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Australian States
         */
        DB::table('state')->insert([
            'state_name' => 'Western Australia',
            'country_id' => 1,    
        ]);
        DB::table('state')->insert([
            'state_name' => 'New South Wales',
            'country_id' => 1,    
        ]);
        DB::table('state')->insert([
            'state_name' => 'Queensland',
            'country_id' => 1,    
        ]);
        DB::table('state')->insert([
            'state_name' => 'South Australia',
            'country_id' => 1,    
        ]);
        DB::table('state')->insert([
            'state_name' => 'Tasmania',
            'country_id' => 1,    
        ]);
        DB::table('state')->insert([
            'state_name' => 'Victoria',
            'country_id' => 1,    
        ]);

        /**
         * American States
         */
        DB::table('state')->insert([
            'state_name' => 'Alabama',
            'country_id' => 2,    
        ]);
	    DB::table('state')->insert([
	            'state_name' => 'Alaska',
	            'country_id' => 2,    
	        ]);
	    DB::table('state')->insert([
	            'state_name' => 'Arizona',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Arkansas',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'California',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Colorado',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Connecticut',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Delaware',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Florida',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Georgia',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Hawaii',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Idaho',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Illinois',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Indiana',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Iowa',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Kansas',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Kentucky',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Louisiana',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Maine',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Maryland',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Massachusetts',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Michigan',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Minnesota',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Mississippi',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Missouri',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Montana',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Nebraska',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Nevada',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'New Hampshire',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'New Jersey',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'New Mexico',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'New York',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'North Carolina',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'North Dakota',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Ohio',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Oklahoma',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Oregon',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Pennsylvania',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Rhode Island',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'South Carolina',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'South Dakota',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Tennessee',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Texas',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Utah',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Vermont',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Virginia',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Washington',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'West Virginia',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Wisconsin',
            'country_id' => 2,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Wyoming',
            'country_id' => 2,    
        ]);

        /**
         * England States
         */
        DB::table('state')->insert([
                'state_name' => 'Bedfordshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Berkshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Buckinghamshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Cambridgeshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Cheshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Cornwall',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Cumbria',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Derbyshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Devon',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Dorset',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Durham',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Essex',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Gloucestershire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Hampshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Hertfordshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Huntingdonshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Kent',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Lancashire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Leicestershire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Lincolnshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Middlesex',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Norfolk',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Northamptonshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Northumberland',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Nottinghamshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Oxfordshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Rutland',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Shropshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Somerset',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Staffordshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Suffolk',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Surrey',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Sussex',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Warwickshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Westmoreland',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Wiltshire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Worcestershire',
            'country_id' => 3,    
        ]);
        DB::table('state')->insert([
                'state_name' => 'Yorkshire',
            'country_id' => 3,    
        ]);

        /**
         * Not Listed
         */
        DB::table('state')->insert([
                'state_name' => 'State Not Listed',
            'country_id' => 4,    
        ]);


    }   
}
