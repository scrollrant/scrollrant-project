<?php

use Illuminate\Database\Seeder;

class UserStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('user_status')->insert([
            'status' => 'Athlete',    
        ]);
        DB::table('user_status')->insert([
            'status' => 'Actor',    
        ]);
        DB::table('user_status')->insert([
            'status' => 'Politician',    
        ]);
        DB::table('user_status')->insert([
            'status' => 'Artist',    
        ]);
    }
}
