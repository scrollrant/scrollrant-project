<?php

use Illuminate\Database\Seeder;

class RallyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('rant_rally')->insert([
            'user_name' => 'aleer_soccerboy@hotmail.com',
            'rally_coordinator_name' => 'ScrollRant Team',
            'rally_category' => 'information rally', 
            'rally_title' => 'ScrollRant Information Rally',
            'rally_description' => 'This is the official ScrollRant rally. This rally aims to keep all our awesome members informed about everything that ScrollRant will be up to. We will post up great statements about things that interest us, new features that are coming out or that we are thinking about implementing. We encourage you to join this rally and write comments about whatever you are thinking. We would also love for you to create your own rallies about anything you are passionate about, you will love it!',   
        ]);
    }
}
