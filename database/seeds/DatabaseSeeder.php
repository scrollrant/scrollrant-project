<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(CountryTableSeeder::class);
        // $this->call(StateTableSeeder::class);
        // $this->call(GenderTableSeeder::class);
        // $this->call(HookTableSeeder::class);
        // $this->call(ScopeTableSeeder::class);
        // $this->call(UserStatusTableSeeder::class);
        //$this->call(RantFaceTableSeeder::class);
          $this->call(ClientTableSeeder::class);
        Model::reguard();
    }
}
