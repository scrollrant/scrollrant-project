 $(function() {
    // var socket = io('http://scrollrant.com:3000', {'transports': ['polling']});
    var socket = io('https://scrollrant.com', {'transports': ['polling']});
    var comment_count = 0;
    
    socket.on("rant-channel:App\\Events\\CommentEvent", function(message){
    	
    //add the comment to the rant!
    // $('<a class="author">' +message.comment.firstname+' '+ message.comment.surname + '</a>').appendTo('#comment_'+message.comment.rantID);

    // $('#comment_'+message.comment.rantID).append('<div class="text">' +message.comment.comment+ '</div>');

        $('#comment_demo_'+message.comment.rantID).html('<a class="author">'+message.comment.firstname+'</a>'+'<p>'+message.comment.comment+ '</p>');
    
        $('<div class="comment">'+

            '<div class="content">'+

              '<a class="author">'+ message.comment.firstname +'</a>'+

              '<div class="metadata">'+

                '<span class="date">'+ message.comment.create_date + '</span>'+

              '</div>'+

              '<div class="text">'+

                message.comment.comment +

              '</div>'+

              '<div class="actions">'+

                // '<a class="reply">'+ 'Reply' + '</a>'+

              '</div>'+

            '</div>'+
            '</div>').appendTo('#p_comment'+message.comment.rantID);

    //increment the comment notification count each time a rant is created
    if (!comment_count) 
    $('#watch_'+message.comment.rantID).append('<div class="ui tiny red circular label" id="alert_'+message.comment.rantID+'">'+comment_count+'</div>');

    $('#count_'+message.comment.rantID).text(parseInt($('#count_'+message.comment.rantID).text()) + 1);

    $('#alert_'+message.comment.rantID).text(++comment_count);

    });
});