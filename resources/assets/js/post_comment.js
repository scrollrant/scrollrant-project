$(document).ready(function() {
  $('.post_comment').on('click', function(e){

    e.preventDefault() 

       //lets get our values from the form....
       var rantid = $(this).closest("form").find('.rant_id').val();

       var userid = $(this).closest("form").find('.user_id').val();

       var comment = $(this).closest("form").find('.user_comment').val();

       var token = $('meta[name="csrf-token"]').attr('content');
       
       // check for any empty values
       if (!rantid || !userid || !comment || !token)
       {
        return;
      }
      //now lets make our ajax call
      $.ajax({
        type: "POST",

        url: comment_url,

        data: { rant_id: rantid, user_id: userid, comment: comment, _token:token}
        
      }).done(function() {
        //empty the input value
        $('.user_comment').val('');
      });       
    });
});