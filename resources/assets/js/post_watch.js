$(document).ready(function() {
  $('.post_watch').on('click', function(e){

    e.preventDefault() 

       //lets get our values from the form....
       var rantid = $(this).closest("form").find('.rant_id').val();

       var userid = $(this).closest("form").find('.user_id').val();

       var token = $('meta[name="csrf-token"]').attr('content');
       
       var type = 'Watch';
       // check for any empty values
       if (!rantid || !userid || !token)
       {

        return;
      }
      //now lets make our ajax call
      $.ajax({
        type: "POST",

        url: watch_url,

        data: { rant_id: rantid, user_id: userid, action: type, _token:token}
        
      }).done(function() {

       
      });       
    });
});