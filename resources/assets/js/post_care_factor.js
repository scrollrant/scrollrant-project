$(function() {
  $('.post_care_factor').on('click', function(e){

    e.preventDefault() 

       //lets get our values from the form....
       var rantid = $(this).closest("form").find('.rant_id').val();

       var userid = $(this).closest("form").find('.user_id').val();

       var care_factor = $(this).closest("form").find(this).val();

       var token = $('meta[name="csrf-token"]').attr('content');
       
       // check for any empty values
       if (!rantid || !userid || !care_factor || !token)
       {
        return;
      }
      //now lets make our ajax call
      $.ajax({
        type: "POST",

        url: care_factor_url,

        data: { rant_id: rantid, user_id: userid, carefactor: care_factor, _token:token}
        
      }).done(function() {
        
          $('.post_care_factor').removeClass('inverted blue');

          $(e.target).addClass("inverted blue");         
      });       
    });
});