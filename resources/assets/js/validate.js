
$(document).ready(function() {
  $('.ui.form')
  .form({
          //on: 'blur',
          inline : true,
          fields: {
            email: {
              identifier  : 'email',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please enter your e-mail'
              },
              {
                type   : 'email',
                prompt : 'Please enter a valid e-mail'
              }
              ]
            },
            email_exists: {
              identifier : 'email_exists',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please enter your e-mail'
              },
              {
                type   : 'email',
                prompt : 'Please enter a valid e-mail'
              },
              {
                type   : 'checkUserName',
                prompt : 'This email is already taken'
              }

              ]
            },
            rant: {
              identifier : 'rant',
              rules: [
              {
                type   : 'empty',
                prompt : 'Enter your rant'
              },
              {
                type   : 'maxLength[500]',
                prompt : 'Please enter exactly 500 characters for your rant'
              }
              ]
            },
            password: {
              identifier  : 'password',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please enter your password'
              },
              {
                type   : 'length[6]',
                prompt : 'Your password must be at least 6 characters'
              }
              ]
            },
            first_name: {
              identifier  : 'first_name',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please enter your first name'
              },
              {
                type   : "regExp[^[a-zA-Z']+$]",
                prompt : 'First name must be characters only'
              },
              {
                type   : 'minLength[3]',
                prompt : 'First name must be at least 3 characters'
              }
              ]
            },
            surname: {
              identifier  : 'surname',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please enter your surname'
              },
              {
                type   : "regExp[^[a-zA-Z']+$]",
                prompt : 'Surname must be characters only'
              },
              {
                type   : 'minLength[3]',
                prompt : 'Surname must be at least 3 characters'
              }
              ]
            },
            alias_name: {
              identifier  : 'alias_name',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please enter your alias name'
              },
              {
                type   : 'minLength[3]',
                prompt : 'Alias name must be at least 3 characters'
              },
              {
                type   : 'checkAliasName',
                prompt : 'This alias name is already taken, too slow!'
              }
              ]
            },
            confirm_password: {
              identifier  : 'password_confirmation',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please enter your password again'
              },
              {
                type   : 'match[password]',
                prompt : 'You did not enter the same password'
              }
              ]
            },
            company_confirm_password: {
              identifier  : 'password_confirmation_company',
              rules: [
              {
                type   : 'match[company_password]',
                prompt : 'You did not enter the same password'
              } 
              ]
            },
            gender: {
              identifier  : 'gender',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please select a gender'
              }
              ]
            },
            month: {
              identifier  : 'month',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please select a month'
              }
              ]
            },
            day: {
              identifier  : 'day',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please enter a day'
              },
              {
                type   : 'integer[1..31]',
                prompt : 'Please enter valid day'
              },
              {
                type   : 'number',
                prompt : 'Please enter valid numbers'
              },
              {
                type   : 'minLength[1]',
                prompt : 'Day must be at least 1 digit'
              },
              {
                type   : 'maxLength[2]',
                prompt : 'Day can only be 2 digits max'
              },
              ]
            },
            year: {
              identifier  : 'year',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please enter a year'
              },
              {
                type   : 'exactLength[4]',
                prompt : 'Please enter exactly 4 digits'
              },
              {
                type   : 'number',
                prompt : 'Please enter valid numbers'
              }
              ]
            },
            country: {
              identifier  : 'country_id',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please select a country'
              }
              ]
            },
            state: {
              identifier  : 'state_id',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please select a state'
              }
              ]
            },
            comment: {
              identifier  : 'comment',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please select a comment'
              }
              ]
            },
            company_name: {
              identifier  : 'company_name',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please enter a company name'
              },
              {
                type   : 'checkCompanyName',
                prompt : 'This company name already exists'
              }
              ]
            },
            company_email: {
              identifier  : 'company_email',
              rules: [
              {
                type   : 'empty',
                prompt : 'Please enter the company email'
              },
              {
                type   : 'email',
                prompt : 'Please enter a valid e-mail'
              },
              {
                type   : 'checkCompanyEmail',
                prompt : 'This email  is already taken'
              }
              ]
            }

          }
        });
});