 $(function() {
    // var socket = io('http://scrollrant.com:3000', {'transports': ['polling']});
    var socket = io('https://scrollrant.com', {'transports': ['polling']});

    socket.on("rant-channel:App\\Events\\WatchEvent", function(message){

     
    //add watch to notifications!
    $('<div class="ui link list">'+
        
        '<div class="item" id="watch_'+message.watch.rant_id+'">'+

        '<a href="https://scrollrant.com/watch/'+message.watch.rant_id+'" class="item">'+message.watch.rant+'</a>'+

        '<button type="submit" class="delete_watch right floated" id="delete_watch'+message.watch.watch_id+'" data-rantid="'+message.watch.rant_id+'" data-watchid="'+message.watch.watch_id+'">X'+

        '</button>'+

        '</div>'+

        '</div>').appendTo('#user_notification_'+message.watch.user_id);
    // increment the watchers count 
    $('#watchers_'+message.watch.rant_id).text(parseInt($('#watchers_'+message.watch.rant_id).text()) + 1); 
    
    /**
     * delete a watch and remove it from the page
     * @param  
     * @return {[type]}      [description]
     */
    $('.delete_watch').last().on('click', function(e){

        e.preventDefault() 

        if (confirm("Do you want to stop watching this rant?"))
        {
            //lets get our values from the form....
            var token = $('meta[name="csrf-token"]').attr('content');

            var watch_id = $(e.target).attr('data-watchid');

            var rant_id = $(e.target).attr('data-rantid');

            //now lets make our ajax call
            $.ajax({
            type: "POST",

            url: delete_url+ '/' +watch_id,

            data: { watch_id: watch_id, rant_id: rant_id, _token:token}
            
            }).done(function() 
            {
                $('#delete_watch'+watch_id).closest('.link.list').remove();
            });       
        }
    });
});
});

