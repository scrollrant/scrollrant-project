$(function() {
  $('.delete_watch').on('click', function(e){

    e.preventDefault() 

      if (confirm("Do you want to stop watching this rant?"))
        {
            //lets get our values from the form....
            var token = $('meta[name="csrf-token"]').attr('content');

            var watch_id = $(e.target).attr('data-watchid');

            var rant_id = $(e.target).attr('data-rantid');

            //now lets make our ajax call
            $.ajax({
            type: "POST",

            url: delete_url+ '/' +watch_id,

            data: { watch_id: watch_id, rant_id: rant_id, _token:token}
            
            }).done(function() 
            {
                $('#delete_watch'+watch_id).closest('.link.list').remove();
            });       
        }
    
  });
});