$( document ).ready(function() {
	$('.ui.checkbox')
	.checkbox()
	;
	$('.ui.dropdown')
	.dropdown()
	;
	$('.upload')
	.popup({
		inline: true
	})
	;
	$('.hook')
	.popup({
		inline: true
	})
	;
	$('.show_hooks_popup')
	.popup({
		pop   : $('.my_hooks_popup.popup'),
		hoverable: true,
		position : 'bottom left',
		delay: {
			show: 300,
			hide: 800
		}
	})
	;
	$('.show_actions_popup.browse')
	.popup({
		pop   : $('.my_actions_popup.popup'),
		hoverable: true,
		position : 'bottom right',
		delay: {
			show: 300,
			hide: 800
		}
	})
	;
	$('.ui.modal.gender')
		.modal('attach events', '.gender', 'show')
	;
	$('.ui.modal.location')
		.modal('attach events', '.location', 'show')
	;
	$('.ui.modal.dob')
		.modal('attach events', '.dob', 'show')
	;
	
	$('.ui.accordion')
		.accordion()
	;
	$('.message .close')
	.on('click', function() {
		$(this)
		.closest('.message')
		.transition('fade')
		;
	})
	;
	$('.ui.rating')
	.rating()
	;



	$('.ui.search')
	  .search({
	    apiSettings: {
	      url: '//scrollrant.com/search/hooks/{query}'
	    },
	    fields: {
	      title   : 'hook_name',
	      url     : 'html_url'
	    },
	    minCharacters : 1
	  })
	;

// 	$('.container')
// 	.visibility({
// 		once: false,
//     // update size when new content loads
//     observeChanges: true,
//     // load content on bottom edge visible
//     onBottomVisible: function() {
//       // loads a max of 5 times
//       alert('you have reached the bottom');
//   }
// })
// 
//;
$('.ui.sticky')
.sticky({
	 offset  : 100,
	context: '#context',
});

});


