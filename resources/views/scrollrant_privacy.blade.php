@extends('auth.master')

@section('title', 'ScrollRant-Privacy')
@section('content')
@include('partials.navbar')
<div class="ui text container">

	<h3 class="">Privacy</h3>
	<div class="col s8 offset-s2 validate" name='privacy_policy' id="privacy_policy">
		<p>At ScrollRant, we collect and manage user data according to the following Privacy Policy, with the goal of incorporating our website values: transparency, accessibility, sanity, usability. This document is part of ScrollRant's <a href="{{{ URL::to('terms') }}}" target="_blank">Terms of Service</a>, and by using scrollrant.com, you agree to the terms of this Privacy Policy and the Terms of Service. Please read the Terms of Service in their entirety, and refer to those for definitions and contacts.</p> 
		<p></p> 
		<h5 style="text-align: left;">Data Collected</h5> 
		<p>We collect data from every visitor and member of the Website to provide users with the best service possible. For example, we collect information such as:</p>
		<ol>
			<li style="text-align: left;">Your Regestration Information when you sign up to ScrollRant.</li>
			<li style="text-align: left;">All Inforation you post up on ScrollRant.</li>
		</ol>
		<p>We ask you to log in and provide certain personal information (such as your name and email address) in order to be able to save your profile and the rants, rallies and comments associated with it. In order to enable these or any other login based features, we use cookies to store session information for your convenience. You can block or delete cookies and still be able to use ScrollRant, although if you do you will then be asked for your username and password every time you log in to the Website. In order to take advantage of certain features of the Website.</p> 
		<p>You are able to view, change and remove some of the data associated with your profile. Should you choose to delete your account, please contact us on our facebook page facebook.com/scrollrant and we will follow up with such request as soon as possible.</p> 
		<p>Minors and children should not use ScrollRant(scrollrant.com). By using the Website, you represent that you have the legal capacity to enter into a binding agreement.</p> 
		<h5 style="text-align: left;">Use of the Data</h5> 
		<p>We only use your personal information to provide you ScrollRant services or to communicate with you about the services or the Website.</p> 
		<p>With respect to any rants you may choose to post on ScrollRant, we allow you the benefit of posting rants privately or publicly. We can't protect public data and we are not responsible for any violation of privacy law you may be liable for.</p> 
		<p>We employ industry standard techniques to protect against unauthorized access of data about you that we store, including personal information.</p> 
		<p><strong>We do not share personal information you have provided to us without your consent</strong>, unless:</p> 
		<ol> 
			<li style="text-align: left;">doing so is appropriate to carry out your own request;</li> 
			<li style="text-align: left;">we believe it's needed to enforce our Terms of Service, or that is legally required;</li> 
			<li style="text-align: left;">we believe it's needed to detect, prevent or address fraud, security or technical issues;</li> 
			<li style="text-align: left;">we believe it's needed to appropriate social research;</li> 
			<li style="text-align: left;">otherwise protect our property, legal rights, or that of others.</li> 
		</ol> 
		<p>ScrollRant is operated from Australia. If you are visiting the Website from outside Australia, you agree to any processing of any personal information you provide us according to this policy.</p> 
		<p>ScrollRant may contact you, by email or other means. For example, ScrollRant may send you promotional emails relating to ScrollRant or other third parties ScrollRant feels you would be interested in, or communicate with you about your use of the ScrollRant website. ScrollRant may also use technology to alert us via a confirmation email when you open an email from us.</p> 
		<h5 style="text-align: left;">Sharing of Data</h5> 
		<p>We don't share your personal information with third parties. Only aggregated, anonymized data is periodically transmitted to external services to help us improve the ScrollRant Website and service. We currently use Analytics tools.</p> 
		
		<p>We also use social buttons provided by services like Twitter, Google+, LinkedIn and Facebook. Your use of these third party services is entirely optional. We are not responsible for the privacy policies and/or practices of these third party services, and you are responsible for reading and understanding those third party services’ privacy policies.</p> 
		<p>We employ and contract with people and other entities that perform certain tasks on our behalf and who are under our control (our “Agents”). We may need to share personal information with our Agents in order to provide products or services to you. Unless we tell you differently, our Agents do not have any right to use Personal Information or other information we share with them beyond what is necessary to assist us. You hereby consent to our sharing of Personal Information with our Agents.</p> 
		<p>We may choose to buy or sell assets. In these types of transactions, user information is typically one of the transferred business assets. Moreover, if we, or substantially all of our assets, were acquired, or if we go out of business or enter bankruptcy, user information would be one of the assets that is transferred or acquired by a third party. You acknowledge that such transfers may occur, and that any acquirer of us or our assets may continue to use your personal information as set forth in this policy.</p> 
		<h5 style="text-align: left;">Changes to the Privacy Policy</h5>
		<p>We may amend this Privacy Policy from time to time. Use of information we collect now is subject to the Privacy Policy in effect at the time such information is used. If we make major changes in the way we collect or use information, we will notify you by posting an announcement on the Website or sending you an email. A user is bound by any changes to the Privacy Policy when he or she uses the Services after such changes have been first posted.</p> 
		<p>Should you have any question or concern, please write to facebook.com/scrollrant!</p>
	</div>
</div>
@endsection


