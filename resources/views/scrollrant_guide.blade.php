@extends('auth.master')

@section('title', 'ScrollRant-Guide')
@section('content')
@include('partials.navbar')
<div class="ui text container">

	<h3 class="">Guide</h3>
	<div class="col s8 offset-s2 validate" name='guide' id="guide">
		<ol>

			<li>
				<h5 id="profile" class="left-align">Profile</h5>
				<p style="text-align: left;"> <b>Rants - </b> rants are the essence of ScrollRant, they are meant to be creative, honest and to the point. 
					If something is worth ranting about, ScrollRant is the first place you should come to vent!
				</p>
			</p>
			<p style="text-align: left;">All the rants that you make will always be available to you on your profile. There you can delete them and browse them.
			</p>
		</li>
		<div class="divider blue"></div>
		<li>
			<h5 id="rant_wall" class="left-align">Rants</h5>
			<p>The rants page is where all the rants go to be heard, read and appreciated! You will find very interesting content here
			</p>
			<p><b>Care Button - </b> the care button is an important feature of ScrollRant and its very easy to use. If you care about someones rant
				then just press care! Cares dictate how important a rant is.
			</p>
			<p><b>Don't Care Button - </b> the don't care button is the opposite of the care button. If you don't care about a rant then dont be afraid to click
				the don't care button. Not caring is NOT a mean gesture on ScrollRant, it is perfectly normal to care about somethings and not others in life and
				we recognise that for you!
			</p>
			<p><b>Comments - </b> comment on rants in REAL TIME! commenting is meant to be fun, interesting, educational and mature! Post comments that people will
				appreciate and most of all let it be your personality.
			</p>
			<p><b>Watch - </b> If there is a rant that you are interested in and would like to get notifications for, then simply watch the rant in the actions dropdown. 
			</p>
		</li>
		<div class="divider blue"></div>
		<li>
			<h5 id="rally_wall" class="left-align">Rant Face</h5>
			<p>
				Every rant you make can have a rant face that appears as an empty circle that you can click on. A rant face gives you a chance to add an expression to your rant. More faces will be added so keep an eye out for them!
			</p>
		</li>
	</ol>			

</div>
</div>
@endsection


