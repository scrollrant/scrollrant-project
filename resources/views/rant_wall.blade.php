@extends('auth.master')

@section('title', 'ScrollRant-Rants')
@section('content')
@include('partials.navbar_fixed')
	<div class="ui  grid centered container " id="context" style="min-height: 1000px !important;">
		<!-- rants and form column computer only-->
		
		<div class="sixteen wide mobile sixteen wide tablet ten wide computer column top-space">
			
			<div class="left ui rail very close computer only" style="min-height: 1000px;">
				<div class="ui sticky" style="height: 1000px !important;" >
					@include('partials.user_details')

					@include('partials.notifications')	

					@include('partials.statistics')

					
				</div>
			</div>

			<div class="right ui rail very close computer only">
				<div class="ui sticky">
					@include('partials.trending')

					@include('partials.most_hooked')
				</div>
			</div>
			
			@foreach ($all_rants as $show_rant)
				@include('partials.rant')
			@endforeach
		</div>
	</div>
@stop
