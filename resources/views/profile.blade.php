@extends('auth.master')

@section('title', 'ScrollRant-Profile')
@section('content')
@include('partials.navbar_fixed')
<div class="ui grid centered container" >
		
		<!-- rants and form column computer only-->
		<div class="sixteen wide mobile sixteen wide tablet ten wide computer column top-space" id="context" style="min-height: 1000px !important;">
			
			<div class="left ui rail very close computer only" style="min-height: 1000px;">
				<div class="ui sticky" style="height: 1000px !important;" >
					@include('partials.user_details')

					@include('partials.notifications')	

					@include('partials.statistics')

					
				</div>
			</div>

			<div class="right ui rail very close computer only">
				<div class="ui sticky">
					@include('partials.trending')

					@include('partials.most_hooked')
				</div>
			</div>

			
			<!--rant form -->
			<div class="ui segment my-side-menu" style="margin-top: -13px;">

				@include('partials.make_rant')

			</div>

			<!-- show the steps info if there are details to complete -->
			@if(!Auth::user()->gender_id || !Auth::user()->country_id || !Auth::user()->state_id || !Auth::user()->date_of_birth)
			<div class="ui four steps">	

				<div class="completed step">
					<i class="small icon"></i>
					<div class="content">
						<div class="title">Basic Profile</div>
						<div class="description">You have a basic profile</div>
					</div>
				</div >

				@if(!isset(Auth::user()->gender_id))
				<a class="gender step">
					<div class="content">
						<div class="title">Gender</div>
						<div class="description">Enter your gender</div>
					</div>
				</a>
				@else
				<div class="completed step">
					<i class="small icon"></i>
					<div class="content">
						<div class="title">Gender</div>
						<div class="description">Thanks for updating gender</div>
					</div>
				</div >
				@endif
				
				@if(!Auth::user()->state_id || !Auth::user()->country_id)	
				<a class="location step">
					<div class="content">
						<div class="title">Location</div>
						<div class="description">Where you are from</div>
					</div>
				</a>
				@else
				<div class="completed step">
					<i class="small icon"></i>
					<div class="content">
						<div class="title">Location</div>
						<div class="description">Thanks for updating your location</div>
					</div>
				</div >
				@endif
				@if(!Auth::user()->date_of_birth)
				<a class="dob step">
					<div class="content">
						<div class="title">Date of birth</div>
						<div class="description">Enter your age</div>
					</div>
				</a>
				@else
				<div class="completed step">
					<i class="small icon"></i>
					<div class="content">
						<div class="title">Date of birth</div>
						<div class="description">Thanks for updating your date of birth</div>
					</div>
				</div >
				@endif
				
				@if(!isset(Auth::user()->gender_id))
					@include('partials.gender_modal')
				@endif
				
				@if(!Auth::user()->state_id || !Auth::user()->country_id || Auth::user()->country_id == 4)
					@include('partials.location_modal')
				@endif

				@if(!Auth::user()->date_of_birth)
					@include('partials.dob_modal')
				@endif


			</div>
			@endif
			
			<!-- the divider for YOUR RANTS -->
			<div class="ui horizontal divider">YOUR RANTS</div>
			
			<!-- the users rants as cards -->
			@foreach ($user_rants as $show_rant)
			@include('partials.rant')
			@endforeach

			@if (!$user_rants->count())
				<div class="ui warning message">
				  <div class="header">
				    Well this is awkward!
				  </div>
				  Your profile is looking a little stale, spice it up with some juicy rants.
				</div>
			@endif
			@include('partials.link_accounts_modal')
		</div>		
</div>
@stop
