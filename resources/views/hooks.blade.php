@extends('auth.master')

@section('title', 'ScrollRant- Hooks')
@section('content')
@if (Auth::user('user'))
@include('partials.navbar_fixed')
@else
@include('partials.navbar')
@endif
	<div class="ui  grid centered container " id="context" style="min-height: 1000px !important;">
		<!-- rants and form column computer only-->
		
		<div class="sixteen wide mobile sixteen wide tablet ten wide computer column top-space">
			
			@if (Auth::user('user'))
				<div class="left ui rail very close computer only" style="min-height: 1000px;">
					<div class="ui sticky" style="height: 1000px !important;" >
						@include('partials.user_details')

						@include('partials.notifications')	

						@include('partials.statistics')			
					</div>
				</div>
		
				<div class="right ui rail very close computer only">
					<div class="ui sticky">
						@include('partials.trending')

						@include('partials.most_hooked')
					</div>
				</div>
			@endif
			
			@if (!$hooked_rants->isEmpty())
			@foreach ($hooked_rants as $show_rant)
				@include('partials.rant')
			@endforeach
				@if (!Auth::user('user'))
					<a class="ui blue header center aligned" href="{{ url('login') }}">Sign in to view more and create rants...</a>
				@endif
			@else
				<div class="ui segment">
					<p>There are not rants for this hook yet, sign in to create a rant!</p>
				</div>
			@endif
		</div>
	</div>
	@if (!Auth::user('user'))
	@include('partials.footer')
	@endif
@stop

