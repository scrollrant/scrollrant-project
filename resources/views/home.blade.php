 
@extends('auth.master')

@section('title', 'Scrollrant - Home')
@section('content')
@include('partials.navbar')
<div class="ui container content_height">
	<div class="ui grid bottom-gap">
		<div class="five wide column computer only">
			<div class="ui center aligned container">
				<i class="huge circular blue users icon"></i>
				<h2 class="ui header">Customer Complaints</h2>
				<div class="ui medium  header">Make complaints to any businesses and get responses back in real time.</div>
			</div>
			<div class="ui section divider"></div>
			<div>
				
				@if(Inspiring::quote())
				<h3 class="ui header">There is always something to rant about!</h3>
				<p class="ui header">	
					<i class="red quote left icon"></i> 
					{{ Inspiring::quote()->rant }}		
					<i class="right floated red quote right icon"></i> 
				</p>
				@endif
			</div>
		</div>
		<div class="four wide column computer only">
			<div class="ui center aligned container">
				<i class="huge circular blue protect icon"></i>
				<h2 class="ui header">Rant Privately</h2>
				<div class="ui medium header">Rant about anything privately and get real time comments.</div>
			</div> 
		</div>
		
		<!-- Login Form computer only-->
		<div class="seven wide column computer only">
			<div class="ui segment">
				<h5 class="ui header center aligned">USER LOGIN WITH</h5>
					<div  class="ui basic segment center aligned">
						<a class="ui facebook button" href="{{{ URL::to('login/callback/facebook') }}}">
						  <i class="facebook icon"></i>
						  Facebook
						</a>
						
						<a class="ui twitter button" href="{{{ URL::to('login/callback/twitter') }}}">
						  <i class="twitter icon"></i>
						  Twitter
						</a>
						{{--
						<a class="ui google plus button" href="{{{ URL::to('login/callback/google') }}}">
						  <i class="google plus icon"></i>
						  Google Plus
						</a>
						--}}
					</div>
				<div class="ui horizontal divider">Or</div>
				<form class="ui large form" role="form" id="login_form" method="POST" action="{{ url('login') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="field">
						<div class="ui left icon input">
							<i class="user icon"></i>
							<input type="text" name="email" data-validate="email" placeholder="E-mail address" value="{{ old('email') }}">
						</div>
					</div>
					<div class="field">
						<div class="ui left icon input">
							<i class="lock icon"></i>
							<input type="password" name="password" data-validate="password" placeholder="Password">
						</div>
					</div>
					<div class="inline field">
						<div class="ui checkbox">
							<input type="checkbox" tabindex="0" class="hidden" id='remember' name="remember">
							<label>Remember Me</label>
						</div>
					</div>
					<button type="submit" class="ui fluid large blue submit button">
						Login
					</button>
					@if (count($errors) > 0)
					<!-- error message for server side validation -->
					<div class="ui error message visible">
						<div class="small header">
							There was some errors with your submission
						</div>
						<ul class="list">						  
							@if ($errors->has('email'))<li><p style="color:red;">{!!$errors->first('email')!!}</p>@endif</li>
							@if ($errors->has('password'))<li><p style="color:red;">{!!$errors->first('password')!!}</p>@endif</li>
							@if ($errors->has('invalid_login'))<li><p style="color:red;">{!!$errors->first('invalid_login')!!}</p>@endif</li>
						</ul>
					</div>
					@endif
				</form>
				<div class="ui message">
					<a href="{{{ URL::to('register') }}}">Sign Up</a> |

					<a class="" href="{{{ URL::to('/password/email') }}}">Forgot your password?</a>
				</div>
			</div>
		</div>
		<!-- Login Form mobile only-->
		<div class="sixteen wide column tablet mobile only">
			<div class="ui  red segment">
				<h5 class="ui header center aligned">USER LOGIN WITH</h5>
				<form class="ui basic segment center aligned" role="form" id="login_form" method="POST" action="{{ url('social_login') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
						
						<a class="ui facebook button" href="{{{ URL::to('login/callback/facebook') }}}">
						  <i class="facebook icon"></i>
						  Facebook
						</a>
						
						<a class="ui twitter button" href="{{{ URL::to('login/callback/twitter') }}}">
						  <i class="twitter icon"></i>
						  Twitter
						</a>
						{{--
						<a class="ui google plus button" href="{{{ URL::to('login/callback/google') }}}">
						  <i class="google plus icon"></i>
						  Google Plus
						</a>
						--}}
				</form>
				<div class="ui horizontal divider">Or</div>
				<form class="ui large form" role="form" id="login_form" method="POST" action="{{ url('login') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="field">
						<div class="ui left icon input">
							<i class="user icon"></i>
							<input type="text" name="email" data-validate="email" placeholder="E-mail address" value="{{ old('email') }}">
						</div>
					</div>
					<div class="field">
						<div class="ui left icon input">
							<i class="lock icon"></i>
							<input type="password" name="password" data-validate="password" placeholder="Password">
						</div>
					</div>
					<div class="inline field">
						<div class="ui checkbox">
							<input type="checkbox" tabindex="0" class="hidden" id='remember' name="remember">
							<label>Remember Me</label>
						</div>
					</div>
					<button type="submit" class="ui fluid large blue submit button">
						Login
					</button>
					@if (count($errors) > 0)
					<!-- error message for server side validation -->
					<div class="ui error message visible">
						<div class="small header">
							There was some errors with your submission
						</div>
						<ul class="list">						  
							@if ($errors->has('email'))<li><p style="color:red;">{!!$errors->first('email')!!}</p>@endif</li>
							@if ($errors->has('password'))<li><p style="color:red;">{!!$errors->first('password')!!}</p>@endif</li>
							@if ($errors->has('invalid_login'))<li><p style="color:red;">{!!$errors->first('invalid_login')!!}</p>@endif</li>
						</ul>
					</div>
					@endif
				</form>
				<div class="ui message">
					<a href="{{{ URL::to('register') }}}">Sign Up</a> |

					<a class="" href="{{{ URL::to('/password/email') }}}">Forgot your password?</a>
				</div>
			</div>
		</div>
	</div>

	
</div>
@include('partials.footer')
@stop

