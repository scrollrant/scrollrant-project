@extends('auth.master')

@section('title', 'ScrollRant- Manage Account')
@section('content')
@include('partials.navbar_fixed')

<div class="ui grid centered container" >
	<!-- for for computer view only -->
	<div class="sixteen wide mobile sixteen wide tablet ten wide computer column top-space" id="context" style="min-height: 1000px !important;">
		
		<div class="left ui rail very close computer only" style="min-height: 1000px;">
			<div class="ui sticky" style="height: 1000px !important;" >
				@include('partials.user_details')

				@include('partials.notifications')	

				@include('partials.statistics')
			</div>
		</div>

		<div class="right ui rail very close computer only">
			<div class="ui sticky">
				@include('partials.trending')

				@include('partials.most_hooked')
			</div>
		</div>
		{{--
		<!-- the divider for EDIT PROFILE -->
		<div class="ui horizontal divider">EDIT PROFILE</div>
		<div class="ui segment center aligned">
			<form class="ui large form" role="form" id="update_alias_name" method="POST" action="{{ url('update_user') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="fields">
			      <div class="twelve wide field">
			      	<label>Alias Name</label>
			        <input type="text" name="alias_name" value="{{ Auth::user()->alias_name }}" placeholder="Alias Name">
			      </div>
			       <div class="twelve wide field">
			       	<label>First Name</label>
			        <input type="text" name="first_name" value="{{ Auth::user()->first_name }}" placeholder="First Name">
			      </div>
			       <div class="twelve wide field">
			       	<label>Surame</label>
			        <input type="text" name="surname" value="{{ Auth::user()->surname }}" placeholder="Enter Surname">
			      </div>
			    </div>
			    <div class="inline fields">
			      <div class="twelve wide field">
			      
			        <input type="password" name="password"  placeholder="Enter New Password">
			      </div>
			      <div class="fluid wide field">
			        <label>Password</label>
			      </div>
			    </div>
			    <div class="ui  message">
				  <div class="header">
				    All the fields above can not be empty!
				  </div>
				</div>
			     <div class="ui clearing divider"></div>
			    <div class="fluid wide field">
			        <button class="ui fluid large blue button" type="submit">Update</button>
			    </div>	
			</form>
		</div>
		--}}
		<!-- the divider for LINK ACCOUNTS -->
		<div class="ui horizontal divider">LINK ACCOUNTS</div>
		<div class="ui segment">
			<h4 class="ui header">Link these social accounts with one click</h4>
			@include('partials.user_linked_accounts')
		</div>
		<!-- the divider for INVITE FRIENDS -->
		<div class="ui horizontal divider">INVITE FRIENDS</div>
		<div class="ui segment">
			<form class="ui large form" role="form" id="friend_invite_form" method="POST" action="{{ url('invite') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="field">
					<label>Invite all your friends and have a vent fest</label>
					<input type="text" name="friend_name" placeholder="Friend's Name" value="{{ old('name') }}">
				</div>
				<div class="field">
					<input type="text" name="friend_email" placeholder="Friend's Email" value="{{ old('email') }}">
				</div>
				<div class="field">
					<button class="ui fluid large blue button" type="submit">Send Invite</button>
				</div>
			</form>
		</div>
		<!-- the divider for YOUR RANTS -->
		<div class="ui horizontal divider">GIVE FEEDBACK</div>

		<div class="ui segment">
			<form class="ui large form" role="form" id="feedback_form" method="POST" action="{{ url('feedback') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="field">
					<label>We value what you have to say</label>
					<input type="text" name="name" placeholder="First Name" value="{{ Auth::user('user')->first_name }}">
				</div>
				<div class="field">
					<select name="feedback_type" class="ui dropdown">
						<option value='' selected="selected">Select Feedback Type </option>
						<option value='01' {{ (old("feedback_type") == 01 ? "selected":"") }} >Sugguest Feature</option>
						<option value='02' {{ (old("feedback_type") == 02 ? "selected":"") }}>Functionality</option>
						<option value='03' {{ (old("feedback_type") == 03 ? "selected":"") }}>Community Evaluation</option>
						<option value='04' {{ (old("feedback_type") == 04 ? "selected":"") }}>Bug</option>
					</select>
				</div>
				<div class="field">
					<textarea name="message" style="height: 170px;" placeholder="Enter your feedback here" length="500">{{ old('rant') }}</textarea>
				</div>
				<div class="field">
					<button class="ui fluid blue button" type="submit">Submit your feedback</button>
				</div>

				@include('partials.errors')
			</form>
		</div>
		

	</div>
</div>
@stop