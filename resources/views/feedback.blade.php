@extends('auth.master')

@section('title', 'ScrollRant-Feedback')
@section('content')
@include('partials.navbar_fixed')

<div class="ui grid centered container" >
	<!-- for for computer view only -->
	<div class="sixteen wide mobile sixteen wide tablet ten wide computer column top-space">
		<div class="ui segment">
			<form class="ui large form" role="form" id="feedback_form" method="POST" action="{{ url('feedback') }}" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="field">
					<label>Your feedback is appreciated</label>
					<input type="text" name="name" placeholder="First Name" value="{{ Auth::user('user')->first_name }}">
				</div>
				<div class="field">
					<select name="feedback_type" class="ui dropdown">
						<option value='' selected="selected">Select Feedback Type </option>
						<option value='01' {{ (old("feedback_type") == 01 ? "selected":"") }} >Sugguest Feature</option>
						<option value='02' {{ (old("feedback_type") == 02 ? "selected":"") }}>Functionality</option>
						<option value='03' {{ (old("feedback_type") == 03 ? "selected":"") }}>Community Evaluation</option>
						<option value='04' {{ (old("feedback_type") == 04 ? "selected":"") }}>Bug</option>
					</select>
				</div>
				<div class="field">
					<textarea name="message" style="height: 170px;" placeholder="Enter your feedback here" length="500">{{ old('rant') }}</textarea>
				</div>
				<div class="field">
					<button class="ui fluid blue button" type="submit">Submit your feedback</button>
				</div>

				@include('partials.errors')
			</form>
		</div>
		<div class="ui segment">
			<form class="ui large form" role="form" id="friend_invite_form" method="POST" action="{{ url('invite') }}" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="field">
					<label>Invite a friend</label>
					<input type="text" name="friend_name" placeholder="Friend's Name" value="{{ old('name') }}">
				</div>
				<div class="field">
					<input type="text" name="friend_email" placeholder="Friend's Email" value="{{ old('email') }}">
				</div>
				<div class="field">
					<button class="ui fluid green button" type="submit">OK</button>
				</div>
			</form>
		</div>
	</div>
</div>
@stop