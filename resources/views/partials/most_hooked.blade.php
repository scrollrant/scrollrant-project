<div class="ui segment my-side-menu">
	<h5 class="ui red header align left">
		Most Hooked Companies

	</h5>
	 <div class="item">
      <div class="ui search" >
      <div class="ui icon input" style="width: 250px;" >
        <input class="prompt" type="text" placeholder="Search hooks..." style="background-color: #F8F8F9;">
        <i class="search icon"></i>
      </div>
      </div>
   </div>
	<div class="ui  clearing divider"></div>

   
	<div class="ui link list">
		@foreach ($most_hooked as $hooked)
		@if ($hooked->make_hooks->count())
		<div class="item">
			<a href="#" class="item">{{$hooked->hook_name}}</a>
			<!-- <i class="info icon right floated"></i> -->
		</div>
		@endif
		@endforeach
	</div>
</div>