<div class="ui fixed borderless  menu grid" style="background-color:#b30000;">

  <div class="tablet and mobile only row">
   <div class="item">
      @if (Auth::user('user'))
        <a class="ui inverted large header center aligned" href="{{ url('profile') }}">Scroll<b>Rant</b></a>
      @else
        <a class="ui inverted large header center aligned" href="{{ url('home') }}">Scroll<b>Rant</b></a>
      @endif
   </div>
   @if (!Auth::user('user') && !Auth::user('company'))
   <a href="{{{url('home')}}}" class=" item">
    Home
  </a>
  @endif
  @if (Auth::user('user'))
  <a href="{{{url('profile')}}}" class=" item">
    <i class="user icon"></i>
    ({{Auth::user('user')->first_name}})
  </a>
  @endif
  @if (Auth::user('company'))
  <a href="{{{url('company_profile')}}}" class="item">
    <i class="user icon"></i>
    Profile
    ({{Auth::user('company')->company_name}})
  </a>
  @endif
  <a class="item" href="{{{url('rants')}}}">
    Rants
  </a>

  <div class="ui dropdown item">
     <i class="dropdown icon"></i>
    <div class="menu">
      <a class="item" href="{{{url('manage')}}}">
         <i class="settings icon"></i>
        Manage
      </a>
      
      @if (Auth::user('user'))
      <div class="item">
        <form action="{{url ('logout')}}" role="form" method="POST" class='col'>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button class="ui icon large red button log_out" type="submit"  name="action">
            <i class="lock icon"></i>
            Logout
          </button>
          @if ($errors->has('logout'))<p style="color:red;">{!!$errors->first('logout')!!}</p>@endif
        </form>
      </div>
      @endif

      @if (Auth::user('company'))
      <div class="item">
        <form action="{{url ('company_logout')}}" role="form" method="POST" class='col'>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button class="ui icon  button log_out" type="submit"  name="action">
            <i class="lock icon"></i>
          </button>
          @if ($errors->has('logout'))<p style="color:red;">{!!$errors->first('logout')!!}</p>@endif
        </form>
      </div>
      @endif
    </div>
  </div>
</div>

<div class="computer only row">

  <div class="item">
    @if (Auth::user('user'))
      <a class="ui inverted large header center aligned" href="{{ url('profile') }}">Scroll<b>Rant</b></a>
    @else
      <a class="ui inverted large header center aligned" href="{{ url('home') }}">Scroll<b>Rant</b></a>
    @endif
 </div>

 @if (!Auth::user('user') && !Auth::user('company'))
 <a href="{{{url('home')}}}" class="right item">
  Home
</a>
@endif
@if (Auth::user('user'))
<a href="{{{url('profile')}}}" class="right item">
  <i class="user icon"></i>
  Profile
  ({{Auth::user('user')->first_name}})
</a>
@endif
@if (Auth::user('company'))
<a href="{{{url('company_profile')}}}" class="right item">
  <i class="user icon"></i>
  Profile
  ({{Auth::user('company')->company_name}})
</a>
@endif
<a class="item" href="{{{url('rants')}}}">
  Rants
</a>
<a class="item" href="{{{url('manage')}}}">
  <i class="settings icon"></i>
    Manage
</a>


@if (Auth::user('user'))
<div class="item">
  <form action="{{url ('logout')}}" role="form" method="POST" class='col'>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button class="ui icon  inverted basic button log_out" title="logout" data-position="bottom center" data-variation="basic"  type="submit"  name="action">
      <i class="lock icon"></i>
    </button>
    @if ($errors->has('logout'))<p style="color:red;">{!!$errors->first('logout')!!}</p>@endif
  </form>
</div>
@endif

@if (Auth::user('company'))
<div class="item">
  <form action="{{url ('company_logout')}}" role="form" method="POST" class='col'>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button class="ui icon  button log_out" type="submit"  name="action">
      <i class="lock icon"></i>
    </button>
    @if ($errors->has('logout'))<p style="color:red;">{!!$errors->first('logout')!!}</p>@endif
  </form>
</div>
@endif
</div>
</div>