
@if ($user_linked_accounts->where('provider_name', 'facebook')->first()['provider_name'] === 'facebook')
<form  style='display:inline;' role="form" id="unlink_account_page" method="POST" action="{{ url('unlink_social_account') }}">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="social_account_id" value="{{ $user_linked_accounts->where('provider_name', 'facebook')->first()['social_account_login_id'] }}">
	<button type="submit" class="ui basic button">
	  <i class="icon unlock"></i>
	 Unlink Facebook 
	</button>
</form>
@else
<a class="ui huge facebook button" href="{{ url('login/callback/facebook') }}">
  <i class="facebook icon"></i>
  Facebook
</a>
@endif

@if ($user_linked_accounts->where('provider_name', 'twitter')->first()['provider_name'] == 'twitter')
<form  style='display:inline;' role="form" id="unlink_account_page" method="POST" action="{{ url('unlink_social_account') }}">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="social_account_id" value="{{ $user_linked_accounts->where('provider_name', 'twitter')->first()['social_account_login_id'] }}">
	<button type="submit" class="ui basic button">
	  <i class="icon unlock"></i>
	 Unlink Twitter 
	</button>
</form>
@else
<a class="ui huge twitter button" href="{{{ URL::to('login/callback/twitter') }}}">
  <i class="twitter icon"></i>
  Twitter
</a>
@endif
{{--
@if ($user_linked_accounts->where('provider_name', 'google')->first()['provider_name'] == 'google')
<form  style='display:inline;' role="form" id="unlink_account_page" method="POST" action="{{ url('unlink_social_account') }}">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="social_account_id" value="{{ $user_linked_accounts->where('provider_name', 'google')->first()['social_account_login_id'] }}">
	<button type="submit" class="ui basic button">
	  <i class="icon unlock"></i>
	 Unlink Google 
	</button>
</form>
@else
<a class="ui huge google plus button" href="{{{ URL::to('login/callback/google') }}}">
  <i class="google plus icon"></i>
  Google Plus
</a>
@endif
--}}
