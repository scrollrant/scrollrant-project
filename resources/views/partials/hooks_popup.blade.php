	@foreach ($show_rant->hooks as $rant_hook)
	<div class="ui flowing popup my_hooks_popup transition hidden">
		<div class="ui  grid">
			<div class="column">
				<div class="card">
					<div class="content">
						@if($rant_hook->company_id != null)
						<img class="right floated tiny ui image" width="128px" height="128px" src="{{$rant_hook}}">
						@else
						<img class="right floated tiny ui image" width="128px" height="128px" src="/images/no_company_icon.png">
						@endif
						<div class="header">
							
							{{$rant_hook->hook_name}} 
							@if($rant_hook->company_id != null)
							<i class="check blue square icon"></i>
							@else
							<i class="warning sign icon"></i>
							@endif

						</div>
						<div class="meta">
							@if($rant_hook->company_id != null)
							Category: NA
							@endif

						</div>
						<div class="description">
							@if($rant_hook->company_id != null)
							<p>This hook has a scrollrant account, so they will able to recieve your rants.</p>
							@else
							This hook has no company associated with it so only users will respond to your complaints.
							@endif
						</div>
						<div class="extra content">
							<span>
								<i class="tag icon"></i>
								Total Hooks: {{$rant_hook->make_hooks->count()}}
							</span class="right floated">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endforeach