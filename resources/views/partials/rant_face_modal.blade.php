<!-- Rant face model this partial is no longer needed because its being used directly in the rant-->
<div class="ui modal" id="rant_face{{$show_rant->rant_id}}">
	<div class="header">
		A face speaks louder than words!
	</div>
	<div class="content">
		<form class="ui large form" role="form" id="rant_face_form" method="POST" action="{{ url('update_rant_face') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="rant_id" value="{{$show_rant->rant_id}}">
			<div class="image content">
				<div class="ui four cards">
					<div class="card">
						<div class="image">
							<!-- <img src="images/pissed.png"> -->
							<input type="image" class="ui medium image" value="pissed_face" name="rant_face_name" src="images/pissed.png" />

						</div>
						<div class="extra">
							Pissed:
							<div class="ui star rating" data-rating="4"></div>
						</div>
					</div>
					<div class="card">
						<div class="image">
							<!-- <img src="images/irritated.png"> -->
							<input type="image" class="ui medium image" value="irrated_face" name="rant_face_name" src="images/irritated.png" />
						</div>
						<div class="extra">
							Irrated:
							<div class="ui star rating" data-rating="2"></div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>