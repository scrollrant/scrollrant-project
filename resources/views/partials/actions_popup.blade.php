@if (Auth::user('user'))
<a class="ui browse show_actions_popup item right floated">
	<i class=" large blue ellipsis vertical medium icon"></i>
</a>
	<!--create an action on the rant form -->
	<form  class="ui my_actions_popup popup transition hidden" role="form" id="rant_actions_form" method="POST" action="{{ url('rant_action') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" class="rant_id" name="rant_id" value="{{ $show_rant->rant_id }}">
		<input type="hidden" class="user_id" name="user_id" value="{{ Auth::user()->user_id }}">
		<div  >    
			<h4 class="ui header">Actions</h4>
			<div class="ui basic vertical  buttons left aligned">
				<button class="ui button post_watch" type="submit" name="action" id="count_watch{{$show_rant->rant_id}}" value="Watch">
					Watch (

					<span id="watchers_{{$show_rant->rant_id}}">
						@if ($show_rant->watchers->count() >= 1)
						{{$show_rant->watchers->count()}}
						@else
						0
						@endif
					</span> )	
				</button>
				<button class="ui button" type="submit" name="action" value="Report">Report</button>
				<button class="ui button" type="submit" name="action" value="Not_Rant">Not a rant</button>
				@if ($show_rant->user->user_id == Auth::user()->user_id)
				<button class="ui button delete_rant" type="submit" name="action" value="Delete">Delete Rant</button>
				@endif
			</div>
		</div>
	</form>		

@endif