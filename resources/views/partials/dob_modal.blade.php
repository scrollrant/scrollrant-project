<!-- date of birth model -->
<div class="ui modal dob">
	<div class="header">
		What's your date of birth?
	</div>
	<div class="content">
		<form class="ui large form" role="form" id="dob_form" method="POST" action="{{ url('update_birthday') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="field">
				<label>Month</label>
				<select name="month" class="ui dropdown">	
					<option value='' selected="selected">Select Month </option>
					<option value='01' {{ (old("month") == 01 ? "selected":"") }} >January</option>
					<option value='02' {{ (old("month") == 02 ? "selected":"") }}>February</option>
					<option value='03' {{ (old("month") == 03 ? "selected":"") }}>March</option>
					<option value='04' {{ (old("month") == 04 ? "selected":"") }}>April</option>
					<option value='05' {{ (old("month") == 05 ? "selected":"") }}>May</option>
					<option value='06' {{ (old("month") == 06 ? "selected":"") }}>June</option>
					<option value='07' {{ (old("month") == 07 ? "selected":"") }}>July</option>
					<option value='08' {{ (old("month") == 8 ? "selected":"") }}>August</option>
					<option value='09' {{ (old("month") == 9 ? "selected":"") }}>September</option>
					<option value='10' {{ (old("month") == 10 ? "selected":"") }}>October</option>
					<option value='11' {{ (old("month") == 11 ? "selected":"") }}>November</option>
					<option value='12' {{ (old("month") == 12 ? "selected":"") }}>December</option> 
				</select>
			</div>
			<div class="field">
				<label>Day</label>
				<input type="text" name="day" placeholder="DD" value="{{ old('day') }}">
			</div>
			<div class="field">
				<label>Year</label>
				<input type="text" name="year" placeholder="YYYY" value="{{ old('year') }}">
			</div>
			
			<button class="ui positive right labeled icon button" type="submit">
				OK
				<i class="checkmark icon"></i>
			</button>
		</form>
	</div>
</div>