<div class="ui segment my-side-menu" id="user_notification_{{Auth::user()->user_id}}">
	<h5 class="ui header align left">
		Notifications
		<i class="small red announcement icon"></i>
	</h5>
	<div class="ui  clearing divider"></div>
	@foreach($watch as $user_watch)
	<div class="ui link list">
		<div class="item" id="watch_{{$user_watch->rant_id}}">
			<a href="{{ route('watch',$user_watch->rant_id) }}" class="item">{{str_limit($user_watch->rant->rant, 20)}}</a>
			<form class="right floated" role="form" id="rant_form" method="POST" action="{{ url('delete_watch', $user_watch->watch_id) }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<button type="submit" class="delete_watch" id="delete_watch{{$user_watch->watch_id}}" data-rantid="{{$user_watch->rant_id}}" data-watchid="{{$user_watch->watch_id}}">X</i></button>
			</form>
		</div>
	</div>
	@endforeach
	@include('partials.warning_message')
	@include('partials.message')
</div>
