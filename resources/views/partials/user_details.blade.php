<div class="ui styled fluid accordion my-side-menu">
	<div class="title">
		<h5 class="ui header align left">
			Your Details
			<i class="dropdown icon"></i>
		</h5> 

	</div>
	<div class="content">
		@foreach($details as $user_details)

		<p>{{$user_details->first_name}}</p>
		<p>{{$user_details->surname}}</p>
		@if ($user_details->gender)
		<p>{{$user_details->gender->gender}}</p>
		@endif
		@if ($user_details->state && $user_details->country)

				@if ($user_details->country->country_name == 'Not Listed')
				<i class="pointing right icon"></i>	<a class="location"> Update Location </a>
				@else
					<p>{{$user_details->state->state_name}}, {{$user_details->country->country_name}}</p>
				@endif								
		@endif
		@endforeach
	</div>
</div>