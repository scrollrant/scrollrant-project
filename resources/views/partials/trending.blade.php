<div class="ui segment my-side-menu">
	<h5 class="ui  header align left">
		Popular Rants
	</h5>
	<div class="ui  clearing divider"></div>
	<div class="ui list">
		@if ($popular_rants->count())
		@foreach ($popular_rants as $popular_rant)
		<div class="item">
			<a href="{{ route('watch',$popular_rant->rant_id) }}" class="item">{{str_limit($popular_rant->rant, 20)}}</a>
			<i class="star grey icon right floated"></i>
		</div>
		@endforeach
		@else
		<p>No popular rants right now!</p>
		@endif

	</div>

</div>