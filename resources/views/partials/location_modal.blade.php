<!-- location model -->
<div class="ui modal location">
	<div class="header">
		Where are you from?
	</div>
	<div class="content">
		<form class="ui large form" role="form" id="location_form" method="POST" action="{{ url('update_location') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			
			<div class="field">
				<label>Country</label>
				<select name="country_id" id="s1" class="ui dropdown s1" data-url="{{ url('register/states')}}">
					<option value="">Select a country</option>
					@foreach($country as $country => $id)
	                    <option value="{{$id}}" {{ (old("country") == $id ? "selected":"") }}> {{$country}} </option>
	                @endforeach
				</select>
			</div>

			<div class="field">
				<label>State</label>
				<select name="state_id" id="s2" class="ui dropdown s2">
					<option value=' '>Select State</option>
				</select>
			</div>
			
			<button class="ui positive right labeled icon button" type="submit">
				OK
				<i class="checkmark icon"></i>
			</button>
			
		</form>
	</div>
</div>