
<div class="ui fluid card my-card">

	<div class="content">
		<!-- this shows the ribbon if the user has influence -->
		@if ($show_rant->user->influence >= 1)
		<a class="ui basic ribbon label">
			@if ($show_rant->user->influence == 1)
			<i class="heart red icon"></i>
			@endif
			@if ($show_rant->user->influence == 2)
			<i class="heart red icon"></i>
			<i class="heart red icon"></i>
			@endif
			@if($show_rant->user->influence == 3)
			<i class="heart red icon"></i>
			<i class="heart red icon"></i>
			<i class="heart red icon"></i>
			@endif
		</a>
		@endif
		<div class="ui items">
			<div class="item">	
			@if ($show_rant->rant_face_id == Null)
			@if ( Auth::user('user') && $show_rant->user->user_id == Auth::user()->user_id)
			<a class="ui small image" id="rant_face{{$show_rant->rant_id}}">
				<img class="ui avatar image" src="{{ asset('/images/circle.png') }}">
				<!--The modal used to pick a rant face-->
				<div class="ui modal" id="rant_face{{$show_rant->rant_id}}">
					<div class="header">
						A face speaks louder than words!
					</div>
					<div class="content">
						<form class="ui large form" role="form" id="rant_face_form" method="POST" action="{{ url('update_rant_face') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="rant_id" value="{{$show_rant->rant_id}}">
							<div class="image content">
								<div class="ui four cards">
									@foreach ($rant_faces as $rant_face)
									<div class="card">
										<div class="image">
											<button type="submit" class="ui fluid button"  name="rant_face_id" value="{{$rant_face->rant_face_id}}"> <img class="ui medium image"  src="{{ asset($rant_face->rant_face_path) }}"> </button>
											<!-- <input type="image" class="ui medium image" value="{{$rant_face->rant_face_id}}" name="rant_face_id" src="{{ asset($rant_face->rant_face_path) }}"/> -->
										</div>
										<div class="extra">
											{{$rant_face->rant_face_name}}:
											<!-- <div class="ui star rating" data-rating="4"></div> -->
										</div>
									</div>
									@endforeach
								</div>
							</div>
						</form>
					</div>
				</div>
			</a>
			@else
				<div class="ui small image">
					<img class="ui avatar image" src="{{ asset('images/circle.png') }}">
				</div>
			@endif
			@else
				<div class="ui small image">
					<img class="ui avatar image" title="{{$show_rant->rant_face->rant_face_name}}" src="{{ asset($show_rant->rant_face->rant_face_path) }}">
				</div>
			@endif
			<div class="content">
				<div class="meta">
					<span>
						@if(Request::is('profile') &&  $show_rant->status =='private')
							Anonymous 
							@if ($show_rant->user->user_status_id != null)
							<a class="ui tag  label"><i class="star icon"></i>{{$show_rant->user->userStatus->status}}</a> 
							@endif
							@include('partials.actions_popup')
						@else
							@if ($show_rant->hooks->count() || $show_rant->status =='public')
							{{$show_rant->user->first_name}}
							{{$show_rant->user->surname}}
							@if ($show_rant->user->user_status_id != null)
							<a class="ui tag  label"><i class="star icon"></i>{{$show_rant->user->userStatus->status}}</a> 
							@endif
							@include('partials.actions_popup')
							@else
							{{$show_rant->user->alias_name}} 
							@if ($show_rant->user->user_status_id != null)
							<a class="ui tag  label"><i class="star icon"></i>{{$show_rant->user->userStatus->status}}</a> 
							@endif
							@include('partials.actions_popup')
							@endif						    
						@endif
					</span>
				</div>
				<div class="description">

					<p>{{$show_rant->rant}}</p>

				</div>
				@if ($show_rant->hooks->count())
				<div class="extra">
					<div class="item">
						@if (Auth::user('user'))
							<a href="#" class="item button show_hooks_popup">
								@foreach ($show_rant->hooks as $rant_hook)
									@ {{$rant_hook->hook_name}}
								@endforeach
							</a>
							@include('partials.hooks_popup')
						@else
							
							@foreach ($show_rant->hooks as $rant_hook)
									@ {{$rant_hook->hook_name}}
							@endforeach
			
						@endif
					</div>
				</div>
				@endif
				@if ($show_rant->photo)
				<div class="ui rounded image">

					<img class="ui image" src="{{asset($show_rant->photo->path)}}">

				</div>
				@endif
			</div>
		</div>
	</div>
</div>
<div class="content">
	<span class="right floated">
		<div class="ui link list">
			<div class="item">
				<a id="count_care_{{$show_rant->rant_id}}">{{$show_rant->careFactor->where('care_factor', 1)->count()}} </a> 
				<a >cares</a>
			</div>
		</div>
	</span>
	<span class="left floated">
		<div class="ui link list">
			<div class="item">
				<a id="count_{{$show_rant->rant_id}}">{{$show_rant->comments->count()}} </a> 
				<a class="hook" data-content="show/hide comments" data-variation="tiny" id="toggle_{{$show_rant->rant_id}}">comments</a>
			</div>
		</div>
	</span>
</div>
<div class="content">
		@if (Auth::user('user'))
		<!--care or dont care form  -->
		<form class="" role="form" id="rant_actions_form" method="POST" action="{{ url('carefactor') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" class="rant_id" name="rant_id" value="{{ $show_rant->rant_id }}">
			<input type="hidden" class="user_id" name="user_id" value="{{ Auth::user()->user_id }}">
			<div class="ui basic fluid buttons">
				@if ($show_rant->careFactor->where('user_id', Auth::user()->user_id)->where('care_factor', 1)->first())
				<button  class="ui inverted blue button post_care_factor" name="carefactor" type="submit" value="1">Care</button>
				@else
				<button  class="ui button post_care_factor" name="carefactor" type="submit" value="1">Care</button>
				@endif
				<div class="or"></div>
				@if ($show_rant->careFactor->where('user_id', Auth::user()->user_id)->where('care_factor', 0)->first())
				<button class="ui inverted blue button post_care_factor" name="carefactor" type="submit" value="0">Dont Care</button>
				@else
				<button class="ui button post_care_factor" name="carefactor" type="submit" value="0">Dont Care</button>
				@endif
			</div>
		</form>
	@endif
	<div class="ui collapsed minimal comments" id="p_comment{{$show_rant->rant_id}}">
		@foreach ($show_rant->comment as $user_comment)
		<div class="comment">
			<div class="content">
				<a class="author">
					
					{{$user_comment->first_name}}
					
				</a>
				<div class="metadata">
					<span class="date">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($user_comment->pivot->created_date))->diffForHumans() }}</span>
				</div>
				<div class="text">
					{{$user_comment->pivot->rant_comment}}
				</div>
				<div class="actions">
					<!-- <a class="reply">Reply</a> -->
				</div>
			</div>
		</div>
		@endforeach
	</div>
	<p id="comment_demo_{{$show_rant->rant_id}}"></p>
</div>
@if (Auth::user('user'))
<div class="extra content">
	<!--create a comment for the rant -->
	
	<form  role="form" id="comments_form" method="POST" action="{{ url('comment') }}">
		<input type="hidden" class="rant_id" name="rant_id" value="{{ $show_rant->rant_id }}">
		<input type="hidden" class="user_id" name="user_id" value="{{ Auth::user()->user_id }}">
		<div class="ui large fluid transparent left icon input">
			<i class="comment outline icon"></i>
			<input type="text" class="user_comment" name="comment" placeholder="Add Comment...">
			<button class="ui tiny basic button post_comment" type="submit">Post</button>
		</div>
	</form>
</div>
@endif
</div>

<script type="text/javascript">
$(document).ready(function() {


	$('.button')
	  .popup({
	    inline: true,
	    hoverable: true
	  })
	;
	$('.ui.modal','#rant_face'+{{$show_rant->rant_id}})
	.modal('attach events', '#rant_face'+{{$show_rant->rant_id}}, 'show')
	;

	$('#toggle_'+{{$show_rant->rant_id}}).on('click', function(e) {

		$('#p_comment'+{{$show_rant->rant_id}}).toggleClass("collapsed"); 
		
		$('#comment_demo_'+{{$show_rant->rant_id}}).toggle();

		e.preventDefault();
	});
});
</script>



