<div class="ui borderless menu" style="background-color:#b30000;">
  <div class="ui container">
    <div class="item">
      <!-- <h1> Scroll<b>Rant</b></h1> -->
      <a class="ui inverted large header center aligned" href="{{ url('home') }}">Scroll<b>Rant</b></a>
    </div>
    <div class="right  menu computer only">
    <div class="item">
      <div class="ui search">
      <div class="ui icon input" style="opacity: 0.5; width: 250px;" >
        <input class="prompt" type="text" placeholder="Search hooks...">
        <i class="search icon"></i>
      </div>
      </div>
    </div>
    <div class="item">
      @if (!Auth::user())
    <a href="{{{url('home')}}}" class="right item">
      Home
    </a>
    @endif
    </div>
  </div>
  
</div>
</div>