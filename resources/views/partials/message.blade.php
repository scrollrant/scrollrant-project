@if (Session::has('message'))
	<div class="ui success message visible">
		<i class="close icon"></i>
		<p>{!! Session::get('message') !!}</p>
	</div>
@endif