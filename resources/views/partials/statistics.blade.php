<div class="ui styled fluid accordion my-side-menu">
	<div class="title">
	<h5 class="ui  header align left">
		Your Statistics
		<i class="dropdown icon"></i>
	</h5>
</div>
<div class="content">
	<div class="ui blue mini statistics">
		
		<div class="ui mini statistic">
			<div class="value">

			<a href="">	{{$rant_stats->where('scope_id', 3)->count()}}</a>

			</div>
			<div class="label">
				State 
			</div>
		</div>
		<div class="statistic">
			<div class="value">
			<a href="">	{{$rant_stats->where('scope_id', 2)->count()}}</a>
			</div>
			<div class="label">
				Country 
			</div>
		</div>
		<div class="statistic">
			<div class="value">
			<a href="">	{{$rant_stats->where('scope_id', 1)->count()}}</a>
			</div>
			<div class="label">
				Global 
			</div>
		</div>
		<div class="statistic">
			<div class="value">
				<a href="">{{$rant_stats->where('scope_id', 4)->count()}}</a>
			</div>
			<div class="label">
				Blocked 
			</div>
		</div>
		
	</div>
</div>
</div>