@if (Session::has('warning_message'))
	<div class="ui warning message visible">
		<i class="close icon"></i>
		<div class="header">
	    	Unable to link account
	  	</div>
		<p>{!! Session::get('warning_message') !!}</p>
	</div>
@endif