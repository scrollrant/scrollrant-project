@if (count($errors) > 0)
<!-- error message for server side validation -->
<div class="ui error message visible">
<div class="small header">
There was some errors with your submission
</div>
<ul class="list">						  
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif