@if (Session::has('socialite_message'))
<div class="ui dimmer modals page transition visible active" style="display: block !important;">
<div class="ui modal transition visible active scrolling">
  <div class="header">
    Link Your Accounts
  </div>
  <div class=" content">
    <div class="description">
      <div class="ui header">You have created a new account. Link your other social media accounts otherwise you will have separate accounts for each social platform.</div>    	
    </div>
  </div>
  <div class="content">
  	@include('partials.user_linked_accounts')
  </div>
  <div class="actions">
    <a href="{{ url('profile') }}" class="ui negative right labeled icon button close_modal">
      Next time
      <i class="close icon"></i>
    </a>
  </div>
</div> 
</div>
@endif

