
<div class="ui  vertical footer segment" style="background-color:#F8F8F9;">
  <div class="ui container">
    <div class="ui stackable  divided equal height stackable grid">
      <div class="stretched row">
      <div class="three wide column">
        <h4 class="ui  header">About</h4>
        <div class="ui  link list">
          <a href="{{{ URL::to('guide') }}}" class="item">Guide</a>
          <a href="{{{ URL::to('privacy') }}}" class="item">Privacy</a>
          <a href="{{{ URL::to('terms') }}}" class="item">Terms</a>
          <a href="https://www.facebook.com/ScrollRant" class="item"><i class="facebook icon"></i>Facebook</a>
          <a href="https://www.twitter.com/ScrollRant" class="item"><i class="twitter icon"></i>Twitter</a>
        </div>
      </div>
      <div class="three wide column">
        <h4 class="ui  header">Services</h4>
        <div class="ui  link list">
          <!-- <a href="{{{ URL::to('company_login') }}}" class="item">Company <i class="star icon"></i></a> -->
          <a href="{{{ URL::to('login') }}}" class="item">Login</a>
          <a href="{{{ URL::to('register') }}}" class="item">User Signup</a>
          <a href="{{{ URL::to('home') }}}" class="item">Home</a>
        </div>
      </div>
      <div class="seven wide column ">
        <h4 class="ui  header">About Us</h4>
        <p>We are a social media site that makes it easy for your complaints, rants and vents to be heard and valued. Make a complaint about a service or rant about life events and we will make sure it is well recieved while you stay private.</p>
      </div>
    </div>
  </div>
  </div>
</div>
