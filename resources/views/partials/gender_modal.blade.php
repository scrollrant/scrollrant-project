<!-- gender model -->
<div class="ui modal gender">
	<!-- <i class="close icon"></i> -->
	<div class="header">
		Pick your gender
	</div>
	<div class="content">
		<form class="ui large form" role="form" id="gender_form" method="POST" action="{{ url('update_gender') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="field">
				<label>Gender</label>
				<select name="gender" class="ui dropdown">
					<option value="">Gender</option>
					<option value="1">Male</option>
					<option value="2">Female</option>
				</select>
			</div>
			
			<button class="ui positive right labeled icon button" type="submit">
				OK
				<i class="checkmark icon"></i>
			</button>
		</form>
	</div>
</div>