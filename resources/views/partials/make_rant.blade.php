<form class="ui large form" role="form" id="rant_form" method="POST" action="{{ url('rant') }}" enctype="multipart/form-data">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="field">
		<h5 class="ui header center aligned">RANT HERE</h5>
		<textarea name="rant" style="height: 170px;" placeholder="Let it rip here" length="500">{{ old('rant') }}</textarea>
	</div>
	<div class="sixteen  wide column">

		<div class="ui basic buttons">
			<div class="upload" data-content="Upload a picture to the rant">
				<label for="file" class="ui icon button">
					<i class="camera icon"></i>
					Upload</label>
					<input type="file" id="file" name="file" style="display:none">
				</div>
				<div class="ui basic floating dropdown labeled search icon button hook" data-content="Tag a company to the rant">
					<input name="hook_id" type="hidden">
					<i class="tag icon"></i>
					<span class="text">Hook Company</span>
					<div class="menu" >
						@foreach($hook as $rant_hook)
							<div class="item" data-value="{{$rant_hook->hook_id}}">{{$rant_hook->hook_name}}</div>
						@endforeach
					</div>
				</div>
			</div>
			
			<button class="ui  right floated red button" type="submit">Post Rant</button>
		</div>
		@include('partials.errors')
	</form>