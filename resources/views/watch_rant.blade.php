@extends('auth.master')

@section('title', 'ScrollRant-Watch')
@section('content')
@include('partials.navbar_fixed')
<div class="ui grid centered container" >
		
		<!-- rants and form column computer only-->
		<div class="sixteen wide mobile sixteen wide tablet ten wide computer column top-space" id="context" style="min-height: 1000px !important;">
			
			<div class="left ui rail very close computer only" style="min-height: 1000px;">
				<div class="ui sticky" style="height: 1000px !important;" >
					@include('partials.user_details')

					@include('partials.notifications')	

					@include('partials.statistics')

					
				</div>
			</div>

			<div class="right ui rail very close computer only">
				<div class="ui sticky">
					@include('partials.trending')

					@include('partials.most_hooked')
				</div>
			</div>

			@include('partials.message')
			
			@foreach ($rant as $show_rant)
				
				@include('partials.rant')

			@endforeach

			<div class="ui horizontal divider">OTHER RANTS MADE BY THIS USER</div>
			
			@foreach ($other_rants as $show_rant)

				@include('partials.rant')
				
			@endforeach

		</div>
</div>
@stop
