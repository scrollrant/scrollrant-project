@extends('auth.master')

@section('title', 'ScrollRant-Company Login')
@include('partials.navbar')
@section('content')

<div class="ui  container content_height">
	<div class="ui stackable grid">
		<div class="seven wide column">
			@include('auth.company_login')
		</div>
		<div class="one wide column">
			<div class="ui vertical divider">OR</div>
		</div>

		<div class="eight wide column">
			@include('auth.company_signup')
		</div>
	</div>

</div>
@include('partials.footer')
@stop