
<form class="ui large form segment" role="form" id="login_form" method="POST" action="{{ url('company_login') }}">
	<h5 class="ui header">Company Login</h5>
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="field">
		<div class="ui left icon input">
			<i class="user icon"></i>
			<input type="text" name="email" placeholder="E-mail address" value="{{ old('email') }}">
		</div>
	</div>
	<div class="field">
		<div class="ui left icon input">
			<i class="lock icon"></i>
			<input type="password" name="password" placeholder="Password">
		</div>
	</div>
	<div class="inline field">
		<div class="ui checkbox">
			<input type="checkbox" tabindex="0" value="true" class="hidden" id='remember' name="remember">
			<label>Remember Me</label>
		</div>
	</div>
	<button type="submit" class="ui fluid large red submit button">
		Login
	</button>
	@include('partials.errors')
</form>
<div class="ui message">
	<a class="" href="{{{ URL::to('/password/email') }}}"> Forgot your password?</a>
</div>

