<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>  @yield('title')</title>
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">	
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="csrf-token" content="{{ Session::token() }}" />
	@yield('css')
	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script>
    	var comment_url = '{{ url("comment") }}';
    	var delete_url = '{{ url("delete_watch/") }}';
    	var watch_url = '{{ url("rant_action") }}';
    	var care_factor_url = '{{ url("carefactor") }}';
	</script>
</head>
<body>
	
	@yield('content')

	<script type="text/javascript" src="{{ asset('/js/main.js') }}"> </script>
	<!-- <script src="https://cdn.socket.io/socket.io-1.3.5.js"></script> -->	
	<!-- <script type="text/javascript" src="http://scrollrant.com:3000/socket.io/socket.io.js"></script> -->
	<script type="text/javascript" src="https://scrollrant.com/socket.io/socket.io.js"></script>
	

	@yield('scripts')
</body>
</html>