@extends('auth.master')

@section('title', 'ScrollRant-Login')
@section('content')
@include('partials.navbar')
<div class="ui text container content_height">
	@include('partials.message')
	<div class="ui segment">
		<h5 class="ui header center aligned">USER LOGIN WITH</h5>
		<form class="ui basic segment center aligned" role="form" id="login_form" method="POST" action="{{ url('social_login') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<a class="ui facebook button" href="{{{ URL::to('login/callback/facebook') }}}">
				<i class="facebook icon"></i>
				Facebook
			</a>
			
			<a class="ui twitter button" href="{{{ URL::to('login/callback/twitter') }}}">
				<i class="twitter icon"></i>
				Twitter
			</a>
			{{--
			<a class="ui google plus button" href="{{{ URL::to('login/callback/google') }}}">
				<i class="google plus icon"></i>
				Google Plus
			</a> --}}
		</form>
		<div class="ui horizontal divider">Or</div>
		<form class="ui large form" role="form" id="login_form" method="POST" action="{{ url('login') }}">

			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="field">
				<div class="ui left icon input">
					<i class="user icon"></i>
					<input type="text" name="email" placeholder="E-mail address" value="{{ old('email') }}">
				</div>
			</div>
			<div class="field">
				<div class="ui left icon input">
					<i class="lock icon"></i>
					<input type="password" name="password" placeholder="Password">
				</div>
			</div>
			<div class="inline field">
				<div class="ui checkbox">
					<input type="checkbox" tabindex="0" value="true" class="hidden" id='remember' name="remember">
					<label>Remember Me</label>
				</div>
			</div>
			<button type="submit" class="ui fluid large blue submit button">
				Login
			</button>
			@include('partials.errors')
		</form>
	</div>
	<div class="ui message">
		<a href="{{{ URL::to('register') }}}">Sign Up</a> |
		<a class="" href="{{{ URL::to('/password/email') }}}">Forgot your password?</a>
	</div>

</div>
@include('partials.footer')
@stop