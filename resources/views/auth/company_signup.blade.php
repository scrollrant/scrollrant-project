@include('partials.message')
	<form class="ui large form segment" role="form" id="login_form" method="POST" action="{{ url('company') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="field">
			<label>Company Name</label>
			<input type="text" name="company_name" placeholder="Company Name" value="{{ old('first_name') }}">
		</div>
		
		<div class="field">
			<label>Email</label>
			<input type="text" name="company_email" placeholder="Company Email" value="{{ old('email') }}">
		</div>
		<div class="two fields">
			<div class="field">
				<label>Password</label>
				<input type="password" name="company_password" placeholder="Password">
			</div>
			<div class="field">
				<label>Confirm Password</label>
				<input type="password" name="password_confirmation_company" placeholder="Confirm Password">
			</div>
		</div>
		<p>By clicking Signup, you agree to our <a href="{{{ URL::to('terms') }}}">Terms</a> and have read our <a href="{{{ URL::to('privacy') }}}">Privacy Policy</a>. </p>
		<button type="submit" class="ui fluid large blue submit button">
			Signup
		</button>
		@include('partials.errors')
	</form>
	<div class="ui message">
		<div class="small header">
		<i class="pointing right icon"></i> You can also create an account using Twitter or Facebook login!
		</div>
		<a href="{{{ URL::to('login/callback/twitter') }}}"><i class="twitter icon"></i>Twitter Login</a>
		<a href="{{{ URL::to('login/callback/facebook') }}}"><i class="facebook icon"></i>Facebook Login</a>
		<!-- Already signed up? <a href="{{{ URL::to('login') }}}">Login</a> -->
	</div>





