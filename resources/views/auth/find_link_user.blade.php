@extends('auth.master')

@section('title', 'ScrollRant-Account Link')
@section('content')
@include('partials.navbar')
<div class="ui text container content_height">
	@include('partials.message')
	<form class="ui large form segment" role="form" id="link_accounts_form" method="POST" action="{{ url('link_accounts') }}">
		<h5 class="ui header">Enter your scrollrant credentials to link accounts</h5>
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" class="social_id" name="social_id" value="{{ $socialite_user['social_id'] }}">
		<input type="hidden" class="social_email" name="social_email" value="{{ $socialite_user['social_email'] }}">
		<input type="hidden" class="social_name" name="social_name" value="{{ $socialite_user['social_name'] }}">
		<div class="field">
			<div class="ui left icon input">
				<i class="user icon"></i>
				<input type="text" name="email" placeholder="E-mail address" value="{{ old('email') }}">
			</div>
		</div>
		<div class="field">
			<div class="ui left icon input">
				<i class="lock icon"></i>
				<input type="password" name="password" placeholder="Password">
			</div>
		</div>
		<button type="submit" class="ui fluid large blue submit button">
			Link Accounts
		</button>
		@include('partials.errors')
	</form>		
</div>
@include('partials.footer')
@stop