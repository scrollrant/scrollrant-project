@extends('auth.master')

@section('title', 'ScrollRant-Signup')
@section('content')
@include('partials.navbar')
<div class="ui text container content_height bottom-gap">
	@include('partials.message')
	<form class="ui large form segment" role="form" id="login_form" method="POST" action="{{ url('user') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="two fields">
			<div class="field">
				<label>First Name</label>
				<input type="text" name="first_name" placeholder="First Name" value="{{ old('first_name') }}">
			</div>
			<div class="field">
				<label>Surname</label>
				<input type="text" name="surname" data-validate="surname" placeholder="Surname" value="{{ old('surname') }}">
			</div>
		</div>
		<div class="field">
			<label>Alias Name</label>
			<input type="text" name="alias_name" placeholder="Alias Name" value="{{ old('alias_name') }}">
		</div>
		<div class="field">
			<label>Email</label>
			<input type="text" name="email" placeholder="Email" value="{{ old('email') }}" data-validate="email_exists">
		</div>
		<div class="field">
			<label>Password</label>
			<input type="password" name="password" placeholder="Password">
		</div>
		<div class="field">
			<label>Confirm Password</label>
			<input type="password" name="password_confirmation" placeholder="Confirm Password">
		</div>
		<p>By clicking Signup, you agree to our <a href="{{{ URL::to('terms') }}}">Terms</a> and have read our <a href="{{{ URL::to('privacy') }}}">Privacy Policy</a>. </p>
		<button type="submit" class="ui fluid large blue submit button">
			Signup
		</button>
		@include('partials.errors')
	</form>
	<div class="ui message">
		Already signed up? <a href="{{{ URL::to('login') }}}">Login</a>
	</div>
</div>
@include('partials.footer')
@stop



