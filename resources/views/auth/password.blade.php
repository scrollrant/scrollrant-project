@extends('auth.master')

@section('title', 'ScrollRant-Request Password Change')
@section('content')
@include('partials.navbar')
<div class="ui text container content_height">
	<form class="ui large form segment" role="form" id="request_password" method="POST" action="{{ url('/password/email') }}">
		<h5 class="ui header">Request a password reset link</h5>
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="field">
			<div class="ui left icon input">
				<i class="user icon"></i>
				<input type="text" name="email" placeholder="E-mail address" value="{{ old('email') }}">
			</div>
		</div>
		<button type="submit" class="ui fluid large blue submit button">
			Send Password Reset Link
		</button>
		@if (session('status'))
			<!--success message for server side validation-->
			<div class="ui success message visible">
				{{ session('status') }}
			</div>
		@endif
		@include('partials.errors')
	</form>
	<div class="ui message">
		Already signed up? <a href="{{{ URL::to('login') }}}">Login</a>
	</div>
		
</div>
@include('partials.footer')
@stop