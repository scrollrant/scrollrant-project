@extends('auth.master')

@section('title', 'ScrollRant-Reset Password')
@section('content')
@include('partials.navbar')
<div class="ui text container content_height">
	<form class="ui large form segment" role="form" id="reset_password" method="POST" action="{{ url('/password/reset') }}">
		<h5 class="ui header">Reset your password</h5>
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="token" value="{{ $token }}">
		<div class="field">
			<div class="ui left icon input">
				<i class="user icon"></i>
				<input type="text" name="email" placeholder="E-mail address" value="{{ old('email') }}">
			</div>
		</div>
		<div class="field">
			<div class="ui left icon input">
				<!-- <i class="privacy icon"></i> -->
				<input type="password" name="password" placeholder="New Password">
			</div>
		</div>
		<div class="field">
			<div class="ui left icon input">
				<!-- <i class="password icon"></i> -->
				<input type="password" name="password_confirmation" placeholder="Re-Enter New Password">
			</div>
		</div>
		<button type="submit" class="ui fluid large blue submit button">
			Reset Password
		</button>
		@if (session('status'))
		<!--success message for server side validation-->
		<div class="ui success message visible">
			{{ session('status') }}
		</div>
		@endif
		@include('partials.errors')
	</form>
	<div class="ui message">
		Already signed up? <a href="{{{ URL::to('login') }}}">Login</a>
	</div>

</div>
@include('partials.footer')
@stop