@extends('auth.master')

@section('title', 'ScrollRant-Company Account Status')
@include('partials.navbar')
@section('content')

<div class="ui  container content_height">
	<div class="ui stackable grid">
		<div class="row">
			<div class="sixteen wide colunm center align">
				@if (Session::has('account_status_message'))
				<div class="ui warning message visible">
					<!-- <i class="close icon"></i> -->
					<p>{!! Session::get('account_status_message') !!}</p>
				</div>
				@endif
			</div>
		</div>
		<div class="seven wide column">
			<!--Link social user to existing scrollrant account-->
			<form  role="form" id="link_account" method="POST" action="{{ url('find_user') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" class="social_name" name="social_name" value="{{ $socialite_user->getName() }}">
				<input type="hidden" class="social_id" name="social_id" value="{{ $socialite_user->getId() }}">
				<input type="hidden" class="social_email" name="social_email" value="{{ $socialite_user->getEmail() }}">

				<button type="submit" class="ui fluid large blue submit button">
					Already have an account
				</button>
			</form>
		</div>
		<div class="one wide column">
			<div class="ui vertical divider">OR</div>
		</div>

		<div class="eight wide column">
			<!--Create a new scrollrant account from the socialite user and link accounts-->
			<form  role="form" id="link_account" method="POST" action="{{ url('#') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" class="social_name" name="social_name" value="{{ $socialite_user->getName() }}">
				<input type="hidden" class="social_id" name="social_id" value="{{ $socialite_user->getId() }}">
				<input type="hidden" class="social_email" name="social_email" value="{{ $socialite_user->getEmail() }}">
				<input type="hidden" class="avatar" name="avatar" value="{{ $socialite_user->getAvatar() }}">
				<button type="submit" class="ui fluid large red submit button">
					Create a new account
				</button>
			</form>
		</div>
	
	</div>

</div>
@include('partials.footer')
@stop